import {bootstrap}    from '@angular/platform-browser-dynamic';
import {appComponent} from './components/app';
import {HTTP_PROVIDERS} from '@angular/http';
import { enableProdMode } from '@angular/core';
enableProdMode();
bootstrap(appComponent,[HTTP_PROVIDERS]);
