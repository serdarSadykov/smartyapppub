import {Component} from '@angular/core';
import {spotsList} from './spot/spotsList';
import {spotService} from '../services/spotService';
import {languageService} from '../services/languageService';
import {notifyService} from '../services/notifyService';
import {swiperTabsService} from '../services/swiperTabsService';
import {datetimepeaker} from '../services/datetimepeaker';
@Component({
    selector: 'smartyApp',
    templateUrl: './app/views/app.html',
    directives:[spotsList],
    providers:[spotService,languageService,notifyService,swiperTabsService,datetimepeaker]
})
export class appComponent {
	constructor() {}
}
