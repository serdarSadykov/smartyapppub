"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var spotsList_1 = require('./spot/spotsList');
var spotService_1 = require('../services/spotService');
var languageService_1 = require('../services/languageService');
var notifyService_1 = require('../services/notifyService');
var swiperTabsService_1 = require('../services/swiperTabsService');
var datetimepeaker_1 = require('../services/datetimepeaker');
var appComponent = (function () {
    function appComponent() {
    }
    appComponent = __decorate([
        core_1.Component({
            selector: 'smartyApp',
            templateUrl: './app/views/app.html',
            directives: [spotsList_1.spotsList],
            providers: [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService, swiperTabsService_1.swiperTabsService, datetimepeaker_1.datetimepeaker]
        }), 
        __metadata('design:paramtypes', [])
    ], appComponent);
    return appComponent;
}());
exports.appComponent = appComponent;
//# sourceMappingURL=app.js.map