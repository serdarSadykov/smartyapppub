"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var spotService_1 = require('./../../services/spotService');
var languageService_1 = require('../../services/languageService');
var notifyService_1 = require('../../services/notifyService');
var smartySpot_1 = require('../../models/smartySpot');
var spotHW_1 = require('./spotHW');
var spotItem = (function () {
    function spotItem(_spotService, _lan, _notify) {
        this._spotService = _spotService;
        this._lan = _lan;
        this._notify = _notify;
        this.spotItemEvent = new core_1.EventEmitter();
        this.commonState = 0;
        this.commonTools = {};
    }
    spotItem.prototype.ngOnInit = function () {
        var _this = this;
        for (var _i = 0, _a = this.spot.hw; _i < _a.length; _i++) {
            var hw = _a[_i];
            for (var _b = 0, _c = hw.tools; _b < _c.length; _b++) {
                var tool = _c[_b];
                this.commonTools[tool] = true;
            }
        }
        this._spotSubscription = this._spotService._spot$.subscribe(function (item) { return _this.spotsChangedHandler(item); });
    };
    spotItem.prototype.ngOnDestroy = function () {
        this._spotSubscription.unsubscribe();
    };
    spotItem.prototype.spotsChangedHandler = function (data) {
        //console.log(this.spot.available?"available":"NAvailable");
        var sum = [];
        for (var _i = 0, _a = this.spot.hw; _i < _a.length; _i++) {
            var hw = _a[_i];
            if (hw.state) {
                sum.push(hw.state | 0);
            }
        }
        this.commonState = sum.length ? (sum.reduce(function (a, b) { return a + b; }, 0) / sum.length) : 0;
    };
    spotItem.prototype.hwStateChangedHandler = function (hw) {
        //this.spot.hw.find((el:smartySpotHW,indx:number,arr:smartySpotHW[])=>{
        //	return el.ID === hw.ID;
        //})
        //this.spotsChangedHandler();
    };
    spotItem.prototype.setState = function (event) {
        var _this = this;
        var state;
        var tool = "DIM";
        this.spot.setIsWorks();
        if (event.target.getAttribute('type') === "checkbox") {
            state = event.target.checked ? 255 : 0;
            tool = "ONOFF";
        }
        else if (event.target.getAttribute('type') === "range") {
            state = event.target.value;
            tool = "DIM";
        }
        this._spotService.setSpotState(this.spot, state, tool).then(function () {
            _this.commonState = state;
            _this.spot.setIsNWorks();
        }, function () {
            _this._notify.error(_this._lan.__('sendToSpotError'));
            if (event.target.getAttribute('type') === "checkbox")
                event.target.checked = _this.commonState > 0;
            else if (event.target.getAttribute('type') === "range")
                event.target.value = _this.commonState;
            _this.spot.setIsNWorks();
        });
    };
    spotItem.prototype.shedulerList = function (event) {
        this.spotItemEvent.emit({ "code": "shedulerList", "spotItem": this });
    };
    spotItem.prototype.configList = function (event) {
        if (['BUTTON', 'INPUT', 'I'].indexOf(event.target.tagName) == -1
            && ['BUTTON', 'INPUT'].indexOf(event.target.parentNode.tagName) == -1
            && (!event.target.firstChild || ['BUTTON', 'INPUT'].indexOf(event.target.firstChild.tagName) == -1)) {
            this.focus = true;
            this.spotItemEvent.emit({ "code": "configList", "spotItem": this });
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', smartySpot_1.smartySpot)
    ], spotItem.prototype, "spot", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], spotItem.prototype, "focus", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], spotItem.prototype, "spotItemEvent", void 0);
    spotItem = __decorate([
        core_1.Component({
            selector: 'spotItem',
            templateUrl: './app/views/spot/spotItem.html',
            directives: [spotHW_1.spotHW],
        }), 
        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService])
    ], spotItem);
    return spotItem;
}());
exports.spotItem = spotItem;
//# sourceMappingURL=spotItem.js.map