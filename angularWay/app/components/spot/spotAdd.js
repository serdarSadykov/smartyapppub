"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var notifyService_1 = require('../../services/notifyService');
var languageService_1 = require('../../services/languageService');
var spotService_1 = require('./../../services/spotService');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
require('rxjs/add/operator/switchMap');
require('rxjs/add/operator/timeout');
require('rxjs/add/operator/toPromise');
var spotAdd = (function () {
    function spotAdd(_spotService, _lan, _notify, http) {
        this._spotService = _spotService;
        this._lan = _lan;
        this._notify = _notify;
        this.http = http;
        this.noSpots = new core_1.EventEmitter();
        this.spotAddEvent = new core_1.EventEmitter();
        this.setConfig = 'http://192.168.4.1/setConfig';
        this.exeCommand = 'http://192.168.4.1/exeCommand';
        this.getInfo = 'http://192.168.4.1/getInfo';
    }
    spotAdd.prototype.ngOnInit = function () { this.begin(); };
    spotAdd.prototype.cloading = function () { this.onLoading = true; };
    spotAdd.prototype.cloaded = function () { this.onLoading = false; };
    spotAdd.prototype.cerror = function () { this.onError = true; };
    spotAdd.prototype.cnoerror = function () { this.onError = false; };
    spotAdd.prototype.begin = function () {
        this.title = this._lan.__('spotSearch');
        this.onLoading = false;
        this.onError = false;
        this.localAP = null;
        this.ssidList = [];
        this.noSSSIDList = false;
        if (confirm(this._lan.__('needWIFI'))) {
            this.step0();
        }
        else {
            this.stopAdd();
        }
    };
    spotAdd.prototype.stopAdd = function () {
        this.cloaded();
        this.spotAddEvent.emit('close');
    };
    spotAdd.prototype.fail = function (message) {
        if (message === void 0) { message = null; }
        console.log("fail");
        this.cloaded();
        this.cerror();
        this.title = message || this._lan.__('errorDefault');
        this._notify.error(message);
        this.noSSSIDList = false;
    };
    spotAdd.prototype.step0 = function () {
        var _this = this;
        this.cloading();
        this._spotService.corExe('currentWiFiSSID', []).then(function (result) {
            _this.localAP = result;
            _this.step1();
        }, function (message) {
            _this.fail(_this._lan.__('noLocalWIFI'));
        });
    };
    spotAdd.prototype.step1 = function () {
        var _this = this;
        this._spotService.corExe('findNewSpot', []).then(function (data) {
            _this.step2();
        }, function (data) {
            if (data == 'NotFound') {
                _this._notify.alert(_this._lan.__('notConfiguredNF'));
                _this._spotService.corExe('connectConfigured', [_this.localAP]).then(function () {
                    _this._spotService.spotsIdentify();
                    _this.stopAdd();
                }, function () {
                    _this.fail(data);
                });
            }
            else {
                _this.fail(data);
            }
        });
    };
    spotAdd.prototype.step2 = function () {
        var _this = this;
        this._spotService.corExe('connectNewSpot', []).then(function () {
            _this.step3();
        }, function (data) {
            if (data == 'NotConnected') {
                _this.fail(_this._lan.__('spotCantConnect'));
            }
            else {
                _this.fail(data);
            }
        });
    };
    spotAdd.prototype.step3 = function () {
        var _this = this;
        this._spotService.corExe('isConnectedToNewSpot', []).then(function (result) {
            _this.step4();
        }, function (data) {
            if (data == 'NotConnected') {
                _this.fail(_this._lan.__('spotCantConnect'));
            }
            else {
                _this.fail(data);
            }
        });
    };
    spotAdd.prototype.step4 = function () {
        var _this = this;
        console.log("step4");
        this.cloading();
        this.ssidList = [];
        this.noSSSIDList = false;
        this.title = this._lan.__('spotSearch');
        this.http.get(this.getInfo + '?ssidlist=1')
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.ssidList = [];
            _this.cloaded();
            if (data.result) {
                _this.title = _this._lan.__('spotConnectWiFi');
                for (var item in data.content) {
                    _this.ssidList.push({ 'bssid': item, 'ssid': data.content[item] });
                }
                if (_this.ssidList.length == 0) {
                    _this.title = _this._lan.__('spotConnectWiFiNotFound');
                    _this.noSSSIDList = true;
                }
            }
            else {
                _this.fail();
            }
        }, function (err) { return _this.fail(err); });
    };
    spotAdd.prototype.step5 = function (apBSSID) {
        var _this = this;
        this.cloading();
        this.ssidList = [];
        this.title = this._lan.__('spotConnectWiFiPass');
        this._spotService.corExe('isConnectedToNewSpot', []).then(function (result) {
            var apPass = prompt(_this._lan.__('spotConnectWiFiPass'), '');
            if (apPass.length == 0) {
                _this._notify.alert(_this._lan.__('spotConnectWiFiCantConnect'));
                _this.step4();
                return;
            }
            console.log(_this.setConfig + '?apBSSID=' + apBSSID + "&apPass=" + apPass);
            _this.http.get(_this.setConfig + '?apBSSID=' + apBSSID + "&apPass=" + apPass)
                .map(function (res) { return res.text(); })
                .subscribe(function (data) {
                if (parseInt(data) == 1) {
                    _this.step6();
                }
                else {
                    _this.fail();
                }
            }, function (err) { return _this.fail(err); });
        }, function (data) {
            if (data == 'NotConnected') {
                _this.fail(_this._lan.__('spotCantConnect'));
            }
            else {
                _this.fail(data);
            }
        });
    };
    spotAdd.prototype.step6 = function () {
        var _this = this;
        this.title = this._lan.__('spotConnectWiFiConnecting');
        this.http.get(this.exeCommand + '?connectAP=1')
            .map(function (res) { return res.text(); })
            .timeout(60000, function () { console.log("timeout"); _this.fail(_this._lan.__('spotCantConnect')); })
            .subscribe(function (data) {
            console.log("step6", parseInt(data));
            switch (parseInt(data)) {
                case 1:
                    _this.step7();
                    break;
                case 2:
                case -1:
                    _this._notify.error(_this._lan.__('spotConnectWiFiPassWrong'));
                    _this.step4();
                    break;
                case -2:
                case -3:
                    _this._notify.error(_this._lan.__('spotConnectWiFiCantConnect'));
                    _this.step4();
                    break;
                case -4:
                    _this._notify.error(_this._lan.__('spotConnectWiFiNotSecured'));
                    _this.step4();
                    break;
                default:
                    _this._notify.error(_this._lan.__('spotConnectWiFiCantConnect'));
                    _this.step4();
            }
        }, function (err) { return _this.fail(err); });
    };
    spotAdd.prototype.step7 = function () {
        var _this = this;
        this.title = this._lan.__('spotConfigured');
        this._spotService.corExe('defaultTimeZone', []).then(function (timeZone) {
            console.log(_this.setConfig + '?timeZone=' + timeZone);
            _this.http.get(_this.setConfig + '?timeZone=' + timeZone)
                .map(function (res) { return res.text(); })
                .subscribe(function (data) {
                if (parseInt(data) == 1) {
                    _this.step8();
                }
                else {
                    _this.fail();
                }
            }, function (err) { return _this.fail(err); });
        }, function () {
            _this.fail();
        });
    };
    spotAdd.prototype.step8 = function () {
        var _this = this;
        this.title = this._lan.__('spotEnterName');
        var spotTitle = prompt(this._lan.__('spotEnterName'), '');
        this.http.get(this.setConfig + '?spotTitle=' + spotTitle)
            .map(function (res) { return res.text(); })
            .subscribe(function (data) {
            if (parseInt(data) == 1) {
                _this.step9();
            }
            else {
                _this.fail();
            }
        }, function (err) { return _this.fail(err); });
    };
    spotAdd.prototype.step9 = function () {
        var _this = this;
        this.http.get(this.exeCommand + '?rebootSpot=1')
            .map(function (res) { return res.text(); })
            .subscribe(function (data) { }, function (err) { }, function () {
            _this._spotService.corExe('connectConfigured', [_this.localAP]).then(function () { }, function () { });
            _this.title = _this._lan.__('spotConnected');
            _this.stopAdd();
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], spotAdd.prototype, "noSpots", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], spotAdd.prototype, "spotAddEvent", void 0);
    spotAdd = __decorate([
        core_1.Component({
            selector: 'spotAdd',
            templateUrl: './app/views/spot/spotAdd.html',
        }),
        __param(3, core_1.Inject(http_1.Http)), 
        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService, http_1.Http])
    ], spotAdd);
    return spotAdd;
}());
exports.spotAdd = spotAdd;
//# sourceMappingURL=spotAdd.js.map