import {Component, Input, Output, Inject, EventEmitter,OnInit} from '@angular/core';
import {notifyService} from '../../services/notifyService';
import {languageService} from '../../services/languageService';
import {spotService} from './../../services/spotService';
import {smartySpotHW} from '../../models/smartySpotHW';
import {Http, Headers, Response, URLSearchParams} from '@angular/http';
import {Observable, Subject, ReplaySubject} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';


@Component({
    selector: 'spotAdd',
    templateUrl: './app/views/spot/spotAdd.html',
})
export class spotAdd implements OnInit{
	@Input() noSpots = new EventEmitter();
	@Output() spotAddEvent = new EventEmitter();
    title:string;
    onLoading:boolean;
    onError:boolean;
    localAP:string;
    setConfig:string;
    exeCommand:string;
    getInfo:string;
	ssidList:Object[];
	noSSSIDList:boolean;
    constructor(private _spotService:spotService,private _lan:languageService,private _notify:notifyService,@Inject(Http) private http: Http) {
        this.setConfig  = 'http://192.168.4.1/setConfig';
        this.exeCommand = 'http://192.168.4.1/exeCommand';
	    this.getInfo	= 'http://192.168.4.1/getInfo';
    }
	ngOnInit() {this.begin()}
    cloading(){this.onLoading = true;}
    cloaded(){this.onLoading = false;}
    cerror(){this.onError = true;}
    cnoerror(){this.onError = false;}
    begin():void{
		this.title = this._lan.__('spotSearch');
        this.onLoading  = false;
        this.onError    = false;
        this.localAP    = null;
		this.ssidList	= [];
		this.noSSSIDList= false;
        if(confirm(this._lan.__('needWIFI'))){
			this.step0();
        }else{
            this.stopAdd();
		}
    }
    stopAdd():void{
		this.cloaded();
        this.spotAddEvent.emit('close');
    }
    fail(message:string = null):void{
		console.log("fail");
		this.cloaded();
		this.cerror();
        this.title = message || this._lan.__('errorDefault');
		this._notify.error(message);
		this.noSSSIDList= false;
	}
    step0():void{
		this.cloading();
        this._spotService.corExe('currentWiFiSSID',[]).then((result)=>{
            this.localAP=result;
            this.step1();
        },(message:string)=>{
            this.fail(this._lan.__('noLocalWIFI'));
        });
    }

	step1():void{
        this._spotService.corExe('findNewSpot',[]).then((data:string)=>{
            this.step2();
        },(data:string)=>{
            if(data == 'NotFound'){
                this._notify.alert(this._lan.__('notConfiguredNF'));
				this._spotService.corExe('connectConfigured',[this.localAP]).then(()=>{
					this._spotService.spotsIdentify();
					this.stopAdd();
				},()=>{
					this.fail(data);
				});
            }else{
                this.fail(data);
            }
        });
    }
    step2():void{
	    this._spotService.corExe('connectNewSpot',[]).then(()=>{
            this.step3();
        },(data:string)=>{
            if(data == 'NotConnected'){
                this.fail(this._lan.__('spotCantConnect'));
            }else{
				this.fail(data);
            }
        })
    }
    step3():void{
        this._spotService.corExe('isConnectedToNewSpot',[]).then((result:string)=>{
            this.step4();
        },(data:string)=>{
            if(data == 'NotConnected'){
                this.fail(this._lan.__('spotCantConnect'));
            }else{
                this.fail(data);
            }
        })
    }
    step4():void{
		console.log("step4");
		this.cloading();
		this.ssidList = [];
		this.noSSSIDList = false;
		this.title = this._lan.__('spotSearch');

        this.http.get(this.getInfo+'?ssidlist=1')
	    .map((res:Response) => res.json())
        .subscribe(
            data => {
				this.ssidList = [];
				this.cloaded();
                if(data.result){
					this.title = this._lan.__('spotConnectWiFi');
					for(let item in data.content){
						this.ssidList.push({'bssid':item,'ssid':data.content[item]});
					}
                    if(this.ssidList.length==0){
                       	this.title = this._lan.__('spotConnectWiFiNotFound');
                        this.noSSSIDList = true;
                    }
                }else{
                    this.fail();
                }
            },
            err	=> this.fail(err)
        );
    }

    step5(apBSSID:string):void{
		this.cloading();
		this.ssidList = [];
		this.title = this._lan.__('spotConnectWiFiPass');
        this._spotService.corExe('isConnectedToNewSpot',[]).then((result:string)=>{
			let apPass:string = prompt(this._lan.__('spotConnectWiFiPass'), '');
			if(apPass.length==0){
				this._notify.alert(this._lan.__('spotConnectWiFiCantConnect'));
				this.step4();
				return;
			}
			console.log(this.setConfig+'?apBSSID='+apBSSID+"&apPass="+apPass);
			this.http.get(this.setConfig+'?apBSSID='+apBSSID+"&apPass="+apPass)
			.map((res:Response) => res.text())
			.subscribe(
				data => {
					if(parseInt(data) == 1){
						this.step6();
					}else{
						this.fail();
					}
				},
				err	=> this.fail(err)
			);
        },(data:string)=>{
            if(data == 'NotConnected'){
                this.fail(this._lan.__('spotCantConnect'));
            }else{
                this.fail(data);
            }
        })
	}

	step6():void{
		this.title = this._lan.__('spotConnectWiFiConnecting');
		this.http.get(this.exeCommand+'?connectAP=1')
		.map((res:Response) => res.text())
		.timeout(60000,()=>{console.log("timeout");this.fail(this._lan.__('spotCantConnect'))})
		.subscribe(
			data => {
				console.log("step6",parseInt(data));
				switch(parseInt(data)){
					case 1:
						this.step7();
						break;
					case 2:
					case -1:
						this._notify.error(this._lan.__('spotConnectWiFiPassWrong'));
						this.step4();
						break;
					case -2:
					case -3:
						this._notify.error(this._lan.__('spotConnectWiFiCantConnect'));
						this.step4();
						break;
					case -4:
						this._notify.error(this._lan.__('spotConnectWiFiNotSecured'));
						this.step4();
						break;
					default:
						this._notify.error(this._lan.__('spotConnectWiFiCantConnect'));
						this.step4();
				}
			},
			err	=> this.fail(err)
		);
	}
	step7():void{
		this.title = this._lan.__('spotConfigured');
		this._spotService.corExe('defaultTimeZone',[]).then((timeZone:string)=>{
			console.log(this.setConfig+'?timeZone='+timeZone);
			this.http.get(this.setConfig+'?timeZone='+timeZone)
			.map((res:Response) => res.text())
			.subscribe(
				data => {
					if(parseInt(data) == 1){
						this.step8();
					}else{
						this.fail();
					}
				},
				err	=> this.fail(err)
			);
		},()=>{
			this.fail();
		})
	}
	step8():void{
		this.title = this._lan.__('spotEnterName');
		let spotTitle:string = prompt(this._lan.__('spotEnterName'), '');
		this.http.get(this.setConfig+'?spotTitle='+spotTitle)
		.map((res:Response) => res.text())
		.subscribe(
			data => {
				if(parseInt(data) == 1){
					this.step9();
				}else{
					this.fail();
				}
			},
			err	=> this.fail(err)
		);
	}
	step9():void{
		this.http.get(this.exeCommand+'?rebootSpot=1')
		.map((res:Response) => res.text())
		.subscribe(
			data => {},
			err	=> {},
			()=>{
				this._spotService.corExe('connectConfigured',[this.localAP]).then(()=>{},()=>{});
				this.title=this._lan.__('spotConnected');
				this.stopAdd();
			}
		);
	}
}