import {Component, Input, Output, EventEmitter,OnInit} from '@angular/core';
import {spotService} from './../../services/spotService';
import {languageService} from '../../services/languageService';
import {notifyService} from '../../services/notifyService';
import {smartySpot} from '../../models/smartySpot';
import {smartySpotHW} from '../../models/smartySpotHW';
@Component({
    selector: 'spotHW',
    templateUrl: './app/views/spot/spotHW.html',
})
export class spotHW implements OnInit{
	@Input() spot:smartySpot;
	@Input() hw:smartySpotHW;
	@Output() stateChangedEvent = new EventEmitter();
	tools:Object;
    constructor(private _spotService:spotService,private _lan:languageService,private _notify:notifyService) {
		this.tools = {};
	}
	ngOnInit() {
		for(let tool of this.hw.tools){
			this.tools[tool] = true;
		}
	}
    onSave():void {}
	setState(event:any){
		let state:number;
		let tool:string = "DIM";
		this.spot.setIsWorks();
		if(event.target.getAttribute('type') === "checkbox"){
			state = event.target.checked?255:0;
			tool = "ONOFF";
		}else if(event.target.getAttribute('type') === "range"){
			state = event.target.value;
			tool = "DIM";
		}
		this._spotService.setSpotHWState(this.spot,this.hw,state,tool).then(()=>{
			this.hw.state = state;
			this.spot.setIsNWorks();
			this.stateChangedEvent.emit(this.hw);
		},()=>{
			this._notify.error(this._lan.__('sendToSpotError'));
			if(event.target.getAttribute('type') === "checkbox") event.target.checked = this.hw.state>0;
			else if(event.target.getAttribute('type') === "range") event.target.value = this.hw.state;
			this.spot.setIsNWorks();
		});
	}
}