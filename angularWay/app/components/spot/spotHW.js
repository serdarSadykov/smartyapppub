"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var spotService_1 = require('./../../services/spotService');
var languageService_1 = require('../../services/languageService');
var notifyService_1 = require('../../services/notifyService');
var smartySpot_1 = require('../../models/smartySpot');
var smartySpotHW_1 = require('../../models/smartySpotHW');
var spotHW = (function () {
    function spotHW(_spotService, _lan, _notify) {
        this._spotService = _spotService;
        this._lan = _lan;
        this._notify = _notify;
        this.stateChangedEvent = new core_1.EventEmitter();
        this.tools = {};
    }
    spotHW.prototype.ngOnInit = function () {
        for (var _i = 0, _a = this.hw.tools; _i < _a.length; _i++) {
            var tool = _a[_i];
            this.tools[tool] = true;
        }
    };
    spotHW.prototype.onSave = function () { };
    spotHW.prototype.setState = function (event) {
        var _this = this;
        var state;
        var tool = "DIM";
        this.spot.setIsWorks();
        if (event.target.getAttribute('type') === "checkbox") {
            state = event.target.checked ? 255 : 0;
            tool = "ONOFF";
        }
        else if (event.target.getAttribute('type') === "range") {
            state = event.target.value;
            tool = "DIM";
        }
        this._spotService.setSpotHWState(this.spot, this.hw, state, tool).then(function () {
            _this.hw.state = state;
            _this.spot.setIsNWorks();
            _this.stateChangedEvent.emit(_this.hw);
        }, function () {
            _this._notify.error(_this._lan.__('sendToSpotError'));
            if (event.target.getAttribute('type') === "checkbox")
                event.target.checked = _this.hw.state > 0;
            else if (event.target.getAttribute('type') === "range")
                event.target.value = _this.hw.state;
            _this.spot.setIsNWorks();
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', smartySpot_1.smartySpot)
    ], spotHW.prototype, "spot", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', smartySpotHW_1.smartySpotHW)
    ], spotHW.prototype, "hw", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], spotHW.prototype, "stateChangedEvent", void 0);
    spotHW = __decorate([
        core_1.Component({
            selector: 'spotHW',
            templateUrl: './app/views/spot/spotHW.html',
        }), 
        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService])
    ], spotHW);
    return spotHW;
}());
exports.spotHW = spotHW;
//# sourceMappingURL=spotHW.js.map