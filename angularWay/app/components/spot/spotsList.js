"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var spotService_1 = require("./../../services/spotService");
var languageService_1 = require("../../services/languageService");
var spotItem_1 = require("./spotItem");
var notifyService_1 = require("../../services/notifyService");
var swiperTabsService_1 = require("../../services/swiperTabsService");
var shedulerList_1 = require("../sheduler/shedulerList");
var shedulerAdd_1 = require("../sheduler/shedulerAdd");
var spotAdd_1 = require("../spot/spotAdd");
var spotsList = (function () {
    function spotsList(_spotService, _lan, _notify, _swiperTabsService) {
        this._spotService = _spotService;
        this._lan = _lan;
        this._notify = _notify;
        this._spotsCats = [];
        this.commonLightState = false;
        this.commonSocketState = false;
        this.commonLightCount = 0;
        this.commonSocketCount = 0;
        this.configSpot = null;
        this.shedulerSpot = null;
        this.mqttConfigForm = false;
        this.mqttStatus = 0;
        this.mqttStatusStr = "";
        this.mqttPublish = "";
        this.mqttSubscribe = "";
        this.mqttServer = "";
        this.mqttPort = "";
        this.mqttUser = "";
        this.mqttPass = "";
        this._swiperTabsService = _swiperTabsService;
        this.needRenderTabs = false;
        this.spotAddM = false;
        this.shedulerAddM = false;
    }
    spotsList.prototype.ngOnInit = function () {
        var _this = this;
        this._spotSubscription = this._spotService._spot$.subscribe(function (spotItems) { return _this.spotsChangedHandler(spotItems); });
        document.addEventListener('backbutton', function () { _this.onBackButton(); }, false);
    };
    spotsList.prototype.ngOnDestroy = function () {
        this._spotSubscription.unsubscribe();
    };
    spotsList.prototype.ngAfterViewChecked = function () {
        if (this.needRenderTabs) {
            console.log("_swiperTabsService rerender");
            this.needRenderTabs = false;
            this._swiperTabsService.renderTabs(".spotsList", this._spotsCats.length > 1 ? 1.7 : 1);
        }
    };
    spotsList.prototype.checkCommonState = function () {
        for (var _i = 0, _a = this._spotsCats; _i < _a.length; _i++) {
            var cat = _a[_i];
            var sum = 0;
            for (var _b = 0, _c = cat.items; _b < _c.length; _b++) {
                var item = _c[_b];
                if (item.available) {
                    for (var _d = 0, _e = item.hw; _d < _e.length; _d++) {
                        var hw = _e[_d];
                        sum += hw.state | 0;
                    }
                }
            }
            switch (cat.type) {
                case "light":
                    this.commonLightState = sum > 0;
                    break;
                case "socket":
                    this.commonSocketState = sum > 0;
                    break;
            }
        }
    };
    spotsList.prototype.renderTabs = function (data) {
        console.log("spot type added");
        this.commonLightCount = 0;
        this.commonSocketCount = 0;
        this._spotsCats = [];
        var added_spots_cats = [];
        for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
            var item = data_1[_i];
            if (added_spots_cats.indexOf(item.type) == -1) {
                added_spots_cats.push(item.type);
                this._spotsCats.push(new spotsCat(item.type, this._lan.__(item.type)));
            }
        }
        //add items to cats
        var _loop_1 = function(item) {
            switch (item.type) {
                case "light":
                    this_1.commonLightCount++;
                    break;
                case "socket":
                    this_1.commonSocketCount++;
                    break;
            }
            var cat = this_1._spotsCats.find(function (el) { return el.type == item.type; });
            if (cat)
                cat.push(item);
        };
        var this_1 = this;
        for (var _a = 0, data_2 = data; _a < data_2.length; _a++) {
            var item = data_2[_a];
            _loop_1(item);
        }
        this.checkCommonState();
        this.needRenderTabs = true;
        this.hideConfigList();
    };
    spotsList.prototype.spotsChangedHandler = function (data) {
        if (!data || data.length == 0)
            return;
        //catch by type
        //if nexist in local
        var _loop_2 = function(sitem) {
            if (!this_2._spotsCats.find(function (el) { return el.type == sitem.type; })) {
                this_2.renderTabs(data);
                return { value: void 0 };
            }
        };
        var this_2 = this;
        for (var _i = 0, data_3 = data; _i < data_3.length; _i++) {
            var sitem = data_3[_i];
            var state_2 = _loop_2(sitem);
            if (typeof state_2 === "object") return state_2.value;
        }
        //if nexist in source
        var _loop_3 = function(cat) {
            if (!data.find(function (el) { return el.type == cat.type; })) {
                this_3.renderTabs(data);
                return { value: void 0 };
            }
        };
        var this_3 = this;
        for (var _a = 0, _b = this._spotsCats; _a < _b.length; _a++) {
            var cat = _b[_a];
            var state_3 = _loop_3(cat);
            if (typeof state_3 === "object") return state_3.value;
        }
        //add if item not found in cats
        var found;
        var _loop_4 = function(sitem) {
            found = false;
            for (var _c = 0, _d = this_4._spotsCats; _c < _d.length; _c++) {
                var cat = _d[_c];
                var item = cat.items.find(function (el) { return el.ID == sitem.ID; });
                if (item) {
                    found = true;
                    item.available = sitem.available;
                    item._inLocal = sitem._inLocal;
                    item.title = sitem.title;
                    break;
                }
            }
            if (!found) {
                var cat = this_4._spotsCats.find(function (el) { return el.type == sitem.type; });
                if (cat)
                    cat.push(sitem);
            }
        };
        var this_4 = this;
        for (var _e = 0, data_4 = data; _e < data_4.length; _e++) {
            var sitem = data_4[_e];
            _loop_4(sitem);
        }
        this.checkCommonState();
    };
    spotsList.prototype.setCommonState = function (type, state) {
        var _this = this;
        var cat = this._spotsCats.find(function (el) { return el.type == type; });
        if (cat)
            var _loop_5 = function(item) {
                item.setIsWorks();
                this_5._spotService.setSpotState(item, state ? 255 : 0, "ONOFF").then(function () {
                    item.setIsNWorks();
                }, function () {
                    _this._notify.error(_this._lan.__("sendToSpotError"));
                    item.setIsNWorks();
                });
            };
            var this_5 = this;
            for (var _i = 0, _a = cat.items; _i < _a.length; _i++) {
                var item = _a[_i];
                _loop_5(item);
            }
    };
    spotsList.prototype.spotItemEventHandler = function (vals) {
        switch (vals.code) {
            case "shedulerList":
                this.shedulerList(vals.spotItem);
                break;
            case "configList":
                this.configList(vals.spotItem);
                break;
        }
    };
    // -----------------------
    spotsList.prototype.onBackButton = function () {
        if (this.mqttConfigForm) {
            this.hideMQTT();
        }
        else if (this.configSpot) {
            this.hideConfigList();
        }
        else if (this.shedulerAddM) {
            this.hideShedulerAdd();
        }
        else if (this.shedulerSpot) {
            this.hideShedulerList();
        }
        else if (this.spotAddM) {
            this.hideSpotAdd();
        }
        else {
            this._spotService.hideApp();
        }
    };
    spotsList.prototype.removeSpot = function () {
        var _this = this;
        if (confirm(this._lan.__("confirm"))) {
            this._spotService.removeSpot(this.configSpot.spot).then(function () {
                _this._notify.alert(_this._lan.__("spotRemoved"));
                _this.hideConfigList();
            }, function () {
                _this._notify.error(_this._lan.__("errorDefault"));
            });
        }
    };
    spotsList.prototype.resetSpot = function () {
        var _this = this;
        if (!this.configSpot.spot._inLocal) {
            this._notify.error(this._lan.__("localOnly"));
            return;
        }
        if (confirm(this._lan.__("confirm"))) {
            this._spotService.resetSpot(this.configSpot.spot).then(function () {
                _this._notify.alert(_this._lan.__("spotReseted"));
                _this.hideConfigList();
            }, function () {
                _this._notify.error(_this._lan.__("errorDefault"));
            });
        }
    };
    spotsList.prototype.renameSpot = function () {
        var _this = this;
        if (confirm(this._lan.__("spotRenamedConfirm"))) {
            var new_name = prompt(this._lan.__("spotEnterName"));
            if (new_name) {
                this._spotService.setSpotTitle(this.configSpot.spot, new_name).then(function () {
                    _this._notify.alert(_this._lan.__("spotRenamed"));
                }, function () {
                    _this._notify.error(_this._lan.__("errorDefault"));
                });
            }
        }
    };
    spotsList.prototype.configList = function (item) {
        if (!this.configSpot || this.configSpot.spot.ID != item.spot.ID) {
            this.hideShedulerList();
            this.hideConfigList();
            this.hideMQTT();
            this.configSpot = item;
        }
    };
    spotsList.prototype.hideConfigList = function () {
        if (this.configSpot) {
            this.configSpot.focus = false;
            this.configSpot = null;
        }
    };
    spotsList.prototype.getMQTTStatus = function () {
        var _this = this;
        if (!this.configSpot.spot._inLocal) {
            this.mqttStatus = -1;
            this.mqttStatusStr = this._lan.__("localOnly");
            return;
        }
        this._spotService.getInfo(this.configSpot.spot, "mqttstatus", 0).then(function (sdata) {
            var data = JSON.parse(sdata);
            _this.mqttStatus = parseInt(data.status) ? 1 : -1;
            if (_this.mqttStatus == 1) {
                _this.mqttStatusStr = _this._lan.__("connected");
                _this.mqttPublish = data.publish.join(", ");
                _this.mqttSubscribe = data.subscribe.join(", ");
            }
            else {
                _this.mqttStatusStr = _this._lan.__("notConnected");
            }
            if (!_this.mqttServer && data.mqttServer)
                _this.mqttServer = data.mqttServer;
            if (!_this.mqttPort && data.mqttPort)
                _this.mqttPort = data.mqttPort;
            if (!_this.mqttUser && data.mqttUser)
                _this.mqttUser = data.mqttUser;
            if (_this.mqttConfigForm)
                setTimeout(function () { _this.getMQTTStatus(); }, 1000);
        }, function (err) {
            _this.mqttStatus = -1;
            _this.mqttStatusStr = _this._lan.__("notConnected");
            if (_this.mqttConfigForm)
                setTimeout(function () { _this.getMQTTStatus(); }, 1000);
        });
    };
    spotsList.prototype.setMQTT = function () {
        if (!this.configSpot.spot._inLocal) {
            this._notify.error(this._lan.__("localOnly"));
            return;
        }
        this.mqttStatus = 0;
        this.mqttStatusStr = "";
        this.mqttPublish = "";
        this.mqttSubscribe = "";
        this.mqttServer = "";
        this.mqttPort = "";
        this.mqttUser = "";
        this.mqttPass = "";
        this.mqttStatusStr = this._lan.__("refresh");
        this.mqttConfigForm = true;
        this.getMQTTStatus();
    };
    spotsList.prototype.saveMQTT = function () {
        var _this = this;
        if (!this.mqttServer || !this.mqttPort) {
            this._notify.alert(this._lan.__("notFullFilled"));
            return;
        }
        if (!this.configSpot.spot._inLocal) {
            this._notify.error(this._lan.__("localOnly"));
            return;
        }
        var params = {
            "mqttServer": this.mqttServer,
            "mqttPort": this.mqttPort,
            "mqttUser": this.mqttUser,
            "mqttPass": this.mqttPass
        };
        this._spotService.setConfig(this.configSpot.spot, params).then(function (success) {
            if (success) {
                _this._notify.alert(_this._lan.__("spotConfigured"));
            }
            else {
                _this._notify.error(_this._lan.__("errorDefault"));
            }
        }, function (err) {
            _this._notify.error(_this._lan.__("errorDefault"));
        });
    };
    spotsList.prototype.hideMQTT = function () {
        this.mqttConfigForm = false;
    };
    //--
    spotsList.prototype.shedulerList = function (item) {
        this.hideConfigList();
        this.hideMQTT();
        this.shedulerSpot = item;
        if (this.shedulerSpot.spot._inLocal) {
            this.shedulerRefresh();
        }
    };
    spotsList.prototype.hideShedulerList = function () {
        this.shedulerSpot = null;
    };
    spotsList.prototype.shedulerListEventHandler = function (code) {
        switch (code) {
            case "hide":
                this.hideShedulerList();
                break;
        }
    };
    spotsList.prototype.shedulerAdd = function () {
        if (this.shedulerSpot)
            this.shedulerAddM = true;
    };
    spotsList.prototype.hideShedulerAdd = function () {
        this.shedulerAddM = false;
    };
    spotsList.prototype.shedulerAddEventHandler = function (result) {
        switch (result) {
            case "hide":
                this.hideShedulerAdd();
                break;
        }
    };
    spotsList.prototype.shedulerReset = function () {
        var _this = this;
        if (confirm(this._lan.__("confirm"))) {
            this._spotService.shedulerReset(this.shedulerSpot.spot).then(function (result) { }, function (fail) {
                _this._notify.error(_this._lan.__("sendToSpotError"));
            });
        }
    };
    spotsList.prototype.shedulerRefresh = function () {
        var _this = this;
        this._notify.error(this._lan.__("shedulerAsking"));
        this._spotService.shedulerRefresh(this.shedulerSpot.spot).then(function (result) {
            _this._notify.error(_this._lan.__("shedulerAskingComplete"));
        }, function (fail) {
            _this._notify.error(_this._lan.__("sendToSpotError"));
        });
    };
    //--
    spotsList.prototype.spotAdd = function () {
        this.spotAddM = true;
    };
    spotsList.prototype.hideSpotAdd = function () {
        var _this = this;
        setTimeout(function () {
            _this.spotAddM = false;
        }, 100);
    };
    spotsList.prototype.spotAddEventHandler = function (code) {
        switch (code) {
            case "close":
                this.hideSpotAdd();
                break;
        }
    };
    spotsList = __decorate([
        core_1.Component({
            selector: "spotsList",
            templateUrl: "./app/views/spot/spotsList.html",
            directives: [spotItem_1.spotItem, shedulerList_1.shedulerList, shedulerAdd_1.shedulerAdd, spotAdd_1.spotAdd]
        }), 
        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService, swiperTabsService_1.swiperTabsService])
    ], spotsList);
    return spotsList;
}());
exports.spotsList = spotsList;
var spotsCat = (function () {
    function spotsCat(type, title) {
        this.type = type;
        this.title = title;
        this.items = [];
    }
    spotsCat.prototype.push = function (item) {
        this.items.push(item);
    };
    return spotsCat;
}());
exports.spotsCat = spotsCat;
//# sourceMappingURL=spotsList.js.map