import {Component, OnInit, DoCheck, AfterViewChecked} from "@angular/core";
import {spotService} from "./../../services/spotService";
import {languageService} from "../../services/languageService";
import {spotItem} from "./spotItem";
import {smartySpot} from "../../models/smartySpot";
import {notifyService} from "../../services/notifyService";
import {swiperTabsService} from "../../services/swiperTabsService";
import {shedulerList} from "../sheduler/shedulerList";
import {shedulerAdd} from "../sheduler/shedulerAdd";
import {spotAdd} from "../spot/spotAdd";
import {spotShedulerItem} from "../../models/spotShedulerItem";
import {Subscription} from "rxjs/Subscription";
@Component({
    selector: "spotsList",
    templateUrl: "./app/views/spot/spotsList.html",
    directives:[spotItem,shedulerList,shedulerAdd,spotAdd]
})
export class spotsList implements OnInit,AfterViewChecked{
    _spotsCats:spotsCat[];
    onChanges:boolean;
    commonLightState:boolean;
    commonLightCount:number;
    commonSocketState:boolean;
    commonSocketCount:number;
    configSpot:spotItem;
    shedulerSpot:spotItem;
    mqttConfigForm:boolean;
    mqttStatus:number;
    mqttStatusStr:string;
    mqttPublish:string;
    mqttSubscribe:string;
    mqttServer:string;
    mqttPort:string;
    mqttUser:string;
    mqttPass:string;
    _swiperTabsService:swiperTabsService;
	_spotSubscription:Subscription;
    needRenderTabs:boolean;
    spotAddM:boolean;
    shedulerAddM:boolean;

    constructor(private _spotService:spotService,private _lan:languageService,private _notify:notifyService,_swiperTabsService:swiperTabsService){
        this._spotsCats	        = [];
        this.commonLightState	= false;
        this.commonSocketState	= false;
        this.commonLightCount	= 0;
        this.commonSocketCount	= 0;
        this.configSpot         = null;
        this.shedulerSpot       = null;
	    this.mqttConfigForm     = false;
        this.mqttStatus         = 0;
        this.mqttStatusStr      = "";
        this.mqttPublish	    = "";
        this.mqttSubscribe      = "";
        this.mqttServer         = "";
        this.mqttPort           = "";
        this.mqttUser           = "";
        this.mqttPass           = "";
        this._swiperTabsService = _swiperTabsService;
        this.needRenderTabs	    = false;
        this.spotAddM	        = false;
        this.shedulerAddM       = false;
    }
	ngOnInit():void{
        this._spotSubscription = this._spotService._spot$.subscribe(spotItems => this.spotsChangedHandler(spotItems));
        document.addEventListener('backbutton', ()=>{this.onBackButton()}, false);
	}
    ngOnDestroy() {
	    this._spotSubscription.unsubscribe();
    }
    ngAfterViewChecked(){
        if(this.needRenderTabs){
            console.log("_swiperTabsService rerender");
            this.needRenderTabs = false;
	        this._swiperTabsService.renderTabs(".spotsList",this._spotsCats.length>1?1.7:1);
        }
    }
    checkCommonState():void{
        for(let cat of this._spotsCats){
            let sum:number = 0;
            for(let item of cat.items){
                if(item.available){
                    for(let hw of item.hw){
                        sum += hw.state | 0;
                    }
                }
            }
            switch(cat.type){
                case "light":
                    this.commonLightState = sum>0;
                    break;
                case "socket":
                    this.commonSocketState = sum>0;
                    break;
            }
        }
    }
    renderTabs(data:smartySpot[]){
        console.log("spot type added");
        this.commonLightCount  = 0;
        this.commonSocketCount = 0;
        this._spotsCats = [];
        let added_spots_cats:string[] = [];
        for(let item of data){
            if(added_spots_cats.indexOf(item.type) == -1){
                added_spots_cats.push(item.type);
                this._spotsCats.push(new spotsCat(item.type,this._lan.__(item.type)));
            }
        }
        //add items to cats
        for(let item of data){
            switch(item.type){
                case "light":
                    this.commonLightCount++;
                    break;
                case "socket":
                    this.commonSocketCount++;
                    break;
            }
            let cat = this._spotsCats.find((el:spotsCat)=>{return el.type == item.type;});
            if(cat) cat.push(item);
        }
	    this.checkCommonState();
        this.needRenderTabs = true;
	    this.hideConfigList();
    }
    spotsChangedHandler(data:smartySpot[]):void{
        if(!data || data.length ==0) return;
        //catch by type
        //if nexist in local
        for(let sitem of data){
            if(!this._spotsCats.find((el:spotsCat)=>{return el.type == sitem.type;})){
                this.renderTabs(data);
                return;
            }
        }
        //if nexist in source
        for(let cat of this._spotsCats){
            if(!data.find((el:smartySpot)=>{return el.type == cat.type;})){
                this.renderTabs(data);
                return;
            }
        }
        
        //add if item not found in cats
        let found:boolean;
        for(let sitem of data){
            found = false;
            for(let cat of this._spotsCats){
                let item = cat.items.find((el:smartySpot)=>{return el.ID == sitem.ID;});
                if(item){
                    found = true;
                    item.available	= sitem.available;
                    item._inLocal	= sitem._inLocal;
                    item.title	    = sitem.title;
                    break;
                }
            }
            if(!found){
	            let cat = this._spotsCats.find((el:spotsCat)=>{return el.type == sitem.type;});
                if(cat) cat.push(sitem);
            }
        }
        this.checkCommonState();
    }

    setCommonState(type:string,state:boolean):void{
	    let cat = this._spotsCats.find((el:spotsCat)=>{return el.type == type;});
        if(cat) for(let item of cat.items){
            item.setIsWorks();
            this._spotService.setSpotState(item,state?255:0,"ONOFF").then(()=>{
                item.setIsNWorks();
            },()=>{
                this._notify.error(this._lan.__("sendToSpotError"));
                item.setIsNWorks();
            });
        }
    }
    spotItemEventHandler(vals:any):void{
        switch(vals.code){
            case "shedulerList":
                this.shedulerList(vals.spotItem);
                break;
            case "configList":
                this.configList(vals.spotItem);
                break;
        }
    }
    // -----------------------
    onBackButton():void{
        if(this.mqttConfigForm){
            this.hideMQTT();
        }else if(this.configSpot){
            this.hideConfigList();
        }else if(this.shedulerAddM){
            this.hideShedulerAdd();
        }else if(this.shedulerSpot){
            this.hideShedulerList();
        }else if(this.spotAddM){
            this.hideSpotAdd();
        }else{
            this._spotService.hideApp();
        }
    }
    removeSpot():void{
	    if(confirm(this._lan.__("confirm"))){
            this._spotService.removeSpot(this.configSpot.spot).then(()=>{
                this._notify.alert(this._lan.__("spotRemoved"));
	            this.hideConfigList();
            },()=>{
                this._notify.error(this._lan.__("errorDefault"));
            });
        }
    }
    resetSpot():void{
        if(!this.configSpot.spot._inLocal){
            this._notify.error(this._lan.__("localOnly"));
            return;
        }
	    if(confirm(this._lan.__("confirm"))){
            this._spotService.resetSpot(this.configSpot.spot).then(()=>{
                this._notify.alert(this._lan.__("spotReseted"));
	            this.hideConfigList();
            },()=>{
                this._notify.error(this._lan.__("errorDefault"));
            });
        }
    }
    renameSpot():void{
	    if(confirm(this._lan.__("spotRenamedConfirm"))){
	        let new_name:string = prompt(this._lan.__("spotEnterName"));
	        if(new_name){
	            this._spotService.setSpotTitle(this.configSpot.spot,new_name).then(()=>{
	            	this._notify.alert(this._lan.__("spotRenamed"));
	            },()=>{
	            	this._notify.error(this._lan.__("errorDefault"));
	            });
	        }
	    }
    }
    configList(item:spotItem):void{
        if(!this.configSpot || this.configSpot.spot.ID != item.spot.ID){
            this.hideShedulerList();
            this.hideConfigList();
            this.hideMQTT();
            this.configSpot = item;
        }
    }
    hideConfigList():void{
        if(this.configSpot){
            this.configSpot.focus	= false;
            this.configSpot	        = null;
        }
    }
    getMQTTStatus(){
        if(!this.configSpot.spot._inLocal){
            this.mqttStatus = -1;
            this.mqttStatusStr = this._lan.__("localOnly");
            return;
        }
        this._spotService.getInfo(this.configSpot.spot,"mqttstatus",0).then(
            sdata=>{
                let data = JSON.parse(sdata);
                this.mqttStatus = parseInt(data.status)?1:-1;
                if(this.mqttStatus == 1){
                    this.mqttStatusStr	= this._lan.__("connected");
                    this.mqttPublish	= data.publish.join(", ");
                    this.mqttSubscribe	= data.subscribe.join(", ");
                }else{
	                this.mqttStatusStr = this._lan.__("notConnected");
                }
                if(!this.mqttServer && data.mqttServer) this.mqttServer  = data.mqttServer;
                if(!this.mqttPort && data.mqttPort)	    this.mqttPort    = data.mqttPort;
                if(!this.mqttUser && data.mqttUser)	    this.mqttUser    = data.mqttUser;

                if(this.mqttConfigForm) setTimeout(()=>{this.getMQTTStatus();},1000);
            },
            err=>{
                this.mqttStatus = -1;
                this.mqttStatusStr = this._lan.__("notConnected");
                if(this.mqttConfigForm) setTimeout(()=>{this.getMQTTStatus();},1000);
            }
        )
    }
    setMQTT():void{
        if(!this.configSpot.spot._inLocal){
            this._notify.error(this._lan.__("localOnly"));
            return;
        }
        this.mqttStatus         = 0;
        this.mqttStatusStr      = "";
        this.mqttPublish	    = "";
        this.mqttSubscribe      = "";
        this.mqttServer         = "";
        this.mqttPort           = "";
        this.mqttUser           = "";
        this.mqttPass           = "";
        this.mqttStatusStr  = this._lan.__("refresh");
        this.mqttConfigForm = true;
        this.getMQTTStatus();
    }
    saveMQTT():void{
        if(!this.mqttServer || !this.mqttPort){
            this._notify.alert(this._lan.__("notFullFilled"));
            return;
        }
        if(!this.configSpot.spot._inLocal){
            this._notify.error(this._lan.__("localOnly"));
            return;
        }
        let params = {
            "mqttServer": this.mqttServer,
            "mqttPort"  : this.mqttPort,
            "mqttUser"  : this.mqttUser,
            "mqttPass"  : this.mqttPass
        }
        this._spotService.setConfig(this.configSpot.spot,params).then(
            success=>{
                if(success){
                    this._notify.alert(this._lan.__("spotConfigured"));
                }else{
                    this._notify.error(this._lan.__("errorDefault"));
                }
            },
            err=>{
                this._notify.error(this._lan.__("errorDefault"));
            }
        );
    }
    hideMQTT():void{
        this.mqttConfigForm = false;
    }
    //--
    shedulerList(item:spotItem):void{
        this.hideConfigList();
	    this.hideMQTT();
        this.shedulerSpot = item;
        if(this.shedulerSpot.spot._inLocal){
            this.shedulerRefresh();
        }
    }
    hideShedulerList():void{
	    this.shedulerSpot = null;
    }
    shedulerListEventHandler(code:string):void{
        switch(code){
            case "hide":
	            this.hideShedulerList();
                break;
        }
    }
    shedulerAdd():void{
        if(this.shedulerSpot) this.shedulerAddM = true;
    }
    hideShedulerAdd():void{
        this.shedulerAddM = false;
    }
    shedulerAddEventHandler(result:string):void{
        switch(result){
            case "hide":
                this.hideShedulerAdd();
                break;
        }
    }
    shedulerReset():void{
	    if(confirm(this._lan.__("confirm"))){
            this._spotService.shedulerReset(this.shedulerSpot.spot).then(result=>{},fail=>{
                this._notify.error(this._lan.__("sendToSpotError"));
            });
        }
    }
    shedulerRefresh():void{
	    this._notify.error(this._lan.__("shedulerAsking"));
        this._spotService.shedulerRefresh(this.shedulerSpot.spot).then(result=>{
            this._notify.error(this._lan.__("shedulerAskingComplete"));
        },fail=>{
            this._notify.error(this._lan.__("sendToSpotError"));
        });
    }
    //--
    spotAdd():void{
        this.spotAddM = true;
    }
    hideSpotAdd():void{
        setTimeout(()=>{
            this.spotAddM = false;
        },100);
    }
    spotAddEventHandler(code:string):void{
        switch(code){
            case "close":
                this.hideSpotAdd();
                break;
        }
    }
}

export class spotsCat{
    items:smartySpot[];
    constructor(public type:string,private title:string){
         this.items = [];
    }
    push(item:smartySpot):void{
        this.items.push(item);
    }
}