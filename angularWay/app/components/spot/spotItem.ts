import {Component, Input, Output, EventEmitter, OnInit, OnChanges, DoCheck, ChangeDetectionStrategy} from '@angular/core';
import {spotService} from './../../services/spotService';
import {languageService} from '../../services/languageService';
import {notifyService} from '../../services/notifyService';
import {smartySpot} from '../../models/smartySpot';
import {spotHW} from './spotHW';
import {smartySpotHW} from '../../models/smartySpotHW';
import {Subscription} from 'rxjs/Subscription';


@Component({
    selector: 'spotItem',
    templateUrl: './app/views/spot/spotItem.html',
    directives:[spotHW],
})
export class spotItem implements OnInit{
	@Input() spot:smartySpot;
	@Input() focus:boolean;
	@Output() spotItemEvent = new EventEmitter();

	commonState:number;
	commonTools:Object;
	_spotSubscription:Subscription;

    constructor(private _spotService:spotService,private _lan:languageService,private _notify:notifyService) {
		this.commonState = 0;
		this.commonTools = {};
	}
	ngOnInit():void{
		for(let hw of this.spot.hw){
			for(let tool of hw.tools){
				this.commonTools[tool] = true;
			}
		}
        this._spotSubscription = this._spotService._spot$.subscribe(item => this.spotsChangedHandler(item));    
	}
    ngOnDestroy() {
	    this._spotSubscription.unsubscribe();
    }
    spotsChangedHandler(data:smartySpot[]):void{
		//console.log(this.spot.available?"available":"NAvailable");
		let sum:number[] = [];
		for(let hw of this.spot.hw){
			if(hw.state){
				sum.push(hw.state | 0);
			}
		}
		this.commonState = sum.length?(sum.reduce((a,b)=>{return a+b},0)/sum.length):0;
	}
	hwStateChangedHandler(hw:smartySpotHW){
		//this.spot.hw.find((el:smartySpotHW,indx:number,arr:smartySpotHW[])=>{
		//	return el.ID === hw.ID;
		//})
		//this.spotsChangedHandler();
	}
	setState(event:any){
		let state:number;
		let tool:string = "DIM";
		this.spot.setIsWorks();
		if(event.target.getAttribute('type') === "checkbox"){
			state = event.target.checked?255:0;
			tool = "ONOFF";
		}else if(event.target.getAttribute('type') === "range"){
			state = event.target.value;
			tool = "DIM";
		}
		this._spotService.setSpotState(this.spot,state,tool).then(()=>{
			this.commonState = state;
			this.spot.setIsNWorks();
		},()=>{
			this._notify.error(this._lan.__('sendToSpotError'));
			if(event.target.getAttribute('type') === "checkbox") event.target.checked = this.commonState>0;
			else if(event.target.getAttribute('type') === "range") event.target.value = this.commonState;
			this.spot.setIsNWorks();
		});
	}
	shedulerList(event:any):void{
		this.spotItemEvent.emit({"code":"shedulerList","spotItem":this});
	}
	configList(event:any):void{
		if(
				['BUTTON','INPUT','I'].indexOf(event.target.tagName) == -1
			&&	['BUTTON','INPUT'].indexOf(event.target.parentNode.tagName) == -1
			&&	(!event.target.firstChild || ['BUTTON','INPUT'].indexOf(event.target.firstChild.tagName) == -1)
		){
			this.focus = true;
			this.spotItemEvent.emit({"code":"configList","spotItem":this});
		}
	}
}