"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var spotService_1 = require('./../../services/spotService');
var languageService_1 = require('../../services/languageService');
var notifyService_1 = require('../../services/notifyService');
var smartySpot_1 = require('../../models/smartySpot');
var shedulerItem_1 = require('./shedulerItem');
var shedulerList = (function () {
    function shedulerList(_spotService, _lan, _notify) {
        this._spotService = _spotService;
        this._lan = _lan;
        this._notify = _notify;
        this.shedulerListEvent = new core_1.EventEmitter();
        this.onLoading = false;
        this.shedulerItems = [];
    }
    shedulerList.prototype.ngOnInit = function () {
        var _this = this;
        this._shedulerSubscription = this._spotService._sheduler$.subscribe(function (items) {
            _this.shedulerItems = [];
            for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
                var item = items_1[_i];
                if (item.spotID == _this.spot.ID) {
                    _this.shedulerItems.push(item);
                }
            }
        });
    };
    shedulerList.prototype.ngOnDestroy = function () {
        this._shedulerSubscription.unsubscribe();
    };
    shedulerList.prototype.shedulerDeleteEventHandler = function (shedulerItem) {
        var _this = this;
        this.onLoading = true;
        this._spotService.shedulerRemove(this.spot, shedulerItem).then(function (result) {
            _this._notify.alert(_this._lan.__('taskRemoved'));
            _this.onLoading = false;
        }, function (err) {
            _this._notify.alert(_this._lan.__('sendToSpotError'));
            _this.onLoading = false;
        });
    };
    shedulerList.prototype.hide = function () {
        this.shedulerListEvent.emit('hide');
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', smartySpot_1.smartySpot)
    ], shedulerList.prototype, "spot", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], shedulerList.prototype, "shedulerListEvent", void 0);
    shedulerList = __decorate([
        core_1.Component({
            selector: 'shedulerList',
            templateUrl: './app/views/sheduler/shedulerList.html',
            directives: [shedulerItem_1.shedulerItem]
        }), 
        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService])
    ], shedulerList);
    return shedulerList;
}());
exports.shedulerList = shedulerList;
//# sourceMappingURL=shedulerList.js.map