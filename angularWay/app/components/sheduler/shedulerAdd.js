"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var spotService_1 = require('./../../services/spotService');
var languageService_1 = require('../../services/languageService');
var notifyService_1 = require('../../services/notifyService');
var spotShedulerItem_1 = require('../../models/spotShedulerItem');
var smartySpot_1 = require('../../models/smartySpot');
var datetimepeaker_1 = require('../../services/datetimepeaker');
var shedulerAdd = (function () {
    function shedulerAdd(_spotService, _lan, _notify, _datetimepeaker) {
        this._spotService = _spotService;
        this._lan = _lan;
        this._notify = _notify;
        this._datetimepeaker = _datetimepeaker;
        this.shedulerAddEvent = new core_1.EventEmitter();
        this.onLoading = false;
        this.hw = null;
        this.taskhw = null;
        this.spotTools = [];
        this.taskPType = null;
        this.DOW = null;
        this.DOM = null;
        this.DATE = null;
        this.HOURS = null;
        this.MINUTES = null;
        this.command = null;
        this.DIM = null;
        this.title = null;
        this.renderDateNeed = false;
        this.shedulerTaskPType = [];
        this.DOWS = [];
        this.avaibleHW = [];
        this.avaibleDOMS = [];
        this.avaibleHOURS = [];
        this.avaibleMINUTES = [];
    }
    shedulerAdd.prototype.ngOnInit = function () {
        this.shedulerTaskPType = this._lan.capts.shedulerTaskPType();
        this.DOWS = this._lan.capts.DOWS();
        for (var _i = 0, _a = this.spot.hw; _i < _a.length; _i++) {
            var item = _a[_i];
            this.avaibleHW.push(this._lan.capts.hw(item.type, item.ID));
        }
        for (var i = 1; i <= 31; i++) {
            this.avaibleDOMS.push(i);
        }
        for (var i = 0; i <= 23; i++) {
            this.avaibleHOURS.push(i);
        }
        for (var i = 0; i <= 55; i += 5) {
            this.avaibleMINUTES.push(i);
        }
    };
    shedulerAdd.prototype.ngDoCheck = function () { };
    shedulerAdd.prototype.ngAfterViewChecked = function () {
        if (this.renderDateNeed) {
            console.log("DATE render");
            this.setTaskDATE(dateYMD());
            this._datetimepeaker.datepeaker(".shedulerAdd .dateInline");
            this.renderDateNeed = false;
        }
    };
    shedulerAdd.prototype.setTaskDATE = function (date) {
        var shedulerAddDATE = document.getElementById("shedulerAddDATE");
        if (shedulerAddDATE)
            shedulerAddDATE.value = date;
    };
    shedulerAdd.prototype.getTaskDATE = function () {
        var shedulerAddDATE = document.getElementById("shedulerAddDATE");
        return shedulerAddDATE ? shedulerAddDATE.value : "";
    };
    shedulerAdd.prototype.setHW = function (event) {
        if (!event.target.value)
            return;
        this.spotTools = [];
        this.hw = this.spot.hw.find(function (el, indx, arr) {
            return el.ID == event.target.value;
        });
        if (this.hw)
            for (var _i = 0, _a = this.hw.tools; _i < _a.length; _i++) {
                var tool = _a[_i];
                if (tool == "ONOFF") {
                    this.spotTools.push({ "key": "ON", "val": this._lan.capts.spotToolsCaptions("ON") });
                    this.spotTools.push({ "key": "OFF", "val": this._lan.capts.spotToolsCaptions("OFF") });
                }
                else {
                    this.spotTools.push({ "key": tool, "val": this._lan.capts.spotToolsCaptions(tool) });
                }
            }
    };
    shedulerAdd.prototype.setTaskPType = function (event) {
        switch (event.target.value) {
            case "DATE":
                this.renderDateNeed = true;
                break;
        }
    };
    shedulerAdd.prototype.save = function () {
        var _this = this;
        this.onLoading = true;
        this.DATE = this.getTaskDATE();
        if (!this.hw
            || !this.taskPType
            || ["EDAY", "DOW", "DOM", "DATE"].indexOf(this.taskPType) == -1
            || !this.command
            || ["ON", "OFF", "DIM"].indexOf(this.command) == -1
            || !this.title
            || this.title.length == 0
            || (this.taskPType == "DOW" && !parseInt(this.DOW))
            || (this.taskPType == "DOM" && !this.DOM)
            || (this.taskPType == "DATE" && !this.DATE)
            || (this.taskPType == "DIM" && !this.DIM)) {
            this._notify.error(this._lan.__('notFullFilled'));
            this.onLoading = false;
            return;
        }
        var commandKey = null;
        var commandVal = null;
        var dateKey = this.taskPType;
        var dateVal = "1";
        var timeH = parseInt(this.HOURS) || 0;
        var timeM = parseInt(this.MINUTES) || 0;
        var title = this.title;
        switch (this.command) {
            case "ON":
                commandKey = "ONOFF";
                commandVal = "1";
                break;
            case "OFF":
                commandKey = "ONOFF";
                commandVal = "0";
                break;
            case "DIM":
                commandKey = "DIM";
                commandVal = this.DIM;
                break;
        }
        switch (this.taskPType) {
            case "DOW":
                dateVal = this.DOW.toString();
                break;
            case "DOM":
                dateVal = this.DOM.toString();
                break;
            case "DATE":
                dateVal = this.DATE;
                break;
        }
        var spot_sheduler_item = new spotShedulerItem_1.spotShedulerItem(null, this.spot.ID, title, commandKey, commandVal, dateKey, dateVal, this.hw.ID, timeH, timeM, "", //dateYMD()//not cur time
        new Date().getHours());
        this._spotService.shedulerAdd(this.spot, spot_sheduler_item).then(function (success) {
            _this._notify.alert(_this._lan.__('taskAdded'));
            _this.onLoading = false;
            _this.close();
        }, function (fail) {
            _this._notify.error(_this._lan.__('sendToSpotError'));
            _this.onLoading = false;
        });
    };
    shedulerAdd.prototype.close = function () {
        this.shedulerAddEvent.emit('hide');
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', smartySpot_1.smartySpot)
    ], shedulerAdd.prototype, "spot", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], shedulerAdd.prototype, "shedulerAddEvent", void 0);
    shedulerAdd = __decorate([
        core_1.Component({
            selector: 'shedulerAdd',
            templateUrl: './app/views/sheduler/shedulerAdd.html',
            directives: []
        }), 
        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService, datetimepeaker_1.datetimepeaker])
    ], shedulerAdd);
    return shedulerAdd;
}());
exports.shedulerAdd = shedulerAdd;
function dateYMD() {
    var year = "", month = "", day = "", date = new Date();
    year = String(date.getFullYear());
    month = String(date.getMonth() + 1);
    if (month.length == 1) {
        month = "0" + month;
    }
    day = String(date.getDate());
    if (day.length == 1) {
        day = "0" + day;
    }
    return day + "." + month + "." + year;
}
//# sourceMappingURL=shedulerAdd.js.map