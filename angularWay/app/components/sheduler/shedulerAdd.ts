import {Component, OnInit, DoCheck, Input, Output, EventEmitter, AfterViewChecked} from '@angular/core';
import {spotService} from './../../services/spotService';
import {languageService} from '../../services/languageService';
import {notifyService} from '../../services/notifyService';
import {spotShedulerItem} from '../../models/spotShedulerItem';
import {smartySpot} from '../../models/smartySpot';
import {smartySpotHW} from '../../models/smartySpotHW';
import {datetimepeaker} from '../../services/datetimepeaker';

@Component({
    selector: 'shedulerAdd',
    templateUrl: './app/views/sheduler/shedulerAdd.html',
    directives:[]
})
export class shedulerAdd implements OnInit,DoCheck,AfterViewChecked{
	@Input() spot:smartySpot;
	@Output() shedulerAddEvent = new EventEmitter();
    onLoading:boolean;
    hw:smartySpotHW;
    taskhw:string;
    spotTools:Object[];
    taskPType:string;
    DOW:string;
    DOM:string;
    DATE:string;
    HOURS:string;
    MINUTES:string;
    command:string;
    DIM:string;
    title:string;
    shedulerTaskPType:Object[];
    DOWS:Object[];
    avaibleHW:Object[];
    avaibleDOMS:number[];
    avaibleHOURS:number[];
    avaibleMINUTES:number[];
    
    renderDateNeed:boolean;
    constructor(private _spotService:spotService,private _lan:languageService,private _notify:notifyService,private _datetimepeaker:datetimepeaker){
        this.onLoading	= false;
        this.hw	        = null;
        this.taskhw	    = null;
        this.spotTools= [];
        this.taskPType = null;
        this.DOW	  = null;
        this.DOM	  = null;
        this.DATE     = null;
        this.HOURS	  = null;
        this.MINUTES  = null;
        this.command  = null;
        this.DIM      = null;
        this.title    = null;
        this.renderDateNeed = false;
        this.shedulerTaskPType = [];
        this.DOWS	        = [];
        this.avaibleHW	    = [];
        this.avaibleDOMS	= [];
        this.avaibleHOURS	= [];
        this.avaibleMINUTES	= [];
    }
    
	ngOnInit():void{
        this.shedulerTaskPType = this._lan.capts.shedulerTaskPType();
        this.DOWS = this._lan.capts.DOWS();
        for(let item of this.spot.hw){
             this.avaibleHW.push(this._lan.capts.hw(item.type,item.ID));
        }
        for(let i=1;i<=31;i++){
            this.avaibleDOMS.push(i);
        }
        for(let i=0;i<=23;i++){
            this.avaibleHOURS.push(i);
        }
        for(let i=0;i<=55;i+=5){
            this.avaibleMINUTES.push(i);
        }
    }
    ngDoCheck():void{}
    ngAfterViewChecked(): void {
        if(this.renderDateNeed){
            console.log("DATE render");
            this.setTaskDATE(dateYMD());
	        this._datetimepeaker.datepeaker(".shedulerAdd .dateInline");
            this.renderDateNeed = false;
        }
    }
    setTaskDATE(date:string){
        let shedulerAddDATE:any = document.getElementById("shedulerAddDATE");
        if(shedulerAddDATE) shedulerAddDATE.value = date;
    }
    getTaskDATE(){
        let shedulerAddDATE:any = document.getElementById("shedulerAddDATE");
        return shedulerAddDATE?shedulerAddDATE.value:"";
    }
    setHW(event:any){
        if(!event.target.value) return;
            
        this.spotTools = [];
        this.hw = this.spot.hw.find((el:smartySpotHW,indx:number,arr:smartySpotHW[])=>{
            return el.ID == event.target.value;
        });
        if(this.hw)
            for(let tool of this.hw.tools){
                if(tool == "ONOFF"){
                    this.spotTools.push({"key":"ON","val":this._lan.capts.spotToolsCaptions("ON")});
                    this.spotTools.push({"key":"OFF","val":this._lan.capts.spotToolsCaptions("OFF")});
                }else{
                    this.spotTools.push({"key":tool,"val":this._lan.capts.spotToolsCaptions(tool)});
                }
            }
    }
    setTaskPType(event:any):void{
        switch(event.target.value){
            case "DATE":
	            this.renderDateNeed = true;
                break;
        }
    }
    save():void{
        this.onLoading = true;
	    this.DATE = this.getTaskDATE();
	    if(
                !this.hw
                || !this.taskPType
                || ["EDAY","DOW","DOM","DATE"].indexOf(this.taskPType) == -1
                || !this.command
                || ["ON","OFF","DIM"].indexOf(this.command) == -1
                || !this.title
                || this.title.length == 0
                || (this.taskPType == "DOW"	    && !parseInt(this.DOW))
                || (this.taskPType == "DOM"	    && !this.DOM)
                || (this.taskPType == "DATE"	&& !this.DATE)
                || (this.taskPType == "DIM"	    && !this.DIM)
	    ){
            this._notify.error(this._lan.__('notFullFilled'));
            this.onLoading = false;
            return;
        }

        let commandKey:string	= null;
        let commandVal:string	= null;
        let dateKey:string	    = this.taskPType;
        let dateVal:string	    = "1";
        let timeH:number        = parseInt(this.HOURS)	|| 0;
        let timeM:number        = parseInt(this.MINUTES)|| 0;
        let title:string        = this.title;
        
        switch(this.command){
            case "ON":
                commandKey = "ONOFF";
                commandVal = "1";
                break;
            case "OFF":
                commandKey = "ONOFF";
                commandVal = "0";
                break;
            case "DIM":
                commandKey = "DIM";
                commandVal = this.DIM;
                break;
        }

        switch(this.taskPType){
            case "DOW":
                dateVal=this.DOW.toString();
                break;
            case "DOM":
                dateVal=this.DOM.toString();
                break;
            case "DATE":
                dateVal=this.DATE;
                break;
        }
        
        let spot_sheduler_item = new spotShedulerItem(
                null,
                this.spot.ID,
                title,
                commandKey,
                commandVal,
                dateKey,
                dateVal,
                this.hw.ID,
                timeH,
                timeM,
                "",//dateYMD()//not cur time
	            new Date().getHours()
        );
        this._spotService.shedulerAdd(this.spot,spot_sheduler_item).then(success=>{
            this._notify.alert(this._lan.__('taskAdded'));
            this.onLoading = false;
            this.close();
        },fail=>{
            this._notify.error(this._lan.__('sendToSpotError'));
            this.onLoading = false;
        });
    }
    close():void{
        this.shedulerAddEvent.emit('hide');
    }
}

function dateYMD():string{
    let year:string     = "",
        month:string    = "",
        day:string      = "",
        date            = new Date();
    year = String(date.getFullYear());
    month = String(date.getMonth() + 1);
    if (month.length == 1) {
        month = "0" + month;
    }
    day = String(date.getDate());
    if (day.length == 1) {
        day = "0" + day;
    }
    return day+"."+month+"."+year ;
}