import {Component, OnInit, DoCheck, Input, Output, EventEmitter} from '@angular/core';
import {spotService} from './../../services/spotService';
import {languageService} from '../../services/languageService';
import {notifyService} from '../../services/notifyService';
import {spotShedulerItem} from '../../models/spotShedulerItem';
import {smartySpot} from '../../models/smartySpot';
import {shedulerItem} from './shedulerItem';
import {shedulerAdd} from '../sheduler/shedulerAdd';
import {Subscription} from 'rxjs/Subscription';
@Component({
    selector: 'shedulerList',
    templateUrl: './app/views/sheduler/shedulerList.html',
    directives:[shedulerItem]
})
export class shedulerList implements OnInit{
	@Input()	spot:smartySpot;
	@Output()	shedulerListEvent = new EventEmitter();
    onLoading:boolean;
    shedulerItems:spotShedulerItem[];
	_shedulerSubscription:Subscription;
    constructor(private _spotService:spotService,private _lan:languageService,private _notify:notifyService){
        this.onLoading = false;
        this.shedulerItems  = [];
    }
	ngOnInit():void{
        this._shedulerSubscription = this._spotService._sheduler$.subscribe(items => {
            this.shedulerItems = [];
            for(let item of items){
                if(item.spotID == this.spot.ID){
                    this.shedulerItems.push(item);
                }
            }
        });    
	}
    ngOnDestroy() {
	    this._shedulerSubscription.unsubscribe();
    }
    shedulerDeleteEventHandler(shedulerItem:spotShedulerItem):void{
        this.onLoading = true;
        this._spotService.shedulerRemove(this.spot,shedulerItem).then((result:boolean)=>{
            this._notify.alert(this._lan.__('taskRemoved'));
            this.onLoading = false;
        },err=>{
            this._notify.alert(this._lan.__('sendToSpotError'));
            this.onLoading = false;
        });
    }
    hide():void{
        this.shedulerListEvent.emit('hide');
    }
}