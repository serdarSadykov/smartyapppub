import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {spotService} from './../../services/spotService';
import {languageService} from '../../services/languageService';
import {notifyService} from '../../services/notifyService';
import {spotShedulerItem} from '../../models/spotShedulerItem';
import {smartySpot} from '../../models/smartySpot';
import {smartySpotHW} from '../../models/smartySpotHW';

@Component({
    selector: 'shedulerItem',
    templateUrl: './app/views/sheduler/shedulerItem.html',
    directives:[]
})
export class shedulerItem implements OnInit{
    @Input() shedulerItem:spotShedulerItem;
	@Input() spot:smartySpot;
	@Output() shedulerDeleteEvent = new EventEmitter();
    commandTHW:string;
    commandTitle:string;
    commandTAction:string;
    commandDateVal:string;
    commandTimeVal:string;
    constructor(private _spotService:spotService,private _lan:languageService,private _notify:notifyService){
        this.commandTHW = "";
        this.commandTAction = "";
        this.commandDateVal = "";
        this.commandTimeVal = "";
    }
	ngOnInit():void{
        switch(this.shedulerItem.commandKey){
            case "ONOFF":
                this.commandTAction = parseInt(this.shedulerItem.commandVal)?this._lan.__("turnOn"):this._lan.__("turnOff");
                break;
            case "DIM":
                this.commandTAction = this._lan.__("dimmer")+":"+(Math.round(100/255*parseInt(this.shedulerItem.commandVal)))+"%";
                break;
        }
        switch (this.shedulerItem.dateKey) {
            case "EDAY":
                this.commandDateVal = this._lan.__("EDAY");
                break;
            case "DOW":
                this.commandDateVal = this._lan.capts.onDOW(parseInt(this.shedulerItem.dateVal));
                break;
            case "DOM":
                this.commandDateVal = this._lan.__("onDOM").replace("#DOM#",this.shedulerItem.dateVal);
                break;
            case "DATE":
                this.commandDateVal = this.shedulerItem.dateVal;
                break;
            default:
                break;
        }
        this.commandTitle	= this.shedulerItem.title;
	    this.commandTimeVal	= this._lan.__("ONTIME").replace("#TIME#",(this.shedulerItem.timeH < 10?"0"+this.shedulerItem.timeH:this.shedulerItem.timeH)+":"+(this.shedulerItem.timeM < 10?"0"+this.shedulerItem.timeM:this.shedulerItem.timeM));
        let taskHW:smartySpotHW = this.spot.hw.find((el:smartySpotHW)=>{ return el.ID == this.shedulerItem.hw;});
        if(taskHW){
        	let str:any = this._lan.capts.hw(taskHW.type,this.shedulerItem.hw);
            if(str) this.commandTHW = str.val;
        }
    }
    delete():void{
        this.shedulerDeleteEvent.emit(this.shedulerItem);
    }
}