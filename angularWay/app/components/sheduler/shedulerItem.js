"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var spotService_1 = require('./../../services/spotService');
var languageService_1 = require('../../services/languageService');
var notifyService_1 = require('../../services/notifyService');
var spotShedulerItem_1 = require('../../models/spotShedulerItem');
var smartySpot_1 = require('../../models/smartySpot');
var shedulerItem = (function () {
    function shedulerItem(_spotService, _lan, _notify) {
        this._spotService = _spotService;
        this._lan = _lan;
        this._notify = _notify;
        this.shedulerDeleteEvent = new core_1.EventEmitter();
        this.commandTHW = "";
        this.commandTAction = "";
        this.commandDateVal = "";
        this.commandTimeVal = "";
    }
    shedulerItem.prototype.ngOnInit = function () {
        var _this = this;
        switch (this.shedulerItem.commandKey) {
            case "ONOFF":
                this.commandTAction = parseInt(this.shedulerItem.commandVal) ? this._lan.__("turnOn") : this._lan.__("turnOff");
                break;
            case "DIM":
                this.commandTAction = this._lan.__("dimmer") + ":" + (Math.round(100 / 255 * parseInt(this.shedulerItem.commandVal))) + "%";
                break;
        }
        switch (this.shedulerItem.dateKey) {
            case "EDAY":
                this.commandDateVal = this._lan.__("EDAY");
                break;
            case "DOW":
                this.commandDateVal = this._lan.capts.onDOW(parseInt(this.shedulerItem.dateVal));
                break;
            case "DOM":
                this.commandDateVal = this._lan.__("onDOM").replace("#DOM#", this.shedulerItem.dateVal);
                break;
            case "DATE":
                this.commandDateVal = this.shedulerItem.dateVal;
                break;
            default:
                break;
        }
        this.commandTitle = this.shedulerItem.title;
        this.commandTimeVal = this._lan.__("ONTIME").replace("#TIME#", (this.shedulerItem.timeH < 10 ? "0" + this.shedulerItem.timeH : this.shedulerItem.timeH) + ":" + (this.shedulerItem.timeM < 10 ? "0" + this.shedulerItem.timeM : this.shedulerItem.timeM));
        var taskHW = this.spot.hw.find(function (el) { return el.ID == _this.shedulerItem.hw; });
        if (taskHW) {
            var str = this._lan.capts.hw(taskHW.type, this.shedulerItem.hw);
            if (str)
                this.commandTHW = str.val;
        }
    };
    shedulerItem.prototype.delete = function () {
        this.shedulerDeleteEvent.emit(this.shedulerItem);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', spotShedulerItem_1.spotShedulerItem)
    ], shedulerItem.prototype, "shedulerItem", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', smartySpot_1.smartySpot)
    ], shedulerItem.prototype, "spot", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], shedulerItem.prototype, "shedulerDeleteEvent", void 0);
    shedulerItem = __decorate([
        core_1.Component({
            selector: 'shedulerItem',
            templateUrl: './app/views/sheduler/shedulerItem.html',
            directives: []
        }), 
        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService])
    ], shedulerItem);
    return shedulerItem;
}());
exports.shedulerItem = shedulerItem;
//# sourceMappingURL=shedulerItem.js.map