"use strict";
var ru = (function () {
    function ru() {
        this.init = "Инициализация";
        this.spotSearch = "Поиск устройств";
        this.spotConnecting = "Подключение устройства";
        this.spotConnected = "Устройтво подключено, через пару минут оно появится в списке";
        this.spotConfigured = "Устройтво настроено";
        this.spotNewAdded = "Добавлено новое устройство";
        this.spotNotFound = "Устройтво не найдено";
        this.spotRemoved = "Устройтво удалено";
        this.spotReseted = "Устройтво сброшено";
        this.spotRenamedConfirm = "Устройство будет переименовано только на вашем устройстве";
        this.spotRenamed = "Устройтво переименовано";
        this.spotEnterName = "Придумайте название устройства";
        this.spotCantConnect = "Не удалось подключиться к устройству";
        this.needWIFI = "Подключитесь к Вашей домашней/офисной WiFi сети и нажите OK";
        this.spotConnectWiFi = "Выберите Вашу WiFi сеть<br/><small>с доступом в интернет</small>";
        this.spotConnectWiFiNotFound = "Сети WiFi устройством не найдены";
        this.spotConnectWiFiConnecting = "Подключение";
        this.spotConnectWiFiPass = "Введите пароль от WiFi сети";
        this.spotConnectWiFiPassWrong = "Пароль от WiFi сети неверный";
        this.spotConnectWiFiCantConnect = "Не удается подключиться к WiFi сети";
        this.spotConnectWiFiNotSecured = "Это открытая сеть. Для работы необходима сеть с доступом по паролю (WPA2-PSK)";
        this.errorDefault = "Произошла ошибка, попробуйте еще раз";
        this.sendToSpotError = "Не удалось выполнить команду";
        this.turnOn = "Включить";
        this.turnOff = "Выключить";
        this.dimmer = "Яркость";
        this.EDAY = "Ежедневно";
        this.onDOM = "Каждое #DOM# число";
        this.ONTIME = "В #TIME#";
        this.taskAdded = "Задание добавлено";
        this.taskRemoved = "Задание удалено";
        this.notFullFilled = "Не заполнены все необходимые поля";
        this.shedulerAsking = "Обновление списка задач";
        this.shedulerAskingComplete = "Обновление списка задач окончено";
        this.shedulerList = "Задачи";
        this.shedulerAdd = "Новая задача";
        this.localOnly = "Это действие возможно выполнить только находясь в одной сети с устройством";
        this.hwChoose = "Выберите устройство";
        this.light = "Выключатель";
        this.socket = "Розетка";
        this.resetSpot = "Это действие приведет к сбросу устройства. Продлжить?";
        this.notConfiguredNF = "Ненастроенные устройства не найдены. Производится поиск в локальной сети.";
        this.refresh = "Обновление";
        this.confirm = "Вы действительно хотите выполнить действие?";
        this.mqttSettings = "Настройка MQTT";
        this.shedulerAddSelectPlan = "Выберите план";
        this.selectAction = "Выберите дейстивие";
        this.HOURS = "Часов";
        this.MINUTES = "Минут";
        this.selectTIME = "Выберите время";
        this.selectDATE = "Выберите дату";
        this.selectDOM = "Выберите число месяца";
        this.shedulerDOWT1 = "Учтите: Если в месяце нет указанного числа, то задача <strong>в этот месяц выполнена не будет</strong>";
        this.selectDOW = "Выберите день недели";
        this.enterDescription = "Введите описание";
        this.save = "Сохранить";
        this.cancel = "Отменить";
        this.spotAddT1 = "Приложение автоматически продолжит работу после подключения";
        this.refreshIt = "Обновить";
        this.tryAgain = "Попробовать снова";
        this.needWIFICapt = "Для работы устройства требуется наличие WiFi сети <strong>с доступом в интернет и шифрованием WPA2-PSK</strong>";
        this.noLocalWIFI = "Ваше устройство не подключено к домашней/офисной сети WiFi";
        this.connected = "Подключено";
        this.notConnected = "Не подключено";
        this.noTasks = "Задач нет";
    }
    ru.prototype.hw = function (type, key) {
        console.log(type, key);
        var items = {
            light: [
                { "key": 1, "val": "Левая кнопка" },
                { "key": 2, "val": "Правая кнопка" }
            ]
        };
        if (type) {
            if (key) {
                if (items[type]) {
                    return items[type].find(function (el) { return el.key == key; });
                }
                return [];
            }
            else {
                if (items[type]) {
                    return items[type];
                }
                return [];
            }
        }
        return items;
    };
    ;
    ru.prototype.spotToolsCaptions = function (key) {
        var items = {
            "ON": "Включить",
            "OFF": "Отключить",
            "DIM": "Яркость"
        };
        return items[key] ? items[key] : [];
    };
    ;
    ru.prototype.shedulerTaskPType = function () {
        return [
            { "key": "EDAY", "val": "Ежедневно" },
            { "key": "DOW", "val": "День недели" },
            { "key": "DOM", "val": "День месяца" },
            { "key": "DATE", "val": "Определенный день" }
        ];
    };
    ;
    ru.prototype.onDOW = function (dow) {
        var items = [
            "По понедельникам",
            "По вторникам",
            "По средам",
            "По четвергам",
            "По пятницам",
            "По субботам",
            "По воскресеньям"
        ];
        if (dow && items[dow]) {
            return items[dow];
        }
        return "";
    };
    ;
    ru.prototype.DOWS = function () {
        return [
            { "key": "1", "val": "Понедельник" },
            { "key": "2", "val": "Вторник" },
            { "key": "3", "val": "Среда" },
            { "key": "4", "val": "Четверг" },
            { "key": "5", "val": "Пятница" },
            { "key": "6", "val": "Суббота" },
            { "key": "7", "val": "Воскресенье" },
        ];
    };
    ;
    return ru;
}());
exports.ru = ru;
;
//# sourceMappingURL=ru.js.map