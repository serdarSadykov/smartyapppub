import {lang} from "./lang";

export class ru implements lang{
	init:						string="Инициализация";
	spotSearch:					string="Поиск устройств";
	spotConnecting:				string="Подключение устройства";
	spotConnected:				string="Устройтво подключено, через пару минут оно появится в списке";
	spotConfigured:				string="Устройтво настроено";
	spotNewAdded:				string="Добавлено новое устройство";
	spotNotFound:				string="Устройтво не найдено";
	spotRemoved:				string="Устройтво удалено";
	spotReseted:				string="Устройтво сброшено";
	spotRenamedConfirm:			string="Устройство будет переименовано только на вашем устройстве";
	spotRenamed:				string="Устройтво переименовано";
	spotEnterName:				string="Придумайте название устройства";
	spotCantConnect:			string="Не удалось подключиться к устройству";
	needWIFI:					string="Подключитесь к Вашей домашней/офисной WiFi сети и нажите OK";
	spotConnectWiFi:			string="Выберите Вашу WiFi сеть<br/><small>с доступом в интернет</small>";
	spotConnectWiFiNotFound:	string="Сети WiFi устройством не найдены";
	spotConnectWiFiConnecting:	string="Подключение";
	spotConnectWiFiPass:		string="Введите пароль от WiFi сети";
	spotConnectWiFiPassWrong:	string="Пароль от WiFi сети неверный";
	spotConnectWiFiCantConnect:	string="Не удается подключиться к WiFi сети";
	spotConnectWiFiNotSecured:	string="Это открытая сеть. Для работы необходима сеть с доступом по паролю (WPA2-PSK)";
	errorDefault:				string="Произошла ошибка, попробуйте еще раз";
	sendToSpotError:			string="Не удалось выполнить команду";
	turnOn:						string="Включить";
	turnOff:					string="Выключить";
	dimmer:						string="Яркость";
	EDAY:						string="Ежедневно";
	onDOM:						string="Каждое #DOM# число";
	ONTIME:						string="В #TIME#";
	taskAdded:					string="Задание добавлено";
	taskRemoved:				string="Задание удалено";
	notFullFilled:				string="Не заполнены все необходимые поля";
	shedulerAsking:				string="Обновление списка задач";
	shedulerAskingComplete:		string="Обновление списка задач окончено";
	shedulerList:				string="Задачи";
	shedulerAdd:				string="Новая задача";
	localOnly:					string="Это действие возможно выполнить только находясь в одной сети с устройством";
	hwChoose:					string="Выберите устройство";
	light:						string="Выключатель";
	socket:						string="Розетка";
	resetSpot:					string="Это действие приведет к сбросу устройства. Продлжить?";
	notConfiguredNF:			string="Ненастроенные устройства не найдены. Производится поиск в локальной сети.";
	refresh:					string="Обновление";
	confirm:					string="Вы действительно хотите выполнить действие?";
	mqttSettings:				string="Настройка MQTT";
	shedulerAddSelectPlan:		string="Выберите план";
	selectAction:				string="Выберите дейстивие";
	HOURS:						string="Часов";
	MINUTES:					string="Минут";
	selectTIME:					string="Выберите время";
	selectDATE:					string="Выберите дату";
	selectDOM:					string="Выберите число месяца";
	shedulerDOWT1:				string="Учтите: Если в месяце нет указанного числа, то задача <strong>в этот месяц выполнена не будет</strong>";
	selectDOW:					string="Выберите день недели";
	enterDescription:			string="Введите описание";
	save:						string="Сохранить";
	cancel:						string="Отменить";
	spotAddT1:					string="Приложение автоматически продолжит работу после подключения";
	refreshIt:					string="Обновить";
	tryAgain:					string="Попробовать снова";
	needWIFICapt:				string="Для работы устройства требуется наличие WiFi сети <strong>с доступом в интернет и шифрованием WPA2-PSK</strong>";
	noLocalWIFI:				string="Ваше устройство не подключено к домашней/офисной сети WiFi";
	connected:					string="Подключено";
	notConnected:				string="Не подключено";
	noTasks:					string="Задач нет";
	hw(type:string,key:string):Object{
		console.log(type,key);
		let items = {
			light:[
				{"key":1,"val":"Левая кнопка"},
				{"key":2,"val":"Правая кнопка"}
			]
		};
		if(type){
			if(key){
				if(items[type]){
					return items[type].find((el:any)=>{return el.key == key;})
				}
				return [];
			}else{
				if(items[type]){
					return items[type];
				}
				return [];
			}
		}
		return items;
	};
	spotToolsCaptions(key:string):string[]{
		let items = {
			"ON"	:"Включить",
			"OFF"	:"Отключить",
			"DIM"	:"Яркость"
		}

		return items[key]?items[key]:[];
	};
	shedulerTaskPType():Object[]{
		return [
			{"key":"EDAY",	"val":"Ежедневно"},
			{"key":"DOW",	"val":"День недели"},
			{"key":"DOM",	"val":"День месяца"},
			{"key":"DATE",	"val":"Определенный день"}
		];
	};
	onDOW(dow:number):string{
		let items = [
			"По понедельникам",
			"По вторникам",
			"По средам",
			"По четвергам",
			"По пятницам",
			"По субботам",
			"По воскресеньям"
		]
		if(dow && items[dow]){
			return items[dow];
		}
		return "";
	};
	DOWS():Object[]{
		return [
			{"key":"1",	"val":"Понедельник"},
			{"key":"2",	"val":"Вторник"},
			{"key":"3",	"val":"Среда"},
			{"key":"4",	"val":"Четверг"},
			{"key":"5",	"val":"Пятница"},
			{"key":"6",	"val":"Суббота"},
			{"key":"7",	"val":"Воскресенье"},
		];
	};
};