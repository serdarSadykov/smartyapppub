import {smartySpotHW} from "./smartySpotHW";

export class identifyMessage{
    constructor(
		public ID:		string,
		public stype:	string,
		public token:	string,
		public title:	string,
		public ip:  	string,
		public hw:      smartySpotHW[]
	) {
	}
}
//"type"	:"identify",
//"ID"	    :"%s",
//"stype"	:"%s",
//"token"	:"%s",
//"title"	:"%s",
//"ip"	:"%s",
//"hw":[{"ID":1,"type":"light","status":%d,"tools":[%s]},{"ID":1,"type":"light","status":%d,"tools":[%s]}]