
import {smartySpotHW} from "./smartySpotHW";
export class smartySpot{
	WORKS:boolean;
    constructor(
		public ID:			string,
		public token:		string,
		public title:		string,
		public ip:			string,
		public hw:			smartySpotHW[],
		public type:		string,
		public _inLocal:	boolean	= false,
		public _lastConnect:Date	= null,
		public available:boolean	= false
	) {
		if(!this._lastConnect){
			this._lastConnect = new Date();
		}
		this.WORKS = false;
	}
	setIsWorks(){
		this.WORKS  = true;
	}
	setIsNWorks(){
		this.WORKS  = false;
	}
	setInLocal():void{
		this._inLocal = true;
		this.setOnline();
	}
	setNInLocal():void{
		this._inLocal = false;
	}
	setOnline():void{
		this._lastConnect = new Date();
		this.available = true;
	}
	setOffline():void{
		this._inLocal = false;
		this.available = false;
	}
	stringify():string{
        return JSON.stringify({
            "ID":			this.ID,
            "token":		this.token,
            "title":		this.title,
            "ip":			this.ip,
            "hw":			this.hw,
            "type":			this.type,
        });
	}
	toJSON():any{
        return {
            "ID":			this.ID,
            "token":		this.token,
            "title":		this.title,
            "ip":			this.ip,
            "hw":			this.hw,
            "type":			this.type,
        };
	}
}