"use strict";
var spotCommand = (function () {
    function spotCommand(ID, spotID, title, commandKey, commandVal, dateKey, dateVal, hw, timeH, timeM, lastCallS, lastCallH, lastCallM) {
        if (ID === void 0) { ID = null; }
        if (hw === void 0) { hw = 0; }
        if (timeH === void 0) { timeH = 0; }
        if (timeM === void 0) { timeM = 0; }
        if (lastCallS === void 0) { lastCallS = ""; }
        if (lastCallH === void 0) { lastCallH = 0; }
        if (lastCallM === void 0) { lastCallM = 0; }
        this.ID = ID;
        this.spotID = spotID;
        this.title = title;
        this.commandKey = commandKey;
        this.commandVal = commandVal;
        this.dateKey = dateKey;
        this.dateVal = dateVal;
        this.hw = hw;
        this.timeH = timeH;
        this.timeM = timeM;
        this.lastCallS = lastCallS;
        this.lastCallH = lastCallH;
        this.lastCallM = lastCallM;
        if (!this.ID)
            this.ID = this.uniqid();
    }
    spotCommand.prototype.uniqid = function (prefix, moreEntropy) {
        if (prefix === void 0) { prefix = ""; }
        if (moreEntropy === void 0) { moreEntropy = false; }
        var retId;
        var _formatSeed = function (seed, reqWidth) {
            seed = parseInt(seed, 10).toString(16); // to hex str
            if (reqWidth < seed.length) {
                return seed.slice(seed.length - reqWidth);
            }
            if (reqWidth > seed.length) {
                return Array(1 + (reqWidth - seed.length)).join('0') + seed;
            }
            return seed;
        };
        var $locutus = {};
        $locutus.php = {};
        if (!$locutus.php.uniqidSeed) {
            $locutus.php.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
        }
        $locutus.php.uniqidSeed++;
        retId = prefix;
        retId += _formatSeed((new Date().getTime() / 1000, 10).toString(), 8);
        retId += _formatSeed($locutus.php.uniqidSeed, 5);
        if (moreEntropy) {
            retId += (Math.random() * 10).toFixed(8).toString();
        }
        return retId;
    };
    ;
    spotCommand.prototype.stringify = function () {
        return JSON.stringify({
            "ID": this.ID,
            "spotID": this.spotID,
            "title": this.title,
            "commandKey": this.commandKey,
            "commandVal": this.commandVal,
            "dateKey": this.dateKey,
            "dateVal": this.dateVal,
            "hw": this.hw,
            "timeH": this.timeH,
            "timeM": this.timeM,
            "lastCallS": this.lastCallS,
            "lastCallH": this.lastCallH,
            "lastCallM": this.lastCallM
        });
    };
    spotCommand.prototype.toJSON = function () {
        return {
            "ID": this.ID,
            "spotID": this.spotID,
            "title": this.title,
            "commandKey": this.commandKey,
            "commandVal": this.commandVal,
            "dateKey": this.dateKey,
            "dateVal": this.dateVal,
            "hw": this.hw,
            "timeH": this.timeH,
            "timeM": this.timeM,
            "lastCallS": this.lastCallS,
            "lastCallH": this.lastCallH,
            "lastCallM": this.lastCallM
        };
    };
    return spotCommand;
}());
exports.spotCommand = spotCommand;
//# sourceMappingURL=spotCommand.js.map