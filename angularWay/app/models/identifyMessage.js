"use strict";
var identifyMessage = (function () {
    function identifyMessage(ID, stype, token, title, ip, hw) {
        this.ID = ID;
        this.stype = stype;
        this.token = token;
        this.title = title;
        this.ip = ip;
        this.hw = hw;
    }
    return identifyMessage;
}());
exports.identifyMessage = identifyMessage;
//"type"	:"identify",
//"ID"	    :"%s",
//"stype"	:"%s",
//"token"	:"%s",
//"title"	:"%s",
//"ip"	:"%s",
//"hw":[{"ID":1,"type":"light","status":%d,"tools":[%s]},{"ID":1,"type":"light","status":%d,"tools":[%s]}] 
//# sourceMappingURL=identifyMessage.js.map