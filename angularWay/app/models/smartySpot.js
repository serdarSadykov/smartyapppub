"use strict";
var smartySpot = (function () {
    function smartySpot(ID, token, title, ip, hw, type, _inLocal, _lastConnect, available) {
        if (_inLocal === void 0) { _inLocal = false; }
        if (_lastConnect === void 0) { _lastConnect = null; }
        if (available === void 0) { available = false; }
        this.ID = ID;
        this.token = token;
        this.title = title;
        this.ip = ip;
        this.hw = hw;
        this.type = type;
        this._inLocal = _inLocal;
        this._lastConnect = _lastConnect;
        this.available = available;
        if (!this._lastConnect) {
            this._lastConnect = new Date();
        }
        this.WORKS = false;
    }
    smartySpot.prototype.setIsWorks = function () {
        this.WORKS = true;
    };
    smartySpot.prototype.setIsNWorks = function () {
        this.WORKS = false;
    };
    smartySpot.prototype.setInLocal = function () {
        this._inLocal = true;
        this.setOnline();
    };
    smartySpot.prototype.setNInLocal = function () {
        this._inLocal = false;
    };
    smartySpot.prototype.setOnline = function () {
        this._lastConnect = new Date();
        this.available = true;
    };
    smartySpot.prototype.setOffline = function () {
        this._inLocal = false;
        this.available = false;
    };
    smartySpot.prototype.stringify = function () {
        return JSON.stringify({
            "ID": this.ID,
            "token": this.token,
            "title": this.title,
            "ip": this.ip,
            "hw": this.hw,
            "type": this.type,
        });
    };
    smartySpot.prototype.toJSON = function () {
        return {
            "ID": this.ID,
            "token": this.token,
            "title": this.title,
            "ip": this.ip,
            "hw": this.hw,
            "type": this.type,
        };
    };
    return smartySpot;
}());
exports.smartySpot = smartySpot;
//# sourceMappingURL=smartySpot.js.map