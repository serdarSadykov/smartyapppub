export class spotCommand{
    constructor(
        public ID:			string = null,
        public spotID:		string,
        public title:		string,
        public commandKey:	string,
        public commandVal:	string,
        public dateKey:	    string,
        public dateVal:	    string,
        public hw:			number = 0,
        public timeH:		number = 0,
        public timeM:		number = 0,
        public lastCallS:	string = "",
        public lastCallH:	number = 0,
        public lastCallM:	number = 0
	){
        if(!this.ID) this.ID = this.uniqid();
    }

    uniqid(prefix:string="", moreEntropy:boolean = false):string{
        let retId:string;
        var _formatSeed = function (seed:string, reqWidth:number) {
            seed = parseInt(seed, 10).toString(16) // to hex str
            if (reqWidth < seed.length) {
                return seed.slice(seed.length - reqWidth)
            }
            if (reqWidth > seed.length) {
                return Array(1 + (reqWidth - seed.length)).join('0') + seed
            }
            return seed
        }
        let $locutus:any = {};
        $locutus.php = {}
        if(!$locutus.php.uniqidSeed) {$locutus.php.uniqidSeed = Math.floor(Math.random() * 0x75bcd15)}
        $locutus.php.uniqidSeed++
        retId = prefix
        retId += _formatSeed((new Date().getTime() / 1000, 10).toString(), 8)
        retId += _formatSeed($locutus.php.uniqidSeed, 5)
        if (moreEntropy) {
            retId += (Math.random() * 10).toFixed(8).toString()
        }
        return retId
    };
    stringify():string{
        return JSON.stringify({
            "ID":			this.ID,
            "spotID":		this.spotID,
            "title":		this.title,
            "commandKey":	this.commandKey,
            "commandVal":	this.commandVal,
            "dateKey":		this.dateKey,
            "dateVal":		this.dateVal,
            "hw":			this.hw,
            "timeH":		this.timeH,
            "timeM":		this.timeM,
            "lastCallS":	this.lastCallS,
            "lastCallH":	this.lastCallH,
            "lastCallM":	this.lastCallM
        });
    }
    toJSON():any{
        return {
            "ID":			this.ID,
            "spotID":		this.spotID,
            "title":		this.title,
            "commandKey":	this.commandKey,
            "commandVal":	this.commandVal,
            "dateKey":		this.dateKey,
            "dateVal":		this.dateVal,
            "hw":			this.hw,
            "timeH":		this.timeH,
            "timeM":		this.timeM,
            "lastCallS":	this.lastCallS,
            "lastCallH":	this.lastCallH,
            "lastCallM":	this.lastCallM
        };
    }
}