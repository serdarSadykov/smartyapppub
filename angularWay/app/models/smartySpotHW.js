"use strict";
var smartySpotHW = (function () {
    function smartySpotHW(ID, type, state, tools) {
        this.ID = ID;
        this.type = type;
        this.state = state;
        this.tools = tools;
    }
    smartySpotHW.prototype.stringify = function () {
        return JSON.stringify({
            "ID": this.ID,
            "type": this.type,
            "state": this.state,
            "tools": this.tools,
        });
    };
    smartySpotHW.prototype.toJSON = function () {
        return {
            "ID": this.ID,
            "type": this.type,
            "state": this.state,
            "tools": this.tools,
        };
    };
    return smartySpotHW;
}());
exports.smartySpotHW = smartySpotHW;
//# sourceMappingURL=smartySpotHW.js.map