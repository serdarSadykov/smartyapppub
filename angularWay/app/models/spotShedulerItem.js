"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var spotCommand_1 = require("./spotCommand");
var spotShedulerItem = (function (_super) {
    __extends(spotShedulerItem, _super);
    function spotShedulerItem() {
        _super.apply(this, arguments);
    }
    return spotShedulerItem;
}(spotCommand_1.spotCommand));
exports.spotShedulerItem = spotShedulerItem;
//# sourceMappingURL=spotShedulerItem.js.map