
export class smartySpotHW{
    constructor(
		public ID:		number,
		public type:	string,
		public state:	number,
		public tools:	string[]
	) {
	}
	stringify(){
        return JSON.stringify({
            "ID":			this.ID,
            "type":			this.type,
            "state":		this.state,
            "tools":		this.tools,
        });
	}
	toJSON():any{
        return {
            "ID":			this.ID,
            "type":			this.type,
            "state":		this.state,
            "tools":		this.tools,
        };
	}
}