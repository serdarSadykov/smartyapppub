import {lang} from '../language/lang';
import {en} from '../language/en';
import {ru} from '../language/ru';

export class languageService{
    capts:lang;
	constructor() {
	    let language:string = "ru";
	    switch(language){
        case "ru":
            this.capts = new ru();
            break;
        case "en":
            this.capts = new en();
            break;
        default:
            this.capts = new en();
        }
	}
    __(key:string):string{
        return this.capts[key];
    }
}