import {OnInit} from '@angular/core';
declare var $:any;

export class notifyService{
    error(text:string,title:string = "Внимание"):void{
        let errorModal = $('#errorModal');
        errorModal.find(".val").html(text);
        errorModal.find(".modal-title").html(title);
        errorModal.modal('show');
    }
    alert(text:string,title:string = "Внимание"):void{
        let notifModal = $('#notifModal');
        notifModal.find(".val").html(text);
        notifModal.find(".modal-title").html(title);
        notifModal.modal('show');
    }
}