"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var smartySpot_1 = require('../models/smartySpot');
var smartySpotHW_1 = require('../models/smartySpotHW');
var spotCommand_1 = require('../models/spotCommand');
var spotShedulerItem_1 = require('../models/spotShedulerItem');
var identifyMessage_1 = require('../models/identifyMessage');
var BehaviorSubject_1 = require('rxjs/BehaviorSubject');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
require('rxjs/add/operator/switchMap');
require('rxjs/add/operator/timeout');
require('rxjs/add/operator/toPromise');
core_1.Injectable();
var spotService = (function () {
    function spotService(http) {
        var _this = this;
        this.http = http;
        this.spotsSource = new BehaviorSubject_1.BehaviorSubject(null);
        this._spot$ = this.spotsSource.asObservable();
        this.shedulerSource = new BehaviorSubject_1.BehaviorSubject(null);
        this._sheduler$ = this.shedulerSource.asObservable();
        this.serverAddr = "http://smarty.ssadykov.com";
        this.spotsLocalGet().then(function (data) { if (data)
            _this.spotsSource.next(data); });
        this.shedulerLocalGet().then(function (data) { if (data)
            _this.shedulerSource.next(data); });
        this._sheduler$.subscribe(function (items) { _this.shedulerLocalSave(); });
        this.corExe("receiveFromSpotDaemon", []).then(function (data) { }, function (err) { });
        this.loopJobs();
        this.udpListner();
    }
    spotService.prototype.loopJobs = function () {
        var _this = this;
        var items = this.spotsSource.getValue() || [];
        var has_changes = false;
        var curTimestamp = (new Date().getTime());
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            if (item.available && curTimestamp - item._lastConnect.getTime() > 20000) {
                has_changes = true;
                item.setOffline();
            }
            if (!item._inLocal) {
                this.getServer(item, new http_1.URLSearchParams()).then(function (sdata) {
                    try {
                        if (sdata)
                            _this.proceedServerMessage(JSON.parse(sdata));
                    }
                    catch (exc) { }
                }, function (err) {
                    console.log("[getServer] err");
                }).catch(function (exc) { });
            }
            this.checkInLocal(item);
        }
        if (has_changes) {
            this.spotsSource.next(items);
        }
        setTimeout(function () { return _this.loopJobs(); }, 1000);
    };
    //--
    spotService.prototype.shedulerRemove = function (_smartySpot, _shedulerItem) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", "shedulerRemove", _shedulerItem.ID, "NOW", "")).then(function (success) {
                var sitems = _this.shedulerSource.getValue() || [];
                var nitems = [];
                for (var _i = 0, sitems_1 = sitems; _i < sitems_1.length; _i++) {
                    var item = sitems_1[_i];
                    if (item.ID != _shedulerItem.ID) {
                        nitems.push(item);
                    }
                }
                _this.shedulerSource.next(nitems);
                resolve(true);
            }, function (fail) {
                reject();
            });
        });
    };
    spotService.prototype.shedulerReset = function (_smartySpot) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", "shedulerReset", "", "NOW", "")).then(function (success) {
                var items = _this.shedulerSource.getValue() || [];
                var nitems = [];
                for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
                    var item = items_2[_i];
                    if (item.spotID != _smartySpot.ID) {
                        nitems.push(item);
                    }
                }
                _this.shedulerSource.next(nitems);
                resolve(true);
            }, function (fail) {
                reject();
            });
        });
    };
    spotService.prototype.shedulerRefresh = function (_smartySpot) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var actuality = _this.getInfo(_smartySpot, "shedulerList", _this.getShedulerActuality(_smartySpot)).then(function (sdata) {
                var sitems = _this.shedulerSource.getValue() || [];
                var nitems = [];
                for (var _i = 0, sitems_2 = sitems; _i < sitems_2.length; _i++) {
                    var item = sitems_2[_i];
                    if (item.spotID != _smartySpot.ID) {
                        nitems.push(item);
                    }
                }
                var rdata = JSON.parse(sdata.replace(/(?:\\f|\f+)+/g, "||").replace(/(?:\\[rn]|[\r\n]+)+/g, "|"));
                if (rdata.items) {
                    var citems = rdata.items.trim().split('||');
                    if (citems)
                        for (var i in citems) {
                            if (citems[i]) {
                                var citem = citems[i].split('|').slice(-13);
                                nitems.push(new spotShedulerItem_1.spotShedulerItem(citem[0], citem[1], citem[2], citem[3], citem[4], citem[5], citem[6], citem[7], citem[8], citem[9], citem[10], citem[11], citem[12]));
                            }
                        }
                }
                _this.shedulerSource.next(nitems);
                _this.setShedulerActuality(_smartySpot);
                resolve(true);
            }, function (fail) {
                reject();
            });
        });
    };
    spotService.prototype.shedulerLocalGet = function () {
        return new Promise(function (resolve) {
            var items = [];
            var source_str = localStorage.getItem("spotsSheduler");
            if (source_str) {
                var sitems = JSON.parse(source_str);
                for (var _i = 0, sitems_3 = sitems; _i < sitems_3.length; _i++) {
                    var item = sitems_3[_i];
                    items.push(new spotShedulerItem_1.spotShedulerItem(item.ID, item.spotID, item.title, item.commandKey, item.commandVal, item.dateKey, item.dateVal, item.hw, item.timeH, item.timeM, item.lastCallS, item.lastCallH, item.lastCallM));
                }
            }
            resolve(items);
        });
    };
    spotService.prototype.shedulerLocalSave = function () {
        var items = this.shedulerSource.getValue();
        if (items) {
            var result = [];
            for (var _i = 0, items_3 = items; _i < items_3.length; _i++) {
                var item = items_3[_i];
                result.push(item.toJSON());
            }
            localStorage.setItem("spotsSheduler", JSON.stringify(result));
        }
    };
    spotService.prototype.shedulerAdd = function (_smartySpot, _shedulerItem) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.sendCommandToSpot(_smartySpot, _shedulerItem).then(function (success) {
                var items = _this.shedulerSource.getValue() || [];
                items.push(_shedulerItem);
                _this.shedulerSource.next(items);
                //console.log("RESOLVE");
                resolve(success);
            }, function (fail) {
                //console.log("reject");
                reject();
            });
        });
    };
    spotService.prototype.setShedulerActuality = function (_smartySpot, actuality) {
        if (actuality === void 0) { actuality = 0; }
        if (localStorage.getItem("shedulerActuality")) {
            var shedulerSpots = JSON.parse(localStorage.getItem("shedulerActuality")) || [];
            shedulerSpots[_smartySpot.ID] = actuality ? actuality : (new Date().getTime() / 1000);
            localStorage.setItem("shedulerActuality", JSON.stringify(shedulerSpots));
        }
    };
    spotService.prototype.getShedulerActuality = function (_smartySpot) {
        var actuality = 0;
        if (localStorage.getItem("shedulerActuality")) {
            var shedulerSpots = JSON.parse(localStorage.getItem("shedulerActuality")) || [];
            if (shedulerSpots[_smartySpot.ID]) {
                actuality = shedulerSpots[_smartySpot.ID];
            }
        }
        return actuality;
    };
    //--
    spotService.prototype.spotsLocalSave = function (items) {
        if (items === void 0) { items = []; }
        var result = [];
        if (!items || items.length == 0)
            items = this.spotsSource.getValue();
        if (items) {
            for (var _i = 0, items_4 = items; _i < items_4.length; _i++) {
                var item = items_4[_i];
                result.push(item.toJSON());
            }
            localStorage.setItem("smartySpot", JSON.stringify(result));
        }
    };
    spotService.prototype.spotsLocalGet = function () {
        return new Promise(function (resolve) {
            var result = [];
            var storage = localStorage.getItem("smartySpot");
            if (storage) {
                var items = JSON.parse(storage);
                for (var _i = 0, items_5 = items; _i < items_5.length; _i++) {
                    var item = items_5[_i];
                    var smarty_SpotHWS = [];
                    for (var _a = 0, _b = item.hw; _a < _b.length; _a++) {
                        var hwitem = _b[_a];
                        smarty_SpotHWS.push(new smartySpotHW_1.smartySpotHW(hwitem.ID, hwitem.type, hwitem.state, hwitem.tools));
                    }
                    result.push(new smartySpot_1.smartySpot(item.ID, item.token, item.title, item.ip, smarty_SpotHWS, item.type));
                }
            }
            result.push(new smartySpot_1.smartySpot("123", "12ss3", "Новый жлемент", "192.168.1.1", [new smartySpotHW_1.smartySpotHW(1, "socket", 255, ["ONOFF", "DIM"]), new smartySpotHW_1.smartySpotHW(2, "socket", 255, ["ONOFF", "DIM"])], "socket"));
            resolve(result);
        });
    };
    spotService.prototype.setSpotState = function (_smartySpot, state, tool) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var items = _this.spotsSource.getValue() || [];
            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", tool, state.toString(), "NOW", "0")).then(function (success) {
                if (parseInt(success) == _smartySpot.hw.length) {
                    for (var _i = 0, _a = _smartySpot.hw; _i < _a.length; _i++) {
                        var item = _a[_i];
                        item.state = state;
                    }
                    _this.spotsSource.next(items);
                    resolve(true);
                }
                else {
                    reject(false);
                }
            }, function (fail) {
                reject(false);
            });
        });
    };
    spotService.prototype.setSpotHWState = function (_smartySpot, _smartySpotHW, state, tool) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var items = _this.spotsSource.getValue() || [];
            var item = items.find(function (el) { return el.ID == _smartySpot.ID; });
            var target;
            if (item)
                target = item.hw.find(function (el) { return el.ID == _smartySpotHW.ID; });
            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", tool, state.toString(), "NOW", "0", _smartySpotHW.ID)).then(function (success) {
                if (target) {
                    target.state = state;
                    _this.spotsSource.next(items);
                }
                resolve(true);
            }, function (fail) {
                //console.log("fail");
                reject(false);
            });
        });
    };
    spotService.prototype.setSpotTitle = function (_smartySpot, title) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var items = _this.spotsSource.getValue() || [];
            for (var indx in items) {
                if (items[indx].ID == _smartySpot.ID) {
                    items[indx].title = title;
                }
            }
            _this.spotsSource.next(items);
            _this.spotsLocalSave(items);
            resolve(true);
        });
    };
    spotService.prototype._removeSpot = function (_smartySpot) {
        var sitems = this.spotsSource.getValue() || [];
        var nitems = [];
        for (var _i = 0, sitems_4 = sitems; _i < sitems_4.length; _i++) {
            var item = sitems_4[_i];
            if (item.ID != _smartySpot.ID) {
                nitems.push(item);
            }
        }
        this.spotsSource.next(nitems);
        this.spotsLocalSave(nitems);
    };
    spotService.prototype.resetSpot = function (_smartySpot) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", "resetSpot", "1", "NOW", "")).then(function (success) { }, function (fail) { });
            setTimeout(function () {
                _this._removeSpot(_smartySpot);
                resolve(true);
            }, 2000);
        });
    };
    spotService.prototype.removeSpot = function (_smartySpot) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._removeSpot(_smartySpot);
            resolve(true);
        });
    };
    spotService.prototype.spotsIdentify = function () {
        this.sendCommandBroadcast("identify", "1");
    };
    spotService.prototype.proceedServerMessage = function (message) {
        try {
            if (message.type == "identify") {
                var spot_items = this.spotsSource.getValue() || [];
                if (spot_items.length > 0) {
                    var spot_item = spot_items.find(function (el) { return el.ID == message.ID; });
                    if (spot_item) {
                        var smarty_SpotHWS = [];
                        for (var indx in message.hw) {
                            smarty_SpotHWS.push(new smartySpotHW_1.smartySpotHW(message.hw[indx].ID, message.hw[indx].type, message.hw[indx].state, message.hw[indx].tools));
                        }
                        var identifyM = new identifyMessage_1.identifyMessage(message.ID, message.stype, message.token, message.title, message.ip, smarty_SpotHWS);
                        spot_item.setOnline();
                        spot_item.ip = identifyM.ip;
                        var _loop_1 = function(hw) {
                            var rhw = identifyM.hw.find(function (el) { return el.ID == hw.ID; });
                            if (rhw)
                                hw.state = rhw.state;
                        };
                        for (var _i = 0, _a = spot_item.hw; _i < _a.length; _i++) {
                            var hw = _a[_i];
                            _loop_1(hw);
                        }
                        this.spotsSource.next(spot_items);
                    }
                }
            }
            else {
            }
        }
        catch (exc) { }
    };
    spotService.prototype.udpListner = function () {
        var _this = this;
        this.corExe("recFromSpot", []).then(function (data) {
            var messages = JSON.parse(data);
            var spot_items = _this.spotsSource.getValue() || [];
            var has_changes = false;
            var _loop_2 = function(message) {
                try {
                    if (message.type == "identify") {
                        var spot_item = void 0;
                        var need_register = true;
                        var smarty_SpotHWS = [];
                        for (var indx in message.hw) {
                            smarty_SpotHWS.push(new smartySpotHW_1.smartySpotHW(message.hw[indx].ID, message.hw[indx].type, message.hw[indx].state, message.hw[indx].tools));
                        }
                        var identifyM_1 = new identifyMessage_1.identifyMessage(message.ID, message.stype, message.token, message.title, message.ip, smarty_SpotHWS);
                        if (spot_items.length > 0) {
                            spot_item = spot_items.find(function (el) { return el.ID == identifyM_1.ID; });
                            if (spot_item) {
                                has_changes = true;
                                need_register = false;
                                spot_item.setInLocal();
                                spot_item.ip = identifyM_1.ip;
                                var _loop_3 = function(hw) {
                                    var rhw = identifyM_1.hw.find(function (el) { return el.ID == hw.ID; });
                                    if (rhw)
                                        hw.state = rhw.state;
                                };
                                for (var _i = 0, _a = spot_item.hw; _i < _a.length; _i++) {
                                    var hw = _a[_i];
                                    _loop_3(hw);
                                }
                            }
                        }
                        if (need_register) {
                            _this.registerNewSpot(identifyM_1);
                        }
                    }
                    else {
                        console.log("m1", messages);
                    }
                }
                catch (exc) {
                    alert("UDPL: " + exc.message);
                }
            };
            for (var _b = 0, messages_1 = messages; _b < messages_1.length; _b++) {
                var message = messages_1[_b];
                _loop_2(message);
            }
            if (has_changes) {
                _this.spotsSource.next(spot_items);
            }
            setTimeout(function () { _this.udpListner(); }, 500);
        }, function (fail) { setTimeout(function () { _this.udpListner(); }, 500); });
    };
    spotService.prototype.registerNewSpot = function (_identifyMessage) {
        var items = this.spotsSource.getValue() || [];
        items.push(new smartySpot_1.smartySpot(_identifyMessage.ID, _identifyMessage.token, _identifyMessage.title, _identifyMessage.ip, _identifyMessage.hw, _identifyMessage.stype, true, new Date(), true));
        this.spotsSource.next(items);
        this.spotsLocalSave(items);
    };
    spotService.prototype.getInfo = function (_smartySpot, infoType, actuality) {
        var _this = this;
        if (actuality === void 0) { actuality = 0; }
        return new Promise(function (resolve, reject) {
            var query = new http_1.URLSearchParams();
            query.set(infoType, "1");
            if (actuality)
                query.set("actuality", actuality.toString());
            if (_smartySpot._inLocal) {
                _this.sendLocal(_smartySpot, query, "/getInfo").then(function (data) { return resolve(data); }, function (err) { return reject(err); });
            }
            else {
                _this.getServer(_smartySpot, query, "/spot/getconf").then(function (data) { return resolve(data); }, function (err) { return reject(err); });
            }
        });
    };
    spotService.prototype.setConfig = function (_smartySpot, params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var query = new http_1.URLSearchParams();
            for (var indx in params) {
                query.set(indx, params[indx]);
            }
            if (_smartySpot._inLocal) {
                _this.sendLocal(_smartySpot, query, "/setConfig").then(function (data) { resolve(data == "1"); }, function (err) { return reject(err); });
            }
            else {
                reject();
            }
        });
    };
    spotService.prototype.sendCommandToSpot = function (_smartySpot, _spotCommand) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var query = new http_1.URLSearchParams();
            query.set("command", _spotCommand.stringify());
            if (_smartySpot._inLocal) {
                _this.sendLocal(_smartySpot, query, "/exeCommand").then(function (data) { if (parseInt(data))
                    resolve(data);
                else
                    reject(data); }, function (err) { return reject(err); });
            }
            else {
                var waitComplete = _spotCommand.dateKey == "NOW";
                _this.sendServer(_smartySpot, query, "/spot/write", (_smartySpot.ID + "_" + _spotCommand.ID), waitComplete).then(function (data) { if (parseInt(data))
                    resolve(data);
                else
                    reject(data); }, function (err) { reject(err); });
            }
        });
    };
    spotService.prototype.sendCommandBroadcast = function (commandKey, commandVal) {
        var spot_command = new spotCommand_1.spotCommand("", "", "", commandKey, commandVal, "", "");
        this.corExe("sendUDP", [spot_command.stringify()]).then(function () { }, function () {
            console.log("sendUDP rejected");
        });
    };
    spotService.prototype.sendLocal = function (_smartySpot, query, path) {
        var _this = this;
        if (path === void 0) { path = "/exeCommand"; }
        return new Promise(function (resolve, reject) {
            _this.http.get("http://" + _smartySpot.ip + path, { search: query })
                .map(function (res) { return res.text(); })
                .timeout(10000, function () { reject(); })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) { return reject(err); }); //can asPromise but zblsya
        });
    };
    spotService.prototype.sendServer = function (_smartySpot, query, path, key, waitComplete) {
        var _this = this;
        if (path === void 0) { path = "/spot/write"; }
        if (waitComplete === void 0) { waitComplete = false; }
        return new Promise(function (resolve, reject) {
            query.set("spot", _smartySpot.ID);
            query.set("token", _smartySpot.token);
            var checkAddr = "/spot/check";
            var server_scommand = new serverSCommand(key, _this.serverAddr, path, query, waitComplete, checkAddr, _this.http);
            server_scommand.send().then(function (data) { resolve(data); }, function (err) { reject(err); });
        });
    };
    spotService.prototype.getServer = function (_smartySpot, query, path) {
        var _this = this;
        if (path === void 0) { path = "/spot/messages"; }
        return new Promise(function (resolve, reject) {
            var headers = new http_1.Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            query.set("spot", _smartySpot.ID);
            query.set("token", _smartySpot.token);
            _this.http.post(_this.serverAddr + path, query.toString(), { headers: headers })
                .map(function (res) { return res.json(); })
                .timeout(10000, function () { reject(); })
                .subscribe(function (data) { if (data.result)
                resolve(data.content);
            else
                reject(data); }, function (err) { return reject(); });
        });
    };
    spotService.prototype.corExe = function (action, params) {
        if (params === void 0) { params = null; }
        return new Promise(function (resolve, reject) {
            params = params || [];
            if (cordova) {
                cordova.exec(function (data) {
                    resolve(data);
                }, function (message) {
                    reject(message);
                }, "smartyPoint", action, params);
            }
            else {
                console.log("no Cordova");
                reject("no Cordova");
            }
        });
    };
    spotService.prototype.checkInLocal = function (_smartySpot) {
        var _this = this;
        var req = new XMLHttpRequest();
        var item = (this.spotsSource.getValue() || []).find(function (el) { return el.ID == _smartySpot.ID; });
        if (item) {
            try {
                req.timeout = 3000;
                req.onreadystatechange = function () {
                    if (req.readyState === req.HEADERS_RECEIVED) {
                        req.abort();
                        if (req.status > 0) {
                            var items = _this.spotsSource.getValue() || [];
                            var item_1 = items.find(function (el) { return el.ID == _smartySpot.ID; });
                            if (item_1) {
                                item_1.setInLocal();
                                _this.spotsSource.next(items);
                            }
                        }
                    }
                };
                var onerror_1 = function () {
                    var items = _this.spotsSource.getValue() || [];
                    var item = items.find(function (el) { return el.ID == _smartySpot.ID; });
                    if (item) {
                        item.setNInLocal();
                        _this.spotsSource.next(items);
                    }
                };
                req.onerror = onerror_1;
                req.ontimeout = onerror_1;
                req.open("GET", "http://" + item.ip, true);
                req.send(null);
            }
            catch (exc) { }
        }
    };
    spotService.prototype.hideApp = function () {
        document.dispatchEvent(new CustomEvent("hideApp", {}));
    };
    spotService = __decorate([
        __param(0, core_1.Inject(http_1.Http)), 
        __metadata('design:paramtypes', [http_1.Http])
    ], spotService);
    return spotService;
}());
exports.spotService = spotService;
var serverSCommand = (function () {
    function serverSCommand(key, server, path, query, waitComplete, checkAddr, http) {
        this.key = key;
        this.server = server;
        this.path = path;
        this.query = query;
        this.waitComplete = waitComplete;
        this.checkAddr = checkAddr;
        this.http = http;
        this.timeout = 50000;
        this.commandAddr = this.server + this.path;
        this.checkAddr = this.server + this.checkAddr;
        this.headers = new http_1.Headers();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }
    serverSCommand.prototype.send = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.commandAddr, _this.query.toString(), { headers: _this.headers })
                .map(function (res) { return res.json(); })
                .timeout(10000, function () { reject(); })
                .subscribe(function (data1) {
                if (data1.result) {
                    if (_this.waitComplete) {
                        _this.checker().then(function (data) { resolve(data); }, function (err) { reject(err); });
                    }
                    else {
                        resolve(data1.result);
                    }
                }
                else {
                    reject();
                }
            }, function (err) { return reject(err); });
        });
    };
    serverSCommand.prototype.checker = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.dateBegin = new Date();
            var query = new http_1.URLSearchParams();
            query.set("key", _this.key);
            var loop = function () {
                if (new Date().getTime() - _this.dateBegin.getTime() < _this.timeout) {
                    _this.http.post(_this.checkAddr, query.toString(), { headers: _this.headers })
                        .map(function (res) { return res.json(); })
                        .timeout(10000, function () {
                        //console.log("timeout1")
                    })
                        .subscribe(function (data) {
                        if (data.result && (!_this.waitComplete || data.message == "comlete")) {
                            //console.log("comlete",data.content);
                            resolve(data.content);
                        }
                        else {
                            setTimeout(loop, 3000);
                        }
                    }, function (err) { return setTimeout(loop, 3000); });
                }
                else {
                    //console.log("timeout");
                    reject("timeout");
                }
            };
            loop();
        });
    };
    return serverSCommand;
}());
//# sourceMappingURL=spotService.js.map