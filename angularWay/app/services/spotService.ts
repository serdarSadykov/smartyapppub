import {Injectable,Inject,EventEmitter} from '@angular/core';
import {smartySpot} from '../models/smartySpot';
import {smartySpotHW} from '../models/smartySpotHW';
import {spotCommand} from '../models/spotCommand';
import {spotShedulerItem} from '../models/spotShedulerItem';
import {identifyMessage} from '../models/identifyMessage';
import {BehaviorSubject}    from 'rxjs/BehaviorSubject';
import {Http, Headers, Response, URLSearchParams} from '@angular/http';
import {Observable, Subject, ReplaySubject} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';

declare var cordova:any;

Injectable()
export class spotService{
    private serverAddr:string;
    private spotsSource: BehaviorSubject<smartySpot[]> = new BehaviorSubject<smartySpot[]>(null);
	_spot$:Observable<smartySpot[]> = this.spotsSource.asObservable();

    private shedulerSource: BehaviorSubject<spotShedulerItem[]> = new BehaviorSubject<spotShedulerItem[]>(null);
	_sheduler$:Observable<spotShedulerItem[]> = this.shedulerSource.asObservable();

    constructor(@Inject(Http) private http:Http){
        this.serverAddr = "http://smarty.ssadykov.com"
        this.spotsLocalGet().then(data=>{if(data) this.spotsSource.next(data); });
        this.shedulerLocalGet().then(data=>{if(data) this.shedulerSource.next(data);})
   
        this._sheduler$.subscribe(items=>{this.shedulerLocalSave();});
		this.corExe("receiveFromSpotDaemon",[]).then(data=>{},err=>{});
        this.loopJobs();
        this.udpListner();
    }
    
    loopJobs():void{
	    let items:smartySpot[]	= this.spotsSource.getValue() || [];
        let has_changes:boolean	= false;
        let curTimestamp:number	= (new Date().getTime());
        for(let item of items){
            if(item.available && curTimestamp - item._lastConnect.getTime() > 20000){
                has_changes = true;
	            item.setOffline();
            }
            if(!item._inLocal){
                this.getServer(item,new URLSearchParams()).then(sdata=>{
                    try{
                        if(sdata) this.proceedServerMessage(JSON.parse(sdata));
                    }catch(exc){}
                },err=>{
                    console.log("[getServer] err");
                }).catch((exc)=>{});
            }
            this.checkInLocal(item);
        }
        if(has_changes){
            this.spotsSource.next(items)
        }
	    setTimeout(()=>this.loopJobs(),1000);
    }
    //--
    shedulerRemove(_smartySpot:smartySpot,_shedulerItem:spotShedulerItem):Promise<boolean>{
        return new Promise((resolve,reject) => {
            this.sendCommandToSpot(_smartySpot,new spotCommand(null,_smartySpot.ID,"","shedulerRemove",_shedulerItem.ID,"NOW","")).then(
                success=>{
                    let sitems:spotShedulerItem[] = this.shedulerSource.getValue() || [];
                    let nitems:spotShedulerItem[] = [];
                    for(let item of sitems){
                        if(item.ID != _shedulerItem.ID){
                            nitems.push(item);
                        }
                    }
                    this.shedulerSource.next(nitems);
                    resolve(true);
                },
                fail=>{
                    reject();
                }
            )
        });
    }
    shedulerReset(_smartySpot:smartySpot):Promise<boolean>{
        return new Promise((resolve,reject) => {
            this.sendCommandToSpot(_smartySpot,new spotCommand(null,_smartySpot.ID,"","shedulerReset","","NOW","")).then(
                success=>{
                    let items:spotShedulerItem[] = this.shedulerSource.getValue() || [];
                    let nitems:spotShedulerItem[] = [];
                    for(let item of items){
                        if(item.spotID != _smartySpot.ID){
                            nitems.push(item);
                        }
                    }
                    this.shedulerSource.next(nitems);
                    resolve(true);
                },
                fail=>{
                    reject();
                }
            )
        });
    }
    shedulerRefresh(_smartySpot:smartySpot):Promise<boolean>{
        return new Promise((resolve,reject) => {
            let actuality = 
            this.getInfo(_smartySpot,"shedulerList",this.getShedulerActuality(_smartySpot)).then(sdata=>{
	            let sitems:spotShedulerItem[] = this.shedulerSource.getValue() || [];
                let nitems:spotShedulerItem[] = [];
                for(let item of sitems){
                    if(item.spotID != _smartySpot.ID){
                        nitems.push(item);
                    }
                }
                let rdata = JSON.parse(sdata.replace(/(?:\\f|\f+)+/g,"||").replace(/(?:\\[rn]|[\r\n]+)+/g, "|"));
	            if(rdata.items){
					let citems = rdata.items.trim().split('||');
					if(citems) for(var i in citems){
                        if(citems[i]){
                            let citem = citems[i].split('|').slice(-13);
                            nitems.push(new spotShedulerItem(
                                citem[0],
                                citem[1],
                                citem[2],
                                citem[3],
                                citem[4],
                                citem[5],
                                citem[6],
                                citem[7],
                                citem[8],
                                citem[9],
                                citem[10],
                                citem[11],
                                citem[12]
                            ));
                        }
					}
				}
                this.shedulerSource.next(nitems);
                this.setShedulerActuality(_smartySpot);
                resolve(true);
            },fail=>{
                reject();
            })
        });
    }
    shedulerLocalGet():Promise<spotShedulerItem[]>{
        return new Promise(resolve=>{
            let items:spotShedulerItem[] = [];
            let source_str:string = localStorage.getItem("spotsSheduler");
            if(source_str){
                let sitems = JSON.parse(source_str);
                for(let item of sitems){
                    items.push(
                        new spotShedulerItem(
                            item.ID,
                            item.spotID,
                            item.title,
                            item.commandKey,
                            item.commandVal,
                            item.dateKey,
                            item.dateVal,
                            item.hw,
                            item.timeH,
                            item.timeM,
                            item.lastCallS,
                            item.lastCallH,
                            item.lastCallM
                        )
                    );
                }
            }
            resolve(items);
        });
    }
    shedulerLocalSave():void{
	    let items:spotShedulerItem[] = this.shedulerSource.getValue();
        if(items){
            let result:string[] = [];
            for(let item of items){
                result.push(item.toJSON());
            }
            localStorage.setItem("spotsSheduler",JSON.stringify(result));
        }
    }
    shedulerAdd(_smartySpot:smartySpot,_shedulerItem:spotShedulerItem):Promise<boolean>{
        return new Promise((resolve,reject) => {
            this.sendCommandToSpot(_smartySpot,_shedulerItem).then(success=>{
                let items:spotShedulerItem[] = this.shedulerSource.getValue() || [];
                items.push(_shedulerItem);
                this.shedulerSource.next(items);
                //console.log("RESOLVE");
                resolve(success);
            },fail=>{
                //console.log("reject");
	            reject()
            })
        });
    }
    setShedulerActuality(_smartySpot:smartySpot,actuality:number = 0):void{
        if(localStorage.getItem("shedulerActuality")){
            let shedulerSpots = JSON.parse(localStorage.getItem("shedulerActuality")) || [];
            shedulerSpots[_smartySpot.ID] = actuality?actuality:(new Date().getTime()/1000);
            localStorage.setItem("shedulerActuality",JSON.stringify(shedulerSpots));
        }
    }
    getShedulerActuality(_smartySpot:smartySpot):number{
        let actuality:number = 0;
        if(localStorage.getItem("shedulerActuality")){
            let shedulerSpots = JSON.parse(localStorage.getItem("shedulerActuality")) || [];
            if(shedulerSpots[_smartySpot.ID]){
	            actuality = shedulerSpots[_smartySpot.ID];
            }
        }
        return actuality;
    }
    //--
    spotsLocalSave(items:smartySpot[] = []):void{
        let result:any[] = [];
        if(!items || items.length == 0) items = this.spotsSource.getValue();
        if(items){
            for(let item of items){
                result.push(item.toJSON());
            }
            localStorage.setItem("smartySpot",JSON.stringify(result));
        }
    }
    spotsLocalGet():Promise<smartySpot[]>{
        return new Promise(resolve => {
            let result:smartySpot[] = [];
            let storage = localStorage.getItem("smartySpot");
           	if(storage){
                let items = JSON.parse(storage);
                for(let item of items){
                    let smarty_SpotHWS:smartySpotHW[] = [];
                    for(let hwitem of item.hw){
                        smarty_SpotHWS.push(new smartySpotHW(hwitem.ID,hwitem.type,hwitem.state,hwitem.tools));
                    }
                    result.push(new smartySpot(item.ID,item.token,item.title,item.ip,smarty_SpotHWS,item.type));
                }
            }
	        result.push(new smartySpot("123","12ss3","Новый жлемент","192.168.1.1",[new smartySpotHW(1,"socket",255,["ONOFF","DIM"]),new smartySpotHW(2,"socket",255,["ONOFF","DIM"])],"socket"));
	        resolve(result);
        });
    }
    setSpotState(_smartySpot:smartySpot,state:number,tool:string):Promise<boolean>{
        return new Promise((resolve,reject) => {
            let items:smartySpot[] = this.spotsSource.getValue() || [];
            this.sendCommandToSpot(_smartySpot,new spotCommand(null,_smartySpot.ID,"",tool,state.toString(),"NOW","0")).then(success=>{
                if(parseInt(success)==_smartySpot.hw.length){
                    for(let item of _smartySpot.hw) item.state = state;
                    this.spotsSource.next(items);
                    resolve(true);
                }else{
                    reject(false);
                }
            },fail=>{
                reject(false);
            });
        });
    }
    setSpotHWState(_smartySpot:smartySpot,_smartySpotHW:smartySpotHW,state:number,tool:string):Promise<boolean>{
        return new Promise((resolve,reject) => {
            let items:smartySpot[] = this.spotsSource.getValue() || [];
            let item:smartySpot = items.find((el:smartySpot)=>{return el.ID == _smartySpot.ID});
            let target:smartySpotHW;
            if(item) target = item.hw.find((el:smartySpotHW)=>{return el.ID == _smartySpotHW.ID});
            
            this.sendCommandToSpot(_smartySpot,new spotCommand(null,_smartySpot.ID,"",tool,state.toString(),"NOW","0",_smartySpotHW.ID)).then(success=>{
                if(target){
                    target.state = state;
                    this.spotsSource.next(items);
                }
                resolve(true);
            },fail=>{
                //console.log("fail");
                reject(false);
            });
        });
    }
    setSpotTitle(_smartySpot:smartySpot,title:string):Promise<boolean>{
        return new Promise((resolve,reject) => {
            let items:smartySpot[] = this.spotsSource.getValue() || [];
            for(let indx in items){
                if(items[indx].ID == _smartySpot.ID){
	                items[indx].title = title;
                }
            }
            this.spotsSource.next(items);
	        this.spotsLocalSave(items);
	        resolve(true);
        });
    }
    _removeSpot(_smartySpot:smartySpot){
        let sitems:smartySpot[] = this.spotsSource.getValue() || [];
        let nitems:smartySpot[] = [];
        for(let item of sitems){
            if(item.ID != _smartySpot.ID){
                nitems.push(item);
            }
        }
        this.spotsSource.next(nitems);
	    this.spotsLocalSave(nitems);
    }
    resetSpot(_smartySpot:smartySpot):Promise<boolean>{
        return new Promise((resolve,reject) => {
            this.sendCommandToSpot(_smartySpot,new spotCommand(null,_smartySpot.ID,"","resetSpot","1","NOW","")).then(success=>{},fail=>{});
	        setTimeout(()=>{
                this._removeSpot(_smartySpot);
	            resolve(true);
            },2000);
        });
    }
    removeSpot(_smartySpot:smartySpot):Promise<boolean>{
        return new Promise((resolve,reject) => {
            this._removeSpot(_smartySpot);
            resolve(true);
        });
    }
    spotsIdentify():void{
        this.sendCommandBroadcast("identify","1");
    }
    
	proceedServerMessage(message:any):void{
        try{
            if(message.type == "identify"){
	            let spot_items:smartySpot[] = this.spotsSource.getValue() || [];
                if(spot_items.length > 0){
                    let spot_item:smartySpot = spot_items.find((el:smartySpot)=>{return el.ID == message.ID;})
                    if(spot_item){
                        let smarty_SpotHWS:smartySpotHW[] = [];
                        for(let indx in message.hw){
                            smarty_SpotHWS.push(new smartySpotHW(message.hw[indx].ID,message.hw[indx].type,message.hw[indx].state,message.hw[indx].tools));
                        }
                        let identifyM:identifyMessage = new identifyMessage(message.ID,message.stype,message.token,message.title,message.ip,smarty_SpotHWS);
                    
                        spot_item.setOnline();
                        spot_item.ip = identifyM.ip;
                        for(let hw of spot_item.hw){
                            let rhw = identifyM.hw.find((el:smartySpotHW)=>{return el.ID == hw.ID});
                            if(rhw) hw.state = rhw.state;
                        }
                        this.spotsSource.next(spot_items);
                    }
                }
            }else{
	            //console.log(message);
            }
	    }catch(exc){}
	}
    
	udpListner():void{
		this.corExe("recFromSpot",[]).then(data=>{
			let messages = JSON.parse(data);
	        let spot_items:smartySpot[] = this.spotsSource.getValue() || [];
            let has_changes:boolean = false;
			for(let message of messages){
				try{
                    if(message.type == "identify"){
                        let spot_item:smartySpot;
                        let need_register:boolean = true;
                        
                        let smarty_SpotHWS:smartySpotHW[] = [];
                        for(let indx in message.hw){
                            smarty_SpotHWS.push(new smartySpotHW(message.hw[indx].ID,message.hw[indx].type,message.hw[indx].state,message.hw[indx].tools));
                        }
                        let identifyM:identifyMessage = new identifyMessage(message.ID,message.stype,message.token,message.title,message.ip,smarty_SpotHWS);
                    
                        if(spot_items.length > 0){
                            spot_item = spot_items.find((el:smartySpot)=>{return el.ID == identifyM.ID;});
                            if(spot_item){
                                has_changes   = true;
                                need_register = false;

                                spot_item.setInLocal();
                                spot_item.ip = identifyM.ip;
                                for(let hw of spot_item.hw){
                                    let rhw = identifyM.hw.find((el:smartySpotHW)=>{return el.ID == hw.ID});
                                    if(rhw) hw.state = rhw.state;
                                }
                            }
                        }
                        if(need_register){
	                        this.registerNewSpot(identifyM);
                        }
                    }else{            
                        console.log("m1",messages);
                    }
				}catch(exc){alert("UDPL: "+exc.message)}
			}
            if(has_changes){
	            this.spotsSource.next(spot_items);
            }
            setTimeout(()=>{this.udpListner();},500);
		},fail=>{setTimeout(()=>{this.udpListner();},500);})
	}
    registerNewSpot(_identifyMessage:identifyMessage){
	    let items:smartySpot[] = this.spotsSource.getValue() || [];
        items.push(new smartySpot(_identifyMessage.ID,
                                        _identifyMessage.token,
                                        _identifyMessage.title,
                                        _identifyMessage.ip,
                                        _identifyMessage.hw,
                                        _identifyMessage.stype,
                                        true,new Date(),true));
        this.spotsSource.next(items);
	    this.spotsLocalSave(items);
    }


    getInfo(_smartySpot:smartySpot,infoType:string,actuality:number = 0):Promise<any>{
        return new Promise((resolve,reject)=>{
            let query:URLSearchParams = new URLSearchParams();
            query.set(infoType,"1");
            if(actuality) query.set("actuality",actuality.toString());
            if(_smartySpot._inLocal){
                this.sendLocal(_smartySpot,query,"/getInfo").then(data=>resolve(data),err=>reject(err));
            }else{
                this.getServer(_smartySpot,query,"/spot/getconf").then(data=>resolve(data),err=>reject(err));
            }
        })
	}
    setConfig(_smartySpot:smartySpot,params:any):Promise<boolean>{
        return new Promise((resolve,reject)=>{
            let query:URLSearchParams = new URLSearchParams();
            for(let indx in params){
                query.set(indx,params[indx]);
            }
            if(_smartySpot._inLocal){
                this.sendLocal(_smartySpot,query,"/setConfig").then(data=>{resolve(data=="1")},err=>reject(err));
            }else{
                reject();
            }
        })
	}
    sendCommandToSpot(_smartySpot:smartySpot,_spotCommand:spotCommand):Promise<any>{
        return new Promise((resolve,reject)=>{
            let query:URLSearchParams = new URLSearchParams();
            query.set("command",_spotCommand.stringify());
            if(_smartySpot._inLocal){
                this.sendLocal(_smartySpot,query,"/exeCommand").then(data=>{if(parseInt(data)) resolve(data); else reject(data) },err=>reject(err));
            }else{
                let waitComplete:boolean = _spotCommand.dateKey =="NOW";
                this.sendServer(_smartySpot,query,"/spot/write",(_smartySpot.ID+"_"+_spotCommand.ID),waitComplete).then(
                    data=>{if(parseInt(data)) resolve(data); else reject(data)},
                    err=>{reject(err)}
                );
            }
        })
	}

	sendCommandBroadcast(commandKey:string,commandVal:string):void{
        let spot_command = new spotCommand("","","",commandKey,commandVal,"","");
        this.corExe("sendUDP",[spot_command.stringify()]).then(()=>{},()=>{
            console.log("sendUDP rejected");
        });
	}
    
	sendLocal(_smartySpot:smartySpot,query:URLSearchParams,path:string="/exeCommand"):Promise<any>{
        return new Promise((resolve,reject)=>{
            this.http.get("http://"+_smartySpot.ip+path,{search: query})
            .map((res:Response) => res.text())
            .timeout(10000,()=>{reject()})
            .subscribe(
                data => {
                    resolve(data);
                },
                err	=> reject(err)
            );//can asPromise but zblsya
        })
	}
	sendServer(_smartySpot:smartySpot,query:URLSearchParams,path:string="/spot/write",key:string,waitComplete:boolean=false):Promise<any>{
        return new Promise((resolve,reject)=>{
            query.set("spot",_smartySpot.ID);
            query.set("token",_smartySpot.token);
            let checkAddr:string = "/spot/check";
            let server_scommand:serverSCommand = new serverSCommand(key,this.serverAddr,path,query,waitComplete,checkAddr,this.http);
            server_scommand.send().then(data=>{resolve(data)},err=>{reject(err)});
        });
	}
	getServer(_smartySpot:smartySpot,query:URLSearchParams,path:string="/spot/messages"):Promise<string>{
        return new Promise<string>((resolve,reject)=>{
            let headers:Headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            query.set("spot",_smartySpot.ID);
            query.set("token",_smartySpot.token);

            this.http.post(this.serverAddr+path,query.toString(),{headers:headers})
            .map((res:Response) => res.json())
            .timeout(10000,()=>{reject()})
            .subscribe(
                data	=> {if(data.result) resolve(data.content); else reject(data);},
                err	    => reject()
            );
        });
	}

    corExe(action:string,params:string[]=null):Promise<any>{
        return new Promise((resolve,reject)=>{
            params = params || [];
            if(cordova){
                cordova.exec((data:any)=>{
                    resolve(data);
                },(message:string)=>{
                    reject(message)
                },"smartyPoint",action,params);
            }else{
                console.log("no Cordova");
                reject("no Cordova")
            }
        })
    }
	checkInLocal(_smartySpot:smartySpot){
        var req = new XMLHttpRequest();
	    let item:smartySpot = (this.spotsSource.getValue() || []).find((el:smartySpot)=>{return el.ID == _smartySpot.ID});
        if(item){
            try{
                req.timeout = 3000;
                req.onreadystatechange = ()=>{
                    if(req.readyState === req.HEADERS_RECEIVED){
                        req.abort();
                        if(req.status>0){
	                        let items:smartySpot[] = this.spotsSource.getValue() || [];
	                        let item:smartySpot = items.find((el:smartySpot)=>{return el.ID == _smartySpot.ID});
                            if(item){
                                item.setInLocal();
                                this.spotsSource.next(items);
                            }
                        }
                    }
                };
                let onerror = ()=>{
                    let items:smartySpot[] = this.spotsSource.getValue() || [];
                    let item:smartySpot = items.find((el:smartySpot)=>{return el.ID == _smartySpot.ID});
                    if(item){
                        item.setNInLocal();
                        this.spotsSource.next(items);
                    }
                }
                req.onerror = onerror;
                req.ontimeout = onerror;
                req.open("GET", "http://"+item.ip, true);
                req.send(null);
            }catch(exc){}
        }
	}
    hideApp():void{
        document.dispatchEvent(new CustomEvent("hideApp", {}));
    }
}

class serverSCommand{
    private dateBegin:Date;
    private timeout:number;
    private commandAddr:string;
    private headers:Headers;
    constructor(
            private key:string,
            private server:string,
            private path:string,
            private query:URLSearchParams,
            private waitComplete:boolean,
            private checkAddr:string,
            private http: Http){
        this.timeout = 50000;
        this.commandAddr= this.server+this.path;
        this.checkAddr	= this.server+this.checkAddr;
       	this.headers = new Headers();
        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    }
    send():Promise<any>{
        return new Promise((resolve,reject)=>{
            this.http.post(this.commandAddr,this.query.toString(),{headers:this.headers})
            .map((res:Response) => res.json())
            .timeout(10000,()=>{reject()})
            .subscribe(
                data1 => {
                    if(data1.result){
                        if(this.waitComplete){
                            this.checker().then(data=>{resolve(data)},err=>{reject(err)});
                        }else{
                            resolve(data1.result);
                        }
                    }else{
                        reject();
                    }
                    
                },
                err	=> reject(err)
            );
        })
    }
    private checker():Promise<boolean>{
        return new Promise((resolve,reject)=>{
            this.dateBegin = new Date();
            let query = new URLSearchParams();
            query.set("key",this.key);
            
            let loop = ()=>{
                if(new Date().getTime() - this.dateBegin.getTime() < this.timeout){
                    this.http.post(this.checkAddr,query.toString(),{headers:this.headers})
                    .map((res:Response) => res.json())
                    .timeout(10000,()=>{
                        //console.log("timeout1")
                     })
                    .subscribe(
                        data => {
                           	if(data.result && (!this.waitComplete || data.message == "comlete")){
                                //console.log("comlete",data.content);
                                resolve(data.content);
	                        }else{
                                setTimeout(loop,3000)
                            }
                        },
                        err	=> setTimeout(loop,3000)
                    );
                }else{
                    //console.log("timeout");
                    reject("timeout");
                }
            }
            loop();
        });
    }
}