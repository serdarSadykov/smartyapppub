import {OnInit} from '@angular/core';
declare var jQuery:any;


export class swiperTabsService{
    _this:any;
    swipe_tabs:any;
    renderTabs(selector:string,tabsPerPage:number){
        if(selector){
            let that:any = this;
            this._this = jQuery(selector);
            var swipe_tabsContainer = this._this.find('.swipe-tabs');
	        var swipe_tabsContentContainer = this._this.find('.swipe-tabs-container');
            if(swipe_tabsContainer.hasClass('slick-initialized')){
                swipe_tabsContainer.slick('unslick');
                swipe_tabsContentContainer.slick('unslick');
                this._this.find("[tabindex]").remove();
            }
	        var swipe_tabs = this._this.find('.swipe-tab'),
                currentIndex = 0,
                activeTabClassName = 'active-tab';

            swipe_tabsContainer.on('init', function(event:any, slick:any) {
                swipe_tabsContentContainer.removeClass('invisible');
                swipe_tabsContainer.removeClass('invisible');
                currentIndex = slick.getCurrent();
                swipe_tabs.removeClass(activeTabClassName);
                that._this.find('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
            });
            swipe_tabsContainer.slick({
                slidesToShow: tabsPerPage,
                slidesToScroll: 1,
                arrows: false,
                infinite: false,
                swipeToSlide: true,
                touchThreshold: 10
            });

            swipe_tabsContentContainer.slick({
                asNavFor: swipe_tabsContainer,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                infinite: false,
                swipeToSlide: true,
	            draggable: false,
                touchThreshold: 10
            });
            swipe_tabs.on('click', function(event:any){
                currentIndex = jQuery(this).data('slick-index');
                swipe_tabs.removeClass(activeTabClassName);
                that._this.find('.swipe-tab[data-slick-index=' + currentIndex +']').addClass(activeTabClassName);
                swipe_tabsContainer.slick('slickGoTo', currentIndex);
                swipe_tabsContentContainer.slick('slickGoTo', currentIndex);
            });

            swipe_tabsContentContainer.on('swipe', function(event:any, slick:any, direction:any){
                currentIndex = jQuery(this).slick('slickCurrentSlide');
                swipe_tabs.removeClass(activeTabClassName);
                that._this.find('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
            });
        }
    }
}