import {OnInit} from '@angular/core';
declare var $:any;

export class datetimepeaker{
    timepeaker(selector:string):void{
		$(selector).datetimepicker({
			datepicker:false,
			step:15,
			inline:true,
			format:'H:i'
		});
    }
    datepeaker(selector:string):void{
		$(selector).datetimepicker({
			timepicker:false,
			format:'d.m.Y',
			inline:true,
			lang:'ru',
            minDate:'0'
		});
    }
}