"use strict";
var datetimepeaker = (function () {
    function datetimepeaker() {
    }
    datetimepeaker.prototype.timepeaker = function (selector) {
        $(selector).datetimepicker({
            datepicker: false,
            step: 15,
            inline: true,
            format: 'H:i'
        });
    };
    datetimepeaker.prototype.datepeaker = function (selector) {
        $(selector).datetimepicker({
            timepicker: false,
            format: 'd.m.Y',
            inline: true,
            lang: 'ru',
            minDate: '0'
        });
    };
    return datetimepeaker;
}());
exports.datetimepeaker = datetimepeaker;
//# sourceMappingURL=datetimepeaker.js.map