"use strict";
var en_1 = require('../language/en');
var ru_1 = require('../language/ru');
var languageService = (function () {
    function languageService() {
        var language = "ru";
        switch (language) {
            case "ru":
                this.capts = new ru_1.ru();
                break;
            case "en":
                this.capts = new en_1.en();
                break;
            default:
                this.capts = new en_1.en();
        }
    }
    languageService.prototype.__ = function (key) {
        return this.capts[key];
    };
    return languageService;
}());
exports.languageService = languageService;
//# sourceMappingURL=languageService.js.map