"use strict";
var notifyService = (function () {
    function notifyService() {
    }
    notifyService.prototype.error = function (text, title) {
        if (title === void 0) { title = "Внимание"; }
        var errorModal = $('#errorModal');
        errorModal.find(".val").html(text);
        errorModal.find(".modal-title").html(title);
        errorModal.modal('show');
    };
    notifyService.prototype.alert = function (text, title) {
        if (title === void 0) { title = "Внимание"; }
        var notifModal = $('#notifModal');
        notifModal.find(".val").html(text);
        notifModal.find(".modal-title").html(title);
        notifModal.modal('show');
    };
    return notifyService;
}());
exports.notifyService = notifyService;
//# sourceMappingURL=notifyService.js.map