webpackJsonp([0],{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var platform_browser_dynamic_1 = __webpack_require__(1);
	var app_1 = __webpack_require__(328);
	var http_1 = __webpack_require__(337);
	var core_1 = __webpack_require__(5);
	core_1.enableProdMode();
	platform_browser_dynamic_1.bootstrap(app_1.appComponent, [http_1.HTTP_PROVIDERS]);
	//# sourceMappingURL=main.js.map

/***/ },

/***/ 328:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(5);
	var spotsList_1 = __webpack_require__(329);
	var spotService_1 = __webpack_require__(330);
	var languageService_1 = __webpack_require__(376);
	var notifyService_1 = __webpack_require__(380);
	var swiperTabsService_1 = __webpack_require__(382);
	var datetimepeaker_1 = __webpack_require__(386);
	var appComponent = (function () {
	    function appComponent() {
	    }
	    appComponent = __decorate([
	        core_1.Component({
	            selector: 'smartyApp',
	            templateUrl: './app/views/app.html',
	            directives: [spotsList_1.spotsList],
	            providers: [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService, swiperTabsService_1.swiperTabsService, datetimepeaker_1.datetimepeaker]
	        }), 
	        __metadata('design:paramtypes', [])
	    ], appComponent);
	    return appComponent;
	}());
	exports.appComponent = appComponent;
	//# sourceMappingURL=app.js.map

/***/ },

/***/ 329:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(5);
	var spotService_1 = __webpack_require__(330);
	var languageService_1 = __webpack_require__(376);
	var spotItem_1 = __webpack_require__(379);
	var notifyService_1 = __webpack_require__(380);
	var swiperTabsService_1 = __webpack_require__(382);
	var shedulerList_1 = __webpack_require__(383);
	var shedulerAdd_1 = __webpack_require__(385);
	var spotAdd_1 = __webpack_require__(387);
	var spotsList = (function () {
	    function spotsList(_spotService, _lan, _notify, _swiperTabsService) {
	        this._spotService = _spotService;
	        this._lan = _lan;
	        this._notify = _notify;
	        this._spotsCats = [];
	        this.commonLightState = false;
	        this.commonSocketState = false;
	        this.commonLightCount = 0;
	        this.commonSocketCount = 0;
	        this.configSpot = null;
	        this.shedulerSpot = null;
	        this.mqttConfigForm = false;
	        this.mqttStatus = 0;
	        this.mqttStatusStr = "";
	        this.mqttPublish = "";
	        this.mqttSubscribe = "";
	        this.mqttServer = "";
	        this.mqttPort = "";
	        this.mqttUser = "";
	        this.mqttPass = "";
	        this._swiperTabsService = _swiperTabsService;
	        this.needRenderTabs = false;
	        this.spotAddM = false;
	        this.shedulerAddM = false;
	    }
	    spotsList.prototype.ngOnInit = function () {
	        var _this = this;
	        this._spotSubscription = this._spotService._spot$.subscribe(function (spotItems) { return _this.spotsChangedHandler(spotItems); });
	        document.addEventListener('backbutton', function () { _this.onBackButton(); }, false);
	    };
	    spotsList.prototype.ngOnDestroy = function () {
	        this._spotSubscription.unsubscribe();
	    };
	    spotsList.prototype.ngAfterViewChecked = function () {
	        if (this.needRenderTabs) {
	            console.log("_swiperTabsService rerender");
	            this.needRenderTabs = false;
	            this._swiperTabsService.renderTabs(".spotsList", this._spotsCats.length > 1 ? 1.7 : 1);
	        }
	    };
	    spotsList.prototype.checkCommonState = function () {
	        for (var _i = 0, _a = this._spotsCats; _i < _a.length; _i++) {
	            var cat = _a[_i];
	            var sum = 0;
	            for (var _b = 0, _c = cat.items; _b < _c.length; _b++) {
	                var item = _c[_b];
	                if (item.available) {
	                    for (var _d = 0, _e = item.hw; _d < _e.length; _d++) {
	                        var hw = _e[_d];
	                        sum += hw.state | 0;
	                    }
	                }
	            }
	            switch (cat.type) {
	                case "light":
	                    this.commonLightState = sum > 0;
	                    break;
	                case "socket":
	                    this.commonSocketState = sum > 0;
	                    break;
	            }
	        }
	    };
	    spotsList.prototype.renderTabs = function (data) {
	        console.log("spot type added");
	        this.commonLightCount = 0;
	        this.commonSocketCount = 0;
	        this._spotsCats = [];
	        var added_spots_cats = [];
	        for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
	            var item = data_1[_i];
	            if (added_spots_cats.indexOf(item.type) == -1) {
	                added_spots_cats.push(item.type);
	                this._spotsCats.push(new spotsCat(item.type, this._lan.__(item.type)));
	            }
	        }
	        //add items to cats
	        var _loop_1 = function(item) {
	            switch (item.type) {
	                case "light":
	                    this_1.commonLightCount++;
	                    break;
	                case "socket":
	                    this_1.commonSocketCount++;
	                    break;
	            }
	            var cat = this_1._spotsCats.find(function (el) { return el.type == item.type; });
	            if (cat)
	                cat.push(item);
	        };
	        var this_1 = this;
	        for (var _a = 0, data_2 = data; _a < data_2.length; _a++) {
	            var item = data_2[_a];
	            _loop_1(item);
	        }
	        this.checkCommonState();
	        this.needRenderTabs = true;
	        this.hideConfigList();
	    };
	    spotsList.prototype.spotsChangedHandler = function (data) {
	        if (!data || data.length == 0)
	            return;
	        //catch by type
	        //if nexist in local
	        var _loop_2 = function(sitem) {
	            if (!this_2._spotsCats.find(function (el) { return el.type == sitem.type; })) {
	                this_2.renderTabs(data);
	                return { value: void 0 };
	            }
	        };
	        var this_2 = this;
	        for (var _i = 0, data_3 = data; _i < data_3.length; _i++) {
	            var sitem = data_3[_i];
	            var state_2 = _loop_2(sitem);
	            if (typeof state_2 === "object") return state_2.value;
	        }
	        //if nexist in source
	        var _loop_3 = function(cat) {
	            if (!data.find(function (el) { return el.type == cat.type; })) {
	                this_3.renderTabs(data);
	                return { value: void 0 };
	            }
	        };
	        var this_3 = this;
	        for (var _a = 0, _b = this._spotsCats; _a < _b.length; _a++) {
	            var cat = _b[_a];
	            var state_3 = _loop_3(cat);
	            if (typeof state_3 === "object") return state_3.value;
	        }
	        //add if item not found in cats
	        var found;
	        var _loop_4 = function(sitem) {
	            found = false;
	            for (var _c = 0, _d = this_4._spotsCats; _c < _d.length; _c++) {
	                var cat = _d[_c];
	                var item = cat.items.find(function (el) { return el.ID == sitem.ID; });
	                if (item) {
	                    found = true;
	                    item.available = sitem.available;
	                    item._inLocal = sitem._inLocal;
	                    item.title = sitem.title;
	                    break;
	                }
	            }
	            if (!found) {
	                var cat = this_4._spotsCats.find(function (el) { return el.type == sitem.type; });
	                if (cat)
	                    cat.push(sitem);
	            }
	        };
	        var this_4 = this;
	        for (var _e = 0, data_4 = data; _e < data_4.length; _e++) {
	            var sitem = data_4[_e];
	            _loop_4(sitem);
	        }
	        this.checkCommonState();
	    };
	    spotsList.prototype.setCommonState = function (type, state) {
	        var _this = this;
	        var cat = this._spotsCats.find(function (el) { return el.type == type; });
	        if (cat)
	            var _loop_5 = function(item) {
	                item.setIsWorks();
	                this_5._spotService.setSpotState(item, state ? 255 : 0, "ONOFF").then(function () {
	                    item.setIsNWorks();
	                }, function () {
	                    _this._notify.error(_this._lan.__("sendToSpotError"));
	                    item.setIsNWorks();
	                });
	            };
	            var this_5 = this;
	            for (var _i = 0, _a = cat.items; _i < _a.length; _i++) {
	                var item = _a[_i];
	                _loop_5(item);
	            }
	    };
	    spotsList.prototype.spotItemEventHandler = function (vals) {
	        switch (vals.code) {
	            case "shedulerList":
	                this.shedulerList(vals.spotItem);
	                break;
	            case "configList":
	                this.configList(vals.spotItem);
	                break;
	        }
	    };
	    // -----------------------
	    spotsList.prototype.onBackButton = function () {
	        if (this.mqttConfigForm) {
	            this.hideMQTT();
	        }
	        else if (this.configSpot) {
	            this.hideConfigList();
	        }
	        else if (this.shedulerAddM) {
	            this.hideShedulerAdd();
	        }
	        else if (this.shedulerSpot) {
	            this.hideShedulerList();
	        }
	        else if (this.spotAddM) {
	            this.hideSpotAdd();
	        }
	        else {
	            this._spotService.hideApp();
	        }
	    };
	    spotsList.prototype.removeSpot = function () {
	        var _this = this;
	        if (confirm(this._lan.__("confirm"))) {
	            this._spotService.removeSpot(this.configSpot.spot).then(function () {
	                _this._notify.alert(_this._lan.__("spotRemoved"));
	                _this.hideConfigList();
	            }, function () {
	                _this._notify.error(_this._lan.__("errorDefault"));
	            });
	        }
	    };
	    spotsList.prototype.resetSpot = function () {
	        var _this = this;
	        if (!this.configSpot.spot._inLocal) {
	            this._notify.error(this._lan.__("localOnly"));
	            return;
	        }
	        if (confirm(this._lan.__("confirm"))) {
	            this._spotService.resetSpot(this.configSpot.spot).then(function () {
	                _this._notify.alert(_this._lan.__("spotReseted"));
	                _this.hideConfigList();
	            }, function () {
	                _this._notify.error(_this._lan.__("errorDefault"));
	            });
	        }
	    };
	    spotsList.prototype.renameSpot = function () {
	        var _this = this;
	        if (confirm(this._lan.__("spotRenamedConfirm"))) {
	            var new_name = prompt(this._lan.__("spotEnterName"));
	            if (new_name) {
	                this._spotService.setSpotTitle(this.configSpot.spot, new_name).then(function () {
	                    _this._notify.alert(_this._lan.__("spotRenamed"));
	                }, function () {
	                    _this._notify.error(_this._lan.__("errorDefault"));
	                });
	            }
	        }
	    };
	    spotsList.prototype.configList = function (item) {
	        if (!this.configSpot || this.configSpot.spot.ID != item.spot.ID) {
	            this.hideShedulerList();
	            this.hideConfigList();
	            this.hideMQTT();
	            this.configSpot = item;
	        }
	    };
	    spotsList.prototype.hideConfigList = function () {
	        if (this.configSpot) {
	            this.configSpot.focus = false;
	            this.configSpot = null;
	        }
	    };
	    spotsList.prototype.getMQTTStatus = function () {
	        var _this = this;
	        if (!this.configSpot.spot._inLocal) {
	            this.mqttStatus = -1;
	            this.mqttStatusStr = this._lan.__("localOnly");
	            return;
	        }
	        this._spotService.getInfo(this.configSpot.spot, "mqttstatus", 0).then(function (sdata) {
	            var data = JSON.parse(sdata);
	            _this.mqttStatus = parseInt(data.status) ? 1 : -1;
	            if (_this.mqttStatus == 1) {
	                _this.mqttStatusStr = _this._lan.__("connected");
	                _this.mqttPublish = data.publish.join(", ");
	                _this.mqttSubscribe = data.subscribe.join(", ");
	            }
	            else {
	                _this.mqttStatusStr = _this._lan.__("notConnected");
	            }
	            if (!_this.mqttServer && data.mqttServer)
	                _this.mqttServer = data.mqttServer;
	            if (!_this.mqttPort && data.mqttPort)
	                _this.mqttPort = data.mqttPort;
	            if (!_this.mqttUser && data.mqttUser)
	                _this.mqttUser = data.mqttUser;
	            if (_this.mqttConfigForm)
	                setTimeout(function () { _this.getMQTTStatus(); }, 1000);
	        }, function (err) {
	            _this.mqttStatus = -1;
	            _this.mqttStatusStr = _this._lan.__("notConnected");
	            if (_this.mqttConfigForm)
	                setTimeout(function () { _this.getMQTTStatus(); }, 1000);
	        });
	    };
	    spotsList.prototype.setMQTT = function () {
	        if (!this.configSpot.spot._inLocal) {
	            this._notify.error(this._lan.__("localOnly"));
	            return;
	        }
	        this.mqttStatus = 0;
	        this.mqttStatusStr = "";
	        this.mqttPublish = "";
	        this.mqttSubscribe = "";
	        this.mqttServer = "";
	        this.mqttPort = "";
	        this.mqttUser = "";
	        this.mqttPass = "";
	        this.mqttStatusStr = this._lan.__("refresh");
	        this.mqttConfigForm = true;
	        this.getMQTTStatus();
	    };
	    spotsList.prototype.saveMQTT = function () {
	        var _this = this;
	        if (!this.mqttServer || !this.mqttPort) {
	            this._notify.alert(this._lan.__("notFullFilled"));
	            return;
	        }
	        if (!this.configSpot.spot._inLocal) {
	            this._notify.error(this._lan.__("localOnly"));
	            return;
	        }
	        var params = {
	            "mqttServer": this.mqttServer,
	            "mqttPort": this.mqttPort,
	            "mqttUser": this.mqttUser,
	            "mqttPass": this.mqttPass
	        };
	        this._spotService.setConfig(this.configSpot.spot, params).then(function (success) {
	            if (success) {
	                _this._notify.alert(_this._lan.__("spotConfigured"));
	            }
	            else {
	                _this._notify.error(_this._lan.__("errorDefault"));
	            }
	        }, function (err) {
	            _this._notify.error(_this._lan.__("errorDefault"));
	        });
	    };
	    spotsList.prototype.hideMQTT = function () {
	        this.mqttConfigForm = false;
	    };
	    //--
	    spotsList.prototype.shedulerList = function (item) {
	        this.hideConfigList();
	        this.hideMQTT();
	        this.shedulerSpot = item;
	        if (this.shedulerSpot.spot._inLocal) {
	            this.shedulerRefresh();
	        }
	    };
	    spotsList.prototype.hideShedulerList = function () {
	        this.shedulerSpot = null;
	    };
	    spotsList.prototype.shedulerListEventHandler = function (code) {
	        switch (code) {
	            case "hide":
	                this.hideShedulerList();
	                break;
	        }
	    };
	    spotsList.prototype.shedulerAdd = function () {
	        if (this.shedulerSpot)
	            this.shedulerAddM = true;
	    };
	    spotsList.prototype.hideShedulerAdd = function () {
	        this.shedulerAddM = false;
	    };
	    spotsList.prototype.shedulerAddEventHandler = function (result) {
	        switch (result) {
	            case "hide":
	                this.hideShedulerAdd();
	                break;
	        }
	    };
	    spotsList.prototype.shedulerReset = function () {
	        var _this = this;
	        if (confirm(this._lan.__("confirm"))) {
	            this._spotService.shedulerReset(this.shedulerSpot.spot).then(function (result) { }, function (fail) {
	                _this._notify.error(_this._lan.__("sendToSpotError"));
	            });
	        }
	    };
	    spotsList.prototype.shedulerRefresh = function () {
	        var _this = this;
	        this._notify.error(this._lan.__("shedulerAsking"));
	        this._spotService.shedulerRefresh(this.shedulerSpot.spot).then(function (result) {
	            _this._notify.error(_this._lan.__("shedulerAskingComplete"));
	        }, function (fail) {
	            _this._notify.error(_this._lan.__("sendToSpotError"));
	        });
	    };
	    //--
	    spotsList.prototype.spotAdd = function () {
	        this.spotAddM = true;
	    };
	    spotsList.prototype.hideSpotAdd = function () {
	        var _this = this;
	        setTimeout(function () {
	            _this.spotAddM = false;
	        }, 100);
	    };
	    spotsList.prototype.spotAddEventHandler = function (code) {
	        switch (code) {
	            case "close":
	                this.hideSpotAdd();
	                break;
	        }
	    };
	    spotsList = __decorate([
	        core_1.Component({
	            selector: "spotsList",
	            templateUrl: "./app/views/spot/spotsList.html",
	            directives: [spotItem_1.spotItem, shedulerList_1.shedulerList, shedulerAdd_1.shedulerAdd, spotAdd_1.spotAdd]
	        }), 
	        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService, swiperTabsService_1.swiperTabsService])
	    ], spotsList);
	    return spotsList;
	}());
	exports.spotsList = spotsList;
	var spotsCat = (function () {
	    function spotsCat(type, title) {
	        this.type = type;
	        this.title = title;
	        this.items = [];
	    }
	    spotsCat.prototype.push = function (item) {
	        this.items.push(item);
	    };
	    return spotsCat;
	}());
	exports.spotsCat = spotsCat;
	//# sourceMappingURL=spotsList.js.map

/***/ },

/***/ 330:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var __param = (this && this.__param) || function (paramIndex, decorator) {
	    return function (target, key) { decorator(target, key, paramIndex); }
	};
	var core_1 = __webpack_require__(5);
	var smartySpot_1 = __webpack_require__(331);
	var smartySpotHW_1 = __webpack_require__(332);
	var spotCommand_1 = __webpack_require__(333);
	var spotShedulerItem_1 = __webpack_require__(334);
	var identifyMessage_1 = __webpack_require__(335);
	var BehaviorSubject_1 = __webpack_require__(336);
	var http_1 = __webpack_require__(337);
	__webpack_require__(358);
	__webpack_require__(360);
	__webpack_require__(367);
	__webpack_require__(375);
	core_1.Injectable();
	var spotService = (function () {
	    function spotService(http) {
	        var _this = this;
	        this.http = http;
	        this.spotsSource = new BehaviorSubject_1.BehaviorSubject(null);
	        this._spot$ = this.spotsSource.asObservable();
	        this.shedulerSource = new BehaviorSubject_1.BehaviorSubject(null);
	        this._sheduler$ = this.shedulerSource.asObservable();
	        this.serverAddr = "http://smarty.ssadykov.com";
	        this.spotsLocalGet().then(function (data) { if (data)
	            _this.spotsSource.next(data); });
	        this.shedulerLocalGet().then(function (data) { if (data)
	            _this.shedulerSource.next(data); });
	        this._sheduler$.subscribe(function (items) { _this.shedulerLocalSave(); });
	        this.corExe("receiveFromSpotDaemon", []).then(function (data) { }, function (err) { });
	        this.loopJobs();
	        this.udpListner();
	    }
	    spotService.prototype.loopJobs = function () {
	        var _this = this;
	        var items = this.spotsSource.getValue() || [];
	        var has_changes = false;
	        var curTimestamp = (new Date().getTime());
	        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
	            var item = items_1[_i];
	            if (item.available && curTimestamp - item._lastConnect.getTime() > 20000) {
	                has_changes = true;
	                item.setOffline();
	            }
	            if (!item._inLocal) {
	                this.getServer(item, new http_1.URLSearchParams()).then(function (sdata) {
	                    try {
	                        if (sdata)
	                            _this.proceedServerMessage(JSON.parse(sdata));
	                    }
	                    catch (exc) { }
	                }, function (err) {
	                    console.log("[getServer] err");
	                }).catch(function (exc) { });
	            }
	            this.checkInLocal(item);
	        }
	        if (has_changes) {
	            this.spotsSource.next(items);
	        }
	        setTimeout(function () { return _this.loopJobs(); }, 1000);
	    };
	    //--
	    spotService.prototype.shedulerRemove = function (_smartySpot, _shedulerItem) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", "shedulerRemove", _shedulerItem.ID, "NOW", "")).then(function (success) {
	                var sitems = _this.shedulerSource.getValue() || [];
	                var nitems = [];
	                for (var _i = 0, sitems_1 = sitems; _i < sitems_1.length; _i++) {
	                    var item = sitems_1[_i];
	                    if (item.ID != _shedulerItem.ID) {
	                        nitems.push(item);
	                    }
	                }
	                _this.shedulerSource.next(nitems);
	                resolve(true);
	            }, function (fail) {
	                reject();
	            });
	        });
	    };
	    spotService.prototype.shedulerReset = function (_smartySpot) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", "shedulerReset", "", "NOW", "")).then(function (success) {
	                var items = _this.shedulerSource.getValue() || [];
	                var nitems = [];
	                for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
	                    var item = items_2[_i];
	                    if (item.spotID != _smartySpot.ID) {
	                        nitems.push(item);
	                    }
	                }
	                _this.shedulerSource.next(nitems);
	                resolve(true);
	            }, function (fail) {
	                reject();
	            });
	        });
	    };
	    spotService.prototype.shedulerRefresh = function (_smartySpot) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            var actuality = _this.getInfo(_smartySpot, "shedulerList", _this.getShedulerActuality(_smartySpot)).then(function (sdata) {
	                var sitems = _this.shedulerSource.getValue() || [];
	                var nitems = [];
	                for (var _i = 0, sitems_2 = sitems; _i < sitems_2.length; _i++) {
	                    var item = sitems_2[_i];
	                    if (item.spotID != _smartySpot.ID) {
	                        nitems.push(item);
	                    }
	                }
	                var rdata = JSON.parse(sdata.replace(/(?:\\f|\f+)+/g, "||").replace(/(?:\\[rn]|[\r\n]+)+/g, "|"));
	                if (rdata.items) {
	                    var citems = rdata.items.trim().split('||');
	                    if (citems)
	                        for (var i in citems) {
	                            if (citems[i]) {
	                                var citem = citems[i].split('|').slice(-13);
	                                nitems.push(new spotShedulerItem_1.spotShedulerItem(citem[0], citem[1], citem[2], citem[3], citem[4], citem[5], citem[6], citem[7], citem[8], citem[9], citem[10], citem[11], citem[12]));
	                            }
	                        }
	                }
	                _this.shedulerSource.next(nitems);
	                _this.setShedulerActuality(_smartySpot);
	                resolve(true);
	            }, function (fail) {
	                reject();
	            });
	        });
	    };
	    spotService.prototype.shedulerLocalGet = function () {
	        return new Promise(function (resolve) {
	            var items = [];
	            var source_str = localStorage.getItem("spotsSheduler");
	            if (source_str) {
	                var sitems = JSON.parse(source_str);
	                for (var _i = 0, sitems_3 = sitems; _i < sitems_3.length; _i++) {
	                    var item = sitems_3[_i];
	                    items.push(new spotShedulerItem_1.spotShedulerItem(item.ID, item.spotID, item.title, item.commandKey, item.commandVal, item.dateKey, item.dateVal, item.hw, item.timeH, item.timeM, item.lastCallS, item.lastCallH, item.lastCallM));
	                }
	            }
	            resolve(items);
	        });
	    };
	    spotService.prototype.shedulerLocalSave = function () {
	        var items = this.shedulerSource.getValue();
	        if (items) {
	            var result = [];
	            for (var _i = 0, items_3 = items; _i < items_3.length; _i++) {
	                var item = items_3[_i];
	                result.push(item.toJSON());
	            }
	            localStorage.setItem("spotsSheduler", JSON.stringify(result));
	        }
	    };
	    spotService.prototype.shedulerAdd = function (_smartySpot, _shedulerItem) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            _this.sendCommandToSpot(_smartySpot, _shedulerItem).then(function (success) {
	                var items = _this.shedulerSource.getValue() || [];
	                items.push(_shedulerItem);
	                _this.shedulerSource.next(items);
	                //console.log("RESOLVE");
	                resolve(success);
	            }, function (fail) {
	                //console.log("reject");
	                reject();
	            });
	        });
	    };
	    spotService.prototype.setShedulerActuality = function (_smartySpot, actuality) {
	        if (actuality === void 0) { actuality = 0; }
	        if (localStorage.getItem("shedulerActuality")) {
	            var shedulerSpots = JSON.parse(localStorage.getItem("shedulerActuality")) || [];
	            shedulerSpots[_smartySpot.ID] = actuality ? actuality : (new Date().getTime() / 1000);
	            localStorage.setItem("shedulerActuality", JSON.stringify(shedulerSpots));
	        }
	    };
	    spotService.prototype.getShedulerActuality = function (_smartySpot) {
	        var actuality = 0;
	        if (localStorage.getItem("shedulerActuality")) {
	            var shedulerSpots = JSON.parse(localStorage.getItem("shedulerActuality")) || [];
	            if (shedulerSpots[_smartySpot.ID]) {
	                actuality = shedulerSpots[_smartySpot.ID];
	            }
	        }
	        return actuality;
	    };
	    //--
	    spotService.prototype.spotsLocalSave = function (items) {
	        if (items === void 0) { items = []; }
	        var result = [];
	        if (!items || items.length == 0)
	            items = this.spotsSource.getValue();
	        if (items) {
	            for (var _i = 0, items_4 = items; _i < items_4.length; _i++) {
	                var item = items_4[_i];
	                result.push(item.toJSON());
	            }
	            localStorage.setItem("smartySpot", JSON.stringify(result));
	        }
	    };
	    spotService.prototype.spotsLocalGet = function () {
	        return new Promise(function (resolve) {
	            var result = [];
	            var storage = localStorage.getItem("smartySpot");
	            if (storage) {
	                var items = JSON.parse(storage);
	                for (var _i = 0, items_5 = items; _i < items_5.length; _i++) {
	                    var item = items_5[_i];
	                    var smarty_SpotHWS = [];
	                    for (var _a = 0, _b = item.hw; _a < _b.length; _a++) {
	                        var hwitem = _b[_a];
	                        smarty_SpotHWS.push(new smartySpotHW_1.smartySpotHW(hwitem.ID, hwitem.type, hwitem.state, hwitem.tools));
	                    }
	                    result.push(new smartySpot_1.smartySpot(item.ID, item.token, item.title, item.ip, smarty_SpotHWS, item.type));
	                }
	            }
	            result.push(new smartySpot_1.smartySpot("123", "12ss3", "Новый жлемент", "192.168.1.1", [new smartySpotHW_1.smartySpotHW(1, "socket", 255, ["ONOFF", "DIM"]), new smartySpotHW_1.smartySpotHW(2, "socket", 255, ["ONOFF", "DIM"])], "socket"));
	            resolve(result);
	        });
	    };
	    spotService.prototype.setSpotState = function (_smartySpot, state, tool) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            var items = _this.spotsSource.getValue() || [];
	            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", tool, state.toString(), "NOW", "0")).then(function (success) {
	                if (parseInt(success) == _smartySpot.hw.length) {
	                    for (var _i = 0, _a = _smartySpot.hw; _i < _a.length; _i++) {
	                        var item = _a[_i];
	                        item.state = state;
	                    }
	                    _this.spotsSource.next(items);
	                    resolve(true);
	                }
	                else {
	                    reject(false);
	                }
	            }, function (fail) {
	                reject(false);
	            });
	        });
	    };
	    spotService.prototype.setSpotHWState = function (_smartySpot, _smartySpotHW, state, tool) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            var items = _this.spotsSource.getValue() || [];
	            var item = items.find(function (el) { return el.ID == _smartySpot.ID; });
	            var target;
	            if (item)
	                target = item.hw.find(function (el) { return el.ID == _smartySpotHW.ID; });
	            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", tool, state.toString(), "NOW", "0", _smartySpotHW.ID)).then(function (success) {
	                if (target) {
	                    target.state = state;
	                    _this.spotsSource.next(items);
	                }
	                resolve(true);
	            }, function (fail) {
	                //console.log("fail");
	                reject(false);
	            });
	        });
	    };
	    spotService.prototype.setSpotTitle = function (_smartySpot, title) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            var items = _this.spotsSource.getValue() || [];
	            for (var indx in items) {
	                if (items[indx].ID == _smartySpot.ID) {
	                    items[indx].title = title;
	                }
	            }
	            _this.spotsSource.next(items);
	            _this.spotsLocalSave(items);
	            resolve(true);
	        });
	    };
	    spotService.prototype._removeSpot = function (_smartySpot) {
	        var sitems = this.spotsSource.getValue() || [];
	        var nitems = [];
	        for (var _i = 0, sitems_4 = sitems; _i < sitems_4.length; _i++) {
	            var item = sitems_4[_i];
	            if (item.ID != _smartySpot.ID) {
	                nitems.push(item);
	            }
	        }
	        this.spotsSource.next(nitems);
	        this.spotsLocalSave(nitems);
	    };
	    spotService.prototype.resetSpot = function (_smartySpot) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            _this.sendCommandToSpot(_smartySpot, new spotCommand_1.spotCommand(null, _smartySpot.ID, "", "resetSpot", "1", "NOW", "")).then(function (success) { }, function (fail) { });
	            setTimeout(function () {
	                _this._removeSpot(_smartySpot);
	                resolve(true);
	            }, 2000);
	        });
	    };
	    spotService.prototype.removeSpot = function (_smartySpot) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            _this._removeSpot(_smartySpot);
	            resolve(true);
	        });
	    };
	    spotService.prototype.spotsIdentify = function () {
	        this.sendCommandBroadcast("identify", "1");
	    };
	    spotService.prototype.proceedServerMessage = function (message) {
	        try {
	            if (message.type == "identify") {
	                var spot_items = this.spotsSource.getValue() || [];
	                if (spot_items.length > 0) {
	                    var spot_item = spot_items.find(function (el) { return el.ID == message.ID; });
	                    if (spot_item) {
	                        var smarty_SpotHWS = [];
	                        for (var indx in message.hw) {
	                            smarty_SpotHWS.push(new smartySpotHW_1.smartySpotHW(message.hw[indx].ID, message.hw[indx].type, message.hw[indx].state, message.hw[indx].tools));
	                        }
	                        var identifyM = new identifyMessage_1.identifyMessage(message.ID, message.stype, message.token, message.title, message.ip, smarty_SpotHWS);
	                        spot_item.setOnline();
	                        spot_item.ip = identifyM.ip;
	                        var _loop_1 = function(hw) {
	                            var rhw = identifyM.hw.find(function (el) { return el.ID == hw.ID; });
	                            if (rhw)
	                                hw.state = rhw.state;
	                        };
	                        for (var _i = 0, _a = spot_item.hw; _i < _a.length; _i++) {
	                            var hw = _a[_i];
	                            _loop_1(hw);
	                        }
	                        this.spotsSource.next(spot_items);
	                    }
	                }
	            }
	            else {
	            }
	        }
	        catch (exc) { }
	    };
	    spotService.prototype.udpListner = function () {
	        var _this = this;
	        this.corExe("recFromSpot", []).then(function (data) {
	            var messages = JSON.parse(data);
	            var spot_items = _this.spotsSource.getValue() || [];
	            var has_changes = false;
	            var _loop_2 = function(message) {
	                try {
	                    if (message.type == "identify") {
	                        var spot_item = void 0;
	                        var need_register = true;
	                        var smarty_SpotHWS = [];
	                        for (var indx in message.hw) {
	                            smarty_SpotHWS.push(new smartySpotHW_1.smartySpotHW(message.hw[indx].ID, message.hw[indx].type, message.hw[indx].state, message.hw[indx].tools));
	                        }
	                        var identifyM_1 = new identifyMessage_1.identifyMessage(message.ID, message.stype, message.token, message.title, message.ip, smarty_SpotHWS);
	                        if (spot_items.length > 0) {
	                            spot_item = spot_items.find(function (el) { return el.ID == identifyM_1.ID; });
	                            if (spot_item) {
	                                has_changes = true;
	                                need_register = false;
	                                spot_item.setInLocal();
	                                spot_item.ip = identifyM_1.ip;
	                                var _loop_3 = function(hw) {
	                                    var rhw = identifyM_1.hw.find(function (el) { return el.ID == hw.ID; });
	                                    if (rhw)
	                                        hw.state = rhw.state;
	                                };
	                                for (var _i = 0, _a = spot_item.hw; _i < _a.length; _i++) {
	                                    var hw = _a[_i];
	                                    _loop_3(hw);
	                                }
	                            }
	                        }
	                        if (need_register) {
	                            _this.registerNewSpot(identifyM_1);
	                        }
	                    }
	                    else {
	                        console.log("m1", messages);
	                    }
	                }
	                catch (exc) {
	                    alert("UDPL: " + exc.message);
	                }
	            };
	            for (var _b = 0, messages_1 = messages; _b < messages_1.length; _b++) {
	                var message = messages_1[_b];
	                _loop_2(message);
	            }
	            if (has_changes) {
	                _this.spotsSource.next(spot_items);
	            }
	            setTimeout(function () { _this.udpListner(); }, 500);
	        }, function (fail) { setTimeout(function () { _this.udpListner(); }, 500); });
	    };
	    spotService.prototype.registerNewSpot = function (_identifyMessage) {
	        var items = this.spotsSource.getValue() || [];
	        items.push(new smartySpot_1.smartySpot(_identifyMessage.ID, _identifyMessage.token, _identifyMessage.title, _identifyMessage.ip, _identifyMessage.hw, _identifyMessage.stype, true, new Date(), true));
	        this.spotsSource.next(items);
	        this.spotsLocalSave(items);
	    };
	    spotService.prototype.getInfo = function (_smartySpot, infoType, actuality) {
	        var _this = this;
	        if (actuality === void 0) { actuality = 0; }
	        return new Promise(function (resolve, reject) {
	            var query = new http_1.URLSearchParams();
	            query.set(infoType, "1");
	            if (actuality)
	                query.set("actuality", actuality.toString());
	            if (_smartySpot._inLocal) {
	                _this.sendLocal(_smartySpot, query, "/getInfo").then(function (data) { return resolve(data); }, function (err) { return reject(err); });
	            }
	            else {
	                _this.getServer(_smartySpot, query, "/spot/getconf").then(function (data) { return resolve(data); }, function (err) { return reject(err); });
	            }
	        });
	    };
	    spotService.prototype.setConfig = function (_smartySpot, params) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            var query = new http_1.URLSearchParams();
	            for (var indx in params) {
	                query.set(indx, params[indx]);
	            }
	            if (_smartySpot._inLocal) {
	                _this.sendLocal(_smartySpot, query, "/setConfig").then(function (data) { resolve(data == "1"); }, function (err) { return reject(err); });
	            }
	            else {
	                reject();
	            }
	        });
	    };
	    spotService.prototype.sendCommandToSpot = function (_smartySpot, _spotCommand) {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            var query = new http_1.URLSearchParams();
	            query.set("command", _spotCommand.stringify());
	            if (_smartySpot._inLocal) {
	                _this.sendLocal(_smartySpot, query, "/exeCommand").then(function (data) { if (parseInt(data))
	                    resolve(data);
	                else
	                    reject(data); }, function (err) { return reject(err); });
	            }
	            else {
	                var waitComplete = _spotCommand.dateKey == "NOW";
	                _this.sendServer(_smartySpot, query, "/spot/write", (_smartySpot.ID + "_" + _spotCommand.ID), waitComplete).then(function (data) { if (parseInt(data))
	                    resolve(data);
	                else
	                    reject(data); }, function (err) { reject(err); });
	            }
	        });
	    };
	    spotService.prototype.sendCommandBroadcast = function (commandKey, commandVal) {
	        var spot_command = new spotCommand_1.spotCommand("", "", "", commandKey, commandVal, "", "");
	        this.corExe("sendUDP", [spot_command.stringify()]).then(function () { }, function () {
	            console.log("sendUDP rejected");
	        });
	    };
	    spotService.prototype.sendLocal = function (_smartySpot, query, path) {
	        var _this = this;
	        if (path === void 0) { path = "/exeCommand"; }
	        return new Promise(function (resolve, reject) {
	            _this.http.get("http://" + _smartySpot.ip + path, { search: query })
	                .map(function (res) { return res.text(); })
	                .timeout(10000, function () { reject(); })
	                .subscribe(function (data) {
	                resolve(data);
	            }, function (err) { return reject(err); }); //can asPromise but zblsya
	        });
	    };
	    spotService.prototype.sendServer = function (_smartySpot, query, path, key, waitComplete) {
	        var _this = this;
	        if (path === void 0) { path = "/spot/write"; }
	        if (waitComplete === void 0) { waitComplete = false; }
	        return new Promise(function (resolve, reject) {
	            query.set("spot", _smartySpot.ID);
	            query.set("token", _smartySpot.token);
	            var checkAddr = "/spot/check";
	            var server_scommand = new serverSCommand(key, _this.serverAddr, path, query, waitComplete, checkAddr, _this.http);
	            server_scommand.send().then(function (data) { resolve(data); }, function (err) { reject(err); });
	        });
	    };
	    spotService.prototype.getServer = function (_smartySpot, query, path) {
	        var _this = this;
	        if (path === void 0) { path = "/spot/messages"; }
	        return new Promise(function (resolve, reject) {
	            var headers = new http_1.Headers();
	            headers.append('Content-Type', 'application/x-www-form-urlencoded');
	            query.set("spot", _smartySpot.ID);
	            query.set("token", _smartySpot.token);
	            _this.http.post(_this.serverAddr + path, query.toString(), { headers: headers })
	                .map(function (res) { return res.json(); })
	                .timeout(10000, function () { reject(); })
	                .subscribe(function (data) { if (data.result)
	                resolve(data.content);
	            else
	                reject(data); }, function (err) { return reject(); });
	        });
	    };
	    spotService.prototype.corExe = function (action, params) {
	        if (params === void 0) { params = null; }
	        return new Promise(function (resolve, reject) {
	            params = params || [];
	            if (cordova) {
	                cordova.exec(function (data) {
	                    resolve(data);
	                }, function (message) {
	                    reject(message);
	                }, "smartyPoint", action, params);
	            }
	            else {
	                console.log("no Cordova");
	                reject("no Cordova");
	            }
	        });
	    };
	    spotService.prototype.checkInLocal = function (_smartySpot) {
	        var _this = this;
	        var req = new XMLHttpRequest();
	        var item = (this.spotsSource.getValue() || []).find(function (el) { return el.ID == _smartySpot.ID; });
	        if (item) {
	            try {
	                req.timeout = 3000;
	                req.onreadystatechange = function () {
	                    if (req.readyState === req.HEADERS_RECEIVED) {
	                        req.abort();
	                        if (req.status > 0) {
	                            var items = _this.spotsSource.getValue() || [];
	                            var item_1 = items.find(function (el) { return el.ID == _smartySpot.ID; });
	                            if (item_1) {
	                                item_1.setInLocal();
	                                _this.spotsSource.next(items);
	                            }
	                        }
	                    }
	                };
	                var onerror_1 = function () {
	                    var items = _this.spotsSource.getValue() || [];
	                    var item = items.find(function (el) { return el.ID == _smartySpot.ID; });
	                    if (item) {
	                        item.setNInLocal();
	                        _this.spotsSource.next(items);
	                    }
	                };
	                req.onerror = onerror_1;
	                req.ontimeout = onerror_1;
	                req.open("GET", "http://" + item.ip, true);
	                req.send(null);
	            }
	            catch (exc) { }
	        }
	    };
	    spotService.prototype.hideApp = function () {
	        document.dispatchEvent(new CustomEvent("hideApp", {}));
	    };
	    spotService = __decorate([
	        __param(0, core_1.Inject(http_1.Http)), 
	        __metadata('design:paramtypes', [http_1.Http])
	    ], spotService);
	    return spotService;
	}());
	exports.spotService = spotService;
	var serverSCommand = (function () {
	    function serverSCommand(key, server, path, query, waitComplete, checkAddr, http) {
	        this.key = key;
	        this.server = server;
	        this.path = path;
	        this.query = query;
	        this.waitComplete = waitComplete;
	        this.checkAddr = checkAddr;
	        this.http = http;
	        this.timeout = 50000;
	        this.commandAddr = this.server + this.path;
	        this.checkAddr = this.server + this.checkAddr;
	        this.headers = new http_1.Headers();
	        this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
	    }
	    serverSCommand.prototype.send = function () {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            _this.http.post(_this.commandAddr, _this.query.toString(), { headers: _this.headers })
	                .map(function (res) { return res.json(); })
	                .timeout(10000, function () { reject(); })
	                .subscribe(function (data1) {
	                if (data1.result) {
	                    if (_this.waitComplete) {
	                        _this.checker().then(function (data) { resolve(data); }, function (err) { reject(err); });
	                    }
	                    else {
	                        resolve(data1.result);
	                    }
	                }
	                else {
	                    reject();
	                }
	            }, function (err) { return reject(err); });
	        });
	    };
	    serverSCommand.prototype.checker = function () {
	        var _this = this;
	        return new Promise(function (resolve, reject) {
	            _this.dateBegin = new Date();
	            var query = new http_1.URLSearchParams();
	            query.set("key", _this.key);
	            var loop = function () {
	                if (new Date().getTime() - _this.dateBegin.getTime() < _this.timeout) {
	                    _this.http.post(_this.checkAddr, query.toString(), { headers: _this.headers })
	                        .map(function (res) { return res.json(); })
	                        .timeout(10000, function () {
	                        //console.log("timeout1")
	                    })
	                        .subscribe(function (data) {
	                        if (data.result && (!_this.waitComplete || data.message == "comlete")) {
	                            //console.log("comlete",data.content);
	                            resolve(data.content);
	                        }
	                        else {
	                            setTimeout(loop, 3000);
	                        }
	                    }, function (err) { return setTimeout(loop, 3000); });
	                }
	                else {
	                    //console.log("timeout");
	                    reject("timeout");
	                }
	            };
	            loop();
	        });
	    };
	    return serverSCommand;
	}());
	//# sourceMappingURL=spotService.js.map

/***/ },

/***/ 331:
/***/ function(module, exports) {

	"use strict";
	var smartySpot = (function () {
	    function smartySpot(ID, token, title, ip, hw, type, _inLocal, _lastConnect, available) {
	        if (_inLocal === void 0) { _inLocal = false; }
	        if (_lastConnect === void 0) { _lastConnect = null; }
	        if (available === void 0) { available = false; }
	        this.ID = ID;
	        this.token = token;
	        this.title = title;
	        this.ip = ip;
	        this.hw = hw;
	        this.type = type;
	        this._inLocal = _inLocal;
	        this._lastConnect = _lastConnect;
	        this.available = available;
	        if (!this._lastConnect) {
	            this._lastConnect = new Date();
	        }
	        this.WORKS = false;
	    }
	    smartySpot.prototype.setIsWorks = function () {
	        this.WORKS = true;
	    };
	    smartySpot.prototype.setIsNWorks = function () {
	        this.WORKS = false;
	    };
	    smartySpot.prototype.setInLocal = function () {
	        this._inLocal = true;
	        this.setOnline();
	    };
	    smartySpot.prototype.setNInLocal = function () {
	        this._inLocal = false;
	    };
	    smartySpot.prototype.setOnline = function () {
	        this._lastConnect = new Date();
	        this.available = true;
	    };
	    smartySpot.prototype.setOffline = function () {
	        this._inLocal = false;
	        this.available = false;
	    };
	    smartySpot.prototype.stringify = function () {
	        return JSON.stringify({
	            "ID": this.ID,
	            "token": this.token,
	            "title": this.title,
	            "ip": this.ip,
	            "hw": this.hw,
	            "type": this.type,
	        });
	    };
	    smartySpot.prototype.toJSON = function () {
	        return {
	            "ID": this.ID,
	            "token": this.token,
	            "title": this.title,
	            "ip": this.ip,
	            "hw": this.hw,
	            "type": this.type,
	        };
	    };
	    return smartySpot;
	}());
	exports.smartySpot = smartySpot;
	//# sourceMappingURL=smartySpot.js.map

/***/ },

/***/ 332:
/***/ function(module, exports) {

	"use strict";
	var smartySpotHW = (function () {
	    function smartySpotHW(ID, type, state, tools) {
	        this.ID = ID;
	        this.type = type;
	        this.state = state;
	        this.tools = tools;
	    }
	    smartySpotHW.prototype.stringify = function () {
	        return JSON.stringify({
	            "ID": this.ID,
	            "type": this.type,
	            "state": this.state,
	            "tools": this.tools,
	        });
	    };
	    smartySpotHW.prototype.toJSON = function () {
	        return {
	            "ID": this.ID,
	            "type": this.type,
	            "state": this.state,
	            "tools": this.tools,
	        };
	    };
	    return smartySpotHW;
	}());
	exports.smartySpotHW = smartySpotHW;
	//# sourceMappingURL=smartySpotHW.js.map

/***/ },

/***/ 333:
/***/ function(module, exports) {

	"use strict";
	var spotCommand = (function () {
	    function spotCommand(ID, spotID, title, commandKey, commandVal, dateKey, dateVal, hw, timeH, timeM, lastCallS, lastCallH, lastCallM) {
	        if (ID === void 0) { ID = null; }
	        if (hw === void 0) { hw = 0; }
	        if (timeH === void 0) { timeH = 0; }
	        if (timeM === void 0) { timeM = 0; }
	        if (lastCallS === void 0) { lastCallS = ""; }
	        if (lastCallH === void 0) { lastCallH = 0; }
	        if (lastCallM === void 0) { lastCallM = 0; }
	        this.ID = ID;
	        this.spotID = spotID;
	        this.title = title;
	        this.commandKey = commandKey;
	        this.commandVal = commandVal;
	        this.dateKey = dateKey;
	        this.dateVal = dateVal;
	        this.hw = hw;
	        this.timeH = timeH;
	        this.timeM = timeM;
	        this.lastCallS = lastCallS;
	        this.lastCallH = lastCallH;
	        this.lastCallM = lastCallM;
	        if (!this.ID)
	            this.ID = this.uniqid();
	    }
	    spotCommand.prototype.uniqid = function (prefix, moreEntropy) {
	        if (prefix === void 0) { prefix = ""; }
	        if (moreEntropy === void 0) { moreEntropy = false; }
	        var retId;
	        var _formatSeed = function (seed, reqWidth) {
	            seed = parseInt(seed, 10).toString(16); // to hex str
	            if (reqWidth < seed.length) {
	                return seed.slice(seed.length - reqWidth);
	            }
	            if (reqWidth > seed.length) {
	                return Array(1 + (reqWidth - seed.length)).join('0') + seed;
	            }
	            return seed;
	        };
	        var $locutus = {};
	        $locutus.php = {};
	        if (!$locutus.php.uniqidSeed) {
	            $locutus.php.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
	        }
	        $locutus.php.uniqidSeed++;
	        retId = prefix;
	        retId += _formatSeed((new Date().getTime() / 1000, 10).toString(), 8);
	        retId += _formatSeed($locutus.php.uniqidSeed, 5);
	        if (moreEntropy) {
	            retId += (Math.random() * 10).toFixed(8).toString();
	        }
	        return retId;
	    };
	    ;
	    spotCommand.prototype.stringify = function () {
	        return JSON.stringify({
	            "ID": this.ID,
	            "spotID": this.spotID,
	            "title": this.title,
	            "commandKey": this.commandKey,
	            "commandVal": this.commandVal,
	            "dateKey": this.dateKey,
	            "dateVal": this.dateVal,
	            "hw": this.hw,
	            "timeH": this.timeH,
	            "timeM": this.timeM,
	            "lastCallS": this.lastCallS,
	            "lastCallH": this.lastCallH,
	            "lastCallM": this.lastCallM
	        });
	    };
	    spotCommand.prototype.toJSON = function () {
	        return {
	            "ID": this.ID,
	            "spotID": this.spotID,
	            "title": this.title,
	            "commandKey": this.commandKey,
	            "commandVal": this.commandVal,
	            "dateKey": this.dateKey,
	            "dateVal": this.dateVal,
	            "hw": this.hw,
	            "timeH": this.timeH,
	            "timeM": this.timeM,
	            "lastCallS": this.lastCallS,
	            "lastCallH": this.lastCallH,
	            "lastCallM": this.lastCallM
	        };
	    };
	    return spotCommand;
	}());
	exports.spotCommand = spotCommand;
	//# sourceMappingURL=spotCommand.js.map

/***/ },

/***/ 334:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var spotCommand_1 = __webpack_require__(333);
	var spotShedulerItem = (function (_super) {
	    __extends(spotShedulerItem, _super);
	    function spotShedulerItem() {
	        _super.apply(this, arguments);
	    }
	    return spotShedulerItem;
	}(spotCommand_1.spotCommand));
	exports.spotShedulerItem = spotShedulerItem;
	//# sourceMappingURL=spotShedulerItem.js.map

/***/ },

/***/ 335:
/***/ function(module, exports) {

	"use strict";
	var identifyMessage = (function () {
	    function identifyMessage(ID, stype, token, title, ip, hw) {
	        this.ID = ID;
	        this.stype = stype;
	        this.token = token;
	        this.title = title;
	        this.ip = ip;
	        this.hw = hw;
	    }
	    return identifyMessage;
	}());
	exports.identifyMessage = identifyMessage;
	//"type"	:"identify",
	//"ID"	    :"%s",
	//"stype"	:"%s",
	//"token"	:"%s",
	//"title"	:"%s",
	//"ip"	:"%s",
	//"hw":[{"ID":1,"type":"light","status":%d,"tools":[%s]},{"ID":1,"type":"light","status":%d,"tools":[%s]}] 
	//# sourceMappingURL=identifyMessage.js.map

/***/ },

/***/ 376:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var en_1 = __webpack_require__(377);
	var ru_1 = __webpack_require__(378);
	var languageService = (function () {
	    function languageService() {
	        var language = "ru";
	        switch (language) {
	            case "ru":
	                this.capts = new ru_1.ru();
	                break;
	            case "en":
	                this.capts = new en_1.en();
	                break;
	            default:
	                this.capts = new en_1.en();
	        }
	    }
	    languageService.prototype.__ = function (key) {
	        return this.capts[key];
	    };
	    return languageService;
	}());
	exports.languageService = languageService;
	//# sourceMappingURL=languageService.js.map

/***/ },

/***/ 377:
/***/ function(module, exports) {

	"use strict";
	var en = (function () {
	    function en() {
	        this.init = "Инициализация";
	        this.spotSearch = "Поиск устройств";
	        this.spotConnecting = "Подключение устройства";
	        this.spotConnected = "Устройтво подключено";
	        this.spotConfigured = "Устройтво настроено";
	        this.spotNewAdded = "Добавлено новое устройство";
	        this.spotNotFound = "Устройтво не найдено";
	        this.spotRemoved = "Устройтво удалено";
	        this.spotReseted = "Устройтво сброшено";
	        this.spotRenamedConfirm = "Устройство будет переименовано только на вашем устройстве";
	        this.spotRenamed = "Устройтво переименовано";
	        this.spotEnterName = "Придумайте название устройства";
	        this.spotCantConnect = "Не удалось подключиться к устройству";
	        this.needWIFI = "Подключитесь к Вашей домашней/офисной WiFi сети и нажите OK";
	        this.spotConnectWiFi = "Выберите Вашу WiFi сеть<br/><small>с доступом в интернет</small>";
	        this.spotConnectWiFiNotFound = "Сети WiFi устройством не найдены";
	        this.spotConnectWiFiConnecting = "Подключение";
	        this.spotConnectWiFiPass = "Введите пароль от WiFi сети";
	        this.spotConnectWiFiPassWrong = "Пароль от WiFi сети неверный";
	        this.spotConnectWiFiCantConnect = "Не удается подключиться к WiFi сети";
	        this.spotConnectWiFiNotSecured = "Это открытая сеть. Для работы необходима сеть с доступом по паролю (WPA2-PSK)";
	        this.errorDefault = "Произошла ошибка, попробуйте еще раз";
	        this.sendToSpotError = "Не удалось выполнить команду";
	        this.turnOn = "Включить";
	        this.turnOff = "Выключить";
	        this.dimmer = "Яркость";
	        this.EDAY = "Ежедневно";
	        this.onDOM = "Каждое #DOM# число";
	        this.ONTIME = "В #TIME# часов";
	        this.taskAdded = "Задание добавлено";
	        this.taskRemoved = "Задание удалено";
	        this.notFullFilled = "Не заполнены все необходимые поля";
	        this.shedulerAsking = "Обновление списка задач";
	        this.shedulerAskingComplete = "Обновление списка задач окончено";
	        this.shedulerList = "Задачи";
	        this.shedulerAdd = "Новая задача";
	        this.localOnly = "Это действие возможно выполнить только находясь в одной сети с устройством";
	        this.hwChoose = "Выберите устройство";
	        this.light = "Выключатель";
	        this.socket = "Розетка";
	        this.resetSpot = "Это действие приведет к сбросу устройства. Продлжить?";
	        this.notConfiguredNF = "Ненастроенные устройства не найдены. Производится поиск в локальной сети.";
	        this.refresh = "Обновление";
	        this.confirm = "Вы действительно хотите выполнить действие?";
	        this.mqttSettings = "Настройка MQTT";
	        this.shedulerAddSelectPlan = "Выберите план";
	        this.selectAction = "Выберите дейстивие";
	        this.enterDescription = "Введите описание";
	        this.save = "Сохранить";
	        this.cancel = "Отменить";
	        this.spotAddT1 = "Приложение автоматически продолжит работу после подключения";
	        this.refreshIt = "Обновить";
	        this.tryAgain = "Попробовать снова";
	        this.needWIFICapt = "Для работы устройства требуется наличие WiFi сети <strong>с доступом в интернет и шифрованием WPA2-PSK</strong>";
	        this.noLocalWIFI = "Ваше устройство не подключено к домашней/офисной сети WiFi";
	        this.connected = "Подключено";
	        this.notConnected = "Не подключено";
	        this.noTasks = "Задач нет";
	        this.HOURS = "Часов";
	        this.MINUTES = "Минут";
	        this.selectTIME = "Выберите время";
	        this.selectDATE = "Выберите дату";
	        this.selectDOM = "Выберите число месяца";
	        this.shedulerDOWT1 = "Учтите: Если в месяце нет указанного числа, то задача <strong>в этот месяц выполнена не будет</strong>";
	        this.selectDOW = "Выберите день недели";
	    }
	    en.prototype.hw = function (type, key) {
	        var items = {
	            light: [
	                { "key": 1, "val": "Левая кнопка" },
	                { "key": 2, "val": "Правая кнопка" }
	            ]
	        };
	        if (type) {
	            if (key) {
	                if (items[type]) {
	                    return items[type].find(function (el) { return el.key == key; });
	                }
	                return [];
	            }
	            else {
	                if (items[type]) {
	                    return items[type];
	                }
	                return [];
	            }
	        }
	        return items;
	    };
	    ;
	    en.prototype.spotToolsCaptions = function (key) {
	        var items = {
	            "ON": "Включить",
	            "OFF": "Отключить",
	            "DIM": "Яркость"
	        };
	        return items[key] ? items[key] : [];
	    };
	    ;
	    en.prototype.shedulerTaskPType = function () {
	        return [
	            { "key": "EDAY", "val": "Ежедневно" },
	            { "key": "DOW", "val": "День недели" },
	            { "key": "DOM", "val": "День месяца" },
	            { "key": "DATE", "val": "Определенный день" }
	        ];
	    };
	    ;
	    en.prototype.onDOW = function (dow) {
	        var items = [
	            "По понедельникам",
	            "По вторникам",
	            "По средам",
	            "По четвергам",
	            "По пятницам",
	            "По субботам",
	            "По воскресеньям"
	        ];
	        if (dow && items[dow]) {
	            return items[dow];
	        }
	        return "";
	    };
	    ;
	    en.prototype.DOWS = function () {
	        return [
	            { "key": "1", "val": "Понедельник" },
	            { "key": "2", "val": "Вторник" },
	            { "key": "3", "val": "Среда" },
	            { "key": "4", "val": "Четверг" },
	            { "key": "5", "val": "Пятница" },
	            { "key": "6", "val": "Суббота" },
	            { "key": "7", "val": "Воскресенье" },
	        ];
	    };
	    ;
	    return en;
	}());
	exports.en = en;
	;
	//# sourceMappingURL=en.js.map

/***/ },

/***/ 378:
/***/ function(module, exports) {

	"use strict";
	var ru = (function () {
	    function ru() {
	        this.init = "Инициализация";
	        this.spotSearch = "Поиск устройств";
	        this.spotConnecting = "Подключение устройства";
	        this.spotConnected = "Устройтво подключено, через пару минут оно появится в списке";
	        this.spotConfigured = "Устройтво настроено";
	        this.spotNewAdded = "Добавлено новое устройство";
	        this.spotNotFound = "Устройтво не найдено";
	        this.spotRemoved = "Устройтво удалено";
	        this.spotReseted = "Устройтво сброшено";
	        this.spotRenamedConfirm = "Устройство будет переименовано только на вашем устройстве";
	        this.spotRenamed = "Устройтво переименовано";
	        this.spotEnterName = "Придумайте название устройства";
	        this.spotCantConnect = "Не удалось подключиться к устройству";
	        this.needWIFI = "Подключитесь к Вашей домашней/офисной WiFi сети и нажите OK";
	        this.spotConnectWiFi = "Выберите Вашу WiFi сеть<br/><small>с доступом в интернет</small>";
	        this.spotConnectWiFiNotFound = "Сети WiFi устройством не найдены";
	        this.spotConnectWiFiConnecting = "Подключение";
	        this.spotConnectWiFiPass = "Введите пароль от WiFi сети";
	        this.spotConnectWiFiPassWrong = "Пароль от WiFi сети неверный";
	        this.spotConnectWiFiCantConnect = "Не удается подключиться к WiFi сети";
	        this.spotConnectWiFiNotSecured = "Это открытая сеть. Для работы необходима сеть с доступом по паролю (WPA2-PSK)";
	        this.errorDefault = "Произошла ошибка, попробуйте еще раз";
	        this.sendToSpotError = "Не удалось выполнить команду";
	        this.turnOn = "Включить";
	        this.turnOff = "Выключить";
	        this.dimmer = "Яркость";
	        this.EDAY = "Ежедневно";
	        this.onDOM = "Каждое #DOM# число";
	        this.ONTIME = "В #TIME#";
	        this.taskAdded = "Задание добавлено";
	        this.taskRemoved = "Задание удалено";
	        this.notFullFilled = "Не заполнены все необходимые поля";
	        this.shedulerAsking = "Обновление списка задач";
	        this.shedulerAskingComplete = "Обновление списка задач окончено";
	        this.shedulerList = "Задачи";
	        this.shedulerAdd = "Новая задача";
	        this.localOnly = "Это действие возможно выполнить только находясь в одной сети с устройством";
	        this.hwChoose = "Выберите устройство";
	        this.light = "Выключатель";
	        this.socket = "Розетка";
	        this.resetSpot = "Это действие приведет к сбросу устройства. Продлжить?";
	        this.notConfiguredNF = "Ненастроенные устройства не найдены. Производится поиск в локальной сети.";
	        this.refresh = "Обновление";
	        this.confirm = "Вы действительно хотите выполнить действие?";
	        this.mqttSettings = "Настройка MQTT";
	        this.shedulerAddSelectPlan = "Выберите план";
	        this.selectAction = "Выберите дейстивие";
	        this.HOURS = "Часов";
	        this.MINUTES = "Минут";
	        this.selectTIME = "Выберите время";
	        this.selectDATE = "Выберите дату";
	        this.selectDOM = "Выберите число месяца";
	        this.shedulerDOWT1 = "Учтите: Если в месяце нет указанного числа, то задача <strong>в этот месяц выполнена не будет</strong>";
	        this.selectDOW = "Выберите день недели";
	        this.enterDescription = "Введите описание";
	        this.save = "Сохранить";
	        this.cancel = "Отменить";
	        this.spotAddT1 = "Приложение автоматически продолжит работу после подключения";
	        this.refreshIt = "Обновить";
	        this.tryAgain = "Попробовать снова";
	        this.needWIFICapt = "Для работы устройства требуется наличие WiFi сети <strong>с доступом в интернет и шифрованием WPA2-PSK</strong>";
	        this.noLocalWIFI = "Ваше устройство не подключено к домашней/офисной сети WiFi";
	        this.connected = "Подключено";
	        this.notConnected = "Не подключено";
	        this.noTasks = "Задач нет";
	    }
	    ru.prototype.hw = function (type, key) {
	        console.log(type, key);
	        var items = {
	            light: [
	                { "key": 1, "val": "Левая кнопка" },
	                { "key": 2, "val": "Правая кнопка" }
	            ]
	        };
	        if (type) {
	            if (key) {
	                if (items[type]) {
	                    return items[type].find(function (el) { return el.key == key; });
	                }
	                return [];
	            }
	            else {
	                if (items[type]) {
	                    return items[type];
	                }
	                return [];
	            }
	        }
	        return items;
	    };
	    ;
	    ru.prototype.spotToolsCaptions = function (key) {
	        var items = {
	            "ON": "Включить",
	            "OFF": "Отключить",
	            "DIM": "Яркость"
	        };
	        return items[key] ? items[key] : [];
	    };
	    ;
	    ru.prototype.shedulerTaskPType = function () {
	        return [
	            { "key": "EDAY", "val": "Ежедневно" },
	            { "key": "DOW", "val": "День недели" },
	            { "key": "DOM", "val": "День месяца" },
	            { "key": "DATE", "val": "Определенный день" }
	        ];
	    };
	    ;
	    ru.prototype.onDOW = function (dow) {
	        var items = [
	            "По понедельникам",
	            "По вторникам",
	            "По средам",
	            "По четвергам",
	            "По пятницам",
	            "По субботам",
	            "По воскресеньям"
	        ];
	        if (dow && items[dow]) {
	            return items[dow];
	        }
	        return "";
	    };
	    ;
	    ru.prototype.DOWS = function () {
	        return [
	            { "key": "1", "val": "Понедельник" },
	            { "key": "2", "val": "Вторник" },
	            { "key": "3", "val": "Среда" },
	            { "key": "4", "val": "Четверг" },
	            { "key": "5", "val": "Пятница" },
	            { "key": "6", "val": "Суббота" },
	            { "key": "7", "val": "Воскресенье" },
	        ];
	    };
	    ;
	    return ru;
	}());
	exports.ru = ru;
	;
	//# sourceMappingURL=ru.js.map

/***/ },

/***/ 379:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(5);
	var spotService_1 = __webpack_require__(330);
	var languageService_1 = __webpack_require__(376);
	var notifyService_1 = __webpack_require__(380);
	var smartySpot_1 = __webpack_require__(331);
	var spotHW_1 = __webpack_require__(381);
	var spotItem = (function () {
	    function spotItem(_spotService, _lan, _notify) {
	        this._spotService = _spotService;
	        this._lan = _lan;
	        this._notify = _notify;
	        this.spotItemEvent = new core_1.EventEmitter();
	        this.commonState = 0;
	        this.commonTools = {};
	    }
	    spotItem.prototype.ngOnInit = function () {
	        var _this = this;
	        for (var _i = 0, _a = this.spot.hw; _i < _a.length; _i++) {
	            var hw = _a[_i];
	            for (var _b = 0, _c = hw.tools; _b < _c.length; _b++) {
	                var tool = _c[_b];
	                this.commonTools[tool] = true;
	            }
	        }
	        this._spotSubscription = this._spotService._spot$.subscribe(function (item) { return _this.spotsChangedHandler(item); });
	    };
	    spotItem.prototype.ngOnDestroy = function () {
	        this._spotSubscription.unsubscribe();
	    };
	    spotItem.prototype.spotsChangedHandler = function (data) {
	        //console.log(this.spot.available?"available":"NAvailable");
	        var sum = [];
	        for (var _i = 0, _a = this.spot.hw; _i < _a.length; _i++) {
	            var hw = _a[_i];
	            if (hw.state) {
	                sum.push(hw.state | 0);
	            }
	        }
	        this.commonState = sum.length ? (sum.reduce(function (a, b) { return a + b; }, 0) / sum.length) : 0;
	    };
	    spotItem.prototype.hwStateChangedHandler = function (hw) {
	        //this.spot.hw.find((el:smartySpotHW,indx:number,arr:smartySpotHW[])=>{
	        //	return el.ID === hw.ID;
	        //})
	        //this.spotsChangedHandler();
	    };
	    spotItem.prototype.setState = function (event) {
	        var _this = this;
	        var state;
	        var tool = "DIM";
	        this.spot.setIsWorks();
	        if (event.target.getAttribute('type') === "checkbox") {
	            state = event.target.checked ? 255 : 0;
	            tool = "ONOFF";
	        }
	        else if (event.target.getAttribute('type') === "range") {
	            state = event.target.value;
	            tool = "DIM";
	        }
	        this._spotService.setSpotState(this.spot, state, tool).then(function () {
	            _this.commonState = state;
	            _this.spot.setIsNWorks();
	        }, function () {
	            _this._notify.error(_this._lan.__('sendToSpotError'));
	            if (event.target.getAttribute('type') === "checkbox")
	                event.target.checked = _this.commonState > 0;
	            else if (event.target.getAttribute('type') === "range")
	                event.target.value = _this.commonState;
	            _this.spot.setIsNWorks();
	        });
	    };
	    spotItem.prototype.shedulerList = function (event) {
	        this.spotItemEvent.emit({ "code": "shedulerList", "spotItem": this });
	    };
	    spotItem.prototype.configList = function (event) {
	        if (['BUTTON', 'INPUT', 'I'].indexOf(event.target.tagName) == -1
	            && ['BUTTON', 'INPUT'].indexOf(event.target.parentNode.tagName) == -1
	            && (!event.target.firstChild || ['BUTTON', 'INPUT'].indexOf(event.target.firstChild.tagName) == -1)) {
	            this.focus = true;
	            this.spotItemEvent.emit({ "code": "configList", "spotItem": this });
	        }
	    };
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', smartySpot_1.smartySpot)
	    ], spotItem.prototype, "spot", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', Boolean)
	    ], spotItem.prototype, "focus", void 0);
	    __decorate([
	        core_1.Output(), 
	        __metadata('design:type', Object)
	    ], spotItem.prototype, "spotItemEvent", void 0);
	    spotItem = __decorate([
	        core_1.Component({
	            selector: 'spotItem',
	            templateUrl: './app/views/spot/spotItem.html',
	            directives: [spotHW_1.spotHW],
	        }), 
	        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService])
	    ], spotItem);
	    return spotItem;
	}());
	exports.spotItem = spotItem;
	//# sourceMappingURL=spotItem.js.map

/***/ },

/***/ 380:
/***/ function(module, exports) {

	"use strict";
	var notifyService = (function () {
	    function notifyService() {
	    }
	    notifyService.prototype.error = function (text, title) {
	        if (title === void 0) { title = "Внимание"; }
	        var errorModal = $('#errorModal');
	        errorModal.find(".val").html(text);
	        errorModal.find(".modal-title").html(title);
	        errorModal.modal('show');
	    };
	    notifyService.prototype.alert = function (text, title) {
	        if (title === void 0) { title = "Внимание"; }
	        var notifModal = $('#notifModal');
	        notifModal.find(".val").html(text);
	        notifModal.find(".modal-title").html(title);
	        notifModal.modal('show');
	    };
	    return notifyService;
	}());
	exports.notifyService = notifyService;
	//# sourceMappingURL=notifyService.js.map

/***/ },

/***/ 381:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(5);
	var spotService_1 = __webpack_require__(330);
	var languageService_1 = __webpack_require__(376);
	var notifyService_1 = __webpack_require__(380);
	var smartySpot_1 = __webpack_require__(331);
	var smartySpotHW_1 = __webpack_require__(332);
	var spotHW = (function () {
	    function spotHW(_spotService, _lan, _notify) {
	        this._spotService = _spotService;
	        this._lan = _lan;
	        this._notify = _notify;
	        this.stateChangedEvent = new core_1.EventEmitter();
	        this.tools = {};
	    }
	    spotHW.prototype.ngOnInit = function () {
	        for (var _i = 0, _a = this.hw.tools; _i < _a.length; _i++) {
	            var tool = _a[_i];
	            this.tools[tool] = true;
	        }
	    };
	    spotHW.prototype.onSave = function () { };
	    spotHW.prototype.setState = function (event) {
	        var _this = this;
	        var state;
	        var tool = "DIM";
	        this.spot.setIsWorks();
	        if (event.target.getAttribute('type') === "checkbox") {
	            state = event.target.checked ? 255 : 0;
	            tool = "ONOFF";
	        }
	        else if (event.target.getAttribute('type') === "range") {
	            state = event.target.value;
	            tool = "DIM";
	        }
	        this._spotService.setSpotHWState(this.spot, this.hw, state, tool).then(function () {
	            _this.hw.state = state;
	            _this.spot.setIsNWorks();
	            _this.stateChangedEvent.emit(_this.hw);
	        }, function () {
	            _this._notify.error(_this._lan.__('sendToSpotError'));
	            if (event.target.getAttribute('type') === "checkbox")
	                event.target.checked = _this.hw.state > 0;
	            else if (event.target.getAttribute('type') === "range")
	                event.target.value = _this.hw.state;
	            _this.spot.setIsNWorks();
	        });
	    };
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', smartySpot_1.smartySpot)
	    ], spotHW.prototype, "spot", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', smartySpotHW_1.smartySpotHW)
	    ], spotHW.prototype, "hw", void 0);
	    __decorate([
	        core_1.Output(), 
	        __metadata('design:type', Object)
	    ], spotHW.prototype, "stateChangedEvent", void 0);
	    spotHW = __decorate([
	        core_1.Component({
	            selector: 'spotHW',
	            templateUrl: './app/views/spot/spotHW.html',
	        }), 
	        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService])
	    ], spotHW);
	    return spotHW;
	}());
	exports.spotHW = spotHW;
	//# sourceMappingURL=spotHW.js.map

/***/ },

/***/ 382:
/***/ function(module, exports) {

	"use strict";
	var swiperTabsService = (function () {
	    function swiperTabsService() {
	    }
	    swiperTabsService.prototype.renderTabs = function (selector, tabsPerPage) {
	        if (selector) {
	            var that_1 = this;
	            this._this = jQuery(selector);
	            var swipe_tabsContainer = this._this.find('.swipe-tabs');
	            var swipe_tabsContentContainer = this._this.find('.swipe-tabs-container');
	            if (swipe_tabsContainer.hasClass('slick-initialized')) {
	                swipe_tabsContainer.slick('unslick');
	                swipe_tabsContentContainer.slick('unslick');
	                this._this.find("[tabindex]").remove();
	            }
	            var swipe_tabs = this._this.find('.swipe-tab'), currentIndex = 0, activeTabClassName = 'active-tab';
	            swipe_tabsContainer.on('init', function (event, slick) {
	                swipe_tabsContentContainer.removeClass('invisible');
	                swipe_tabsContainer.removeClass('invisible');
	                currentIndex = slick.getCurrent();
	                swipe_tabs.removeClass(activeTabClassName);
	                that_1._this.find('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
	            });
	            swipe_tabsContainer.slick({
	                slidesToShow: tabsPerPage,
	                slidesToScroll: 1,
	                arrows: false,
	                infinite: false,
	                swipeToSlide: true,
	                touchThreshold: 10
	            });
	            swipe_tabsContentContainer.slick({
	                asNavFor: swipe_tabsContainer,
	                slidesToShow: 1,
	                slidesToScroll: 1,
	                arrows: false,
	                infinite: false,
	                swipeToSlide: true,
	                draggable: false,
	                touchThreshold: 10
	            });
	            swipe_tabs.on('click', function (event) {
	                currentIndex = jQuery(this).data('slick-index');
	                swipe_tabs.removeClass(activeTabClassName);
	                that_1._this.find('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
	                swipe_tabsContainer.slick('slickGoTo', currentIndex);
	                swipe_tabsContentContainer.slick('slickGoTo', currentIndex);
	            });
	            swipe_tabsContentContainer.on('swipe', function (event, slick, direction) {
	                currentIndex = jQuery(this).slick('slickCurrentSlide');
	                swipe_tabs.removeClass(activeTabClassName);
	                that_1._this.find('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
	            });
	        }
	    };
	    return swiperTabsService;
	}());
	exports.swiperTabsService = swiperTabsService;
	//# sourceMappingURL=swiperTabsService.js.map

/***/ },

/***/ 383:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(5);
	var spotService_1 = __webpack_require__(330);
	var languageService_1 = __webpack_require__(376);
	var notifyService_1 = __webpack_require__(380);
	var smartySpot_1 = __webpack_require__(331);
	var shedulerItem_1 = __webpack_require__(384);
	var shedulerList = (function () {
	    function shedulerList(_spotService, _lan, _notify) {
	        this._spotService = _spotService;
	        this._lan = _lan;
	        this._notify = _notify;
	        this.shedulerListEvent = new core_1.EventEmitter();
	        this.onLoading = false;
	        this.shedulerItems = [];
	    }
	    shedulerList.prototype.ngOnInit = function () {
	        var _this = this;
	        this._shedulerSubscription = this._spotService._sheduler$.subscribe(function (items) {
	            _this.shedulerItems = [];
	            for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
	                var item = items_1[_i];
	                if (item.spotID == _this.spot.ID) {
	                    _this.shedulerItems.push(item);
	                }
	            }
	        });
	    };
	    shedulerList.prototype.ngOnDestroy = function () {
	        this._shedulerSubscription.unsubscribe();
	    };
	    shedulerList.prototype.shedulerDeleteEventHandler = function (shedulerItem) {
	        var _this = this;
	        this.onLoading = true;
	        this._spotService.shedulerRemove(this.spot, shedulerItem).then(function (result) {
	            _this._notify.alert(_this._lan.__('taskRemoved'));
	            _this.onLoading = false;
	        }, function (err) {
	            _this._notify.alert(_this._lan.__('sendToSpotError'));
	            _this.onLoading = false;
	        });
	    };
	    shedulerList.prototype.hide = function () {
	        this.shedulerListEvent.emit('hide');
	    };
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', smartySpot_1.smartySpot)
	    ], shedulerList.prototype, "spot", void 0);
	    __decorate([
	        core_1.Output(), 
	        __metadata('design:type', Object)
	    ], shedulerList.prototype, "shedulerListEvent", void 0);
	    shedulerList = __decorate([
	        core_1.Component({
	            selector: 'shedulerList',
	            templateUrl: './app/views/sheduler/shedulerList.html',
	            directives: [shedulerItem_1.shedulerItem]
	        }), 
	        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService])
	    ], shedulerList);
	    return shedulerList;
	}());
	exports.shedulerList = shedulerList;
	//# sourceMappingURL=shedulerList.js.map

/***/ },

/***/ 384:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(5);
	var spotService_1 = __webpack_require__(330);
	var languageService_1 = __webpack_require__(376);
	var notifyService_1 = __webpack_require__(380);
	var spotShedulerItem_1 = __webpack_require__(334);
	var smartySpot_1 = __webpack_require__(331);
	var shedulerItem = (function () {
	    function shedulerItem(_spotService, _lan, _notify) {
	        this._spotService = _spotService;
	        this._lan = _lan;
	        this._notify = _notify;
	        this.shedulerDeleteEvent = new core_1.EventEmitter();
	        this.commandTHW = "";
	        this.commandTAction = "";
	        this.commandDateVal = "";
	        this.commandTimeVal = "";
	    }
	    shedulerItem.prototype.ngOnInit = function () {
	        var _this = this;
	        switch (this.shedulerItem.commandKey) {
	            case "ONOFF":
	                this.commandTAction = parseInt(this.shedulerItem.commandVal) ? this._lan.__("turnOn") : this._lan.__("turnOff");
	                break;
	            case "DIM":
	                this.commandTAction = this._lan.__("dimmer") + ":" + (Math.round(100 / 255 * parseInt(this.shedulerItem.commandVal))) + "%";
	                break;
	        }
	        switch (this.shedulerItem.dateKey) {
	            case "EDAY":
	                this.commandDateVal = this._lan.__("EDAY");
	                break;
	            case "DOW":
	                this.commandDateVal = this._lan.capts.onDOW(parseInt(this.shedulerItem.dateVal));
	                break;
	            case "DOM":
	                this.commandDateVal = this._lan.__("onDOM").replace("#DOM#", this.shedulerItem.dateVal);
	                break;
	            case "DATE":
	                this.commandDateVal = this.shedulerItem.dateVal;
	                break;
	            default:
	                break;
	        }
	        this.commandTitle = this.shedulerItem.title;
	        this.commandTimeVal = this._lan.__("ONTIME").replace("#TIME#", (this.shedulerItem.timeH < 10 ? "0" + this.shedulerItem.timeH : this.shedulerItem.timeH) + ":" + (this.shedulerItem.timeM < 10 ? "0" + this.shedulerItem.timeM : this.shedulerItem.timeM));
	        var taskHW = this.spot.hw.find(function (el) { return el.ID == _this.shedulerItem.hw; });
	        if (taskHW) {
	            var str = this._lan.capts.hw(taskHW.type, this.shedulerItem.hw);
	            if (str)
	                this.commandTHW = str.val;
	        }
	    };
	    shedulerItem.prototype.delete = function () {
	        this.shedulerDeleteEvent.emit(this.shedulerItem);
	    };
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', spotShedulerItem_1.spotShedulerItem)
	    ], shedulerItem.prototype, "shedulerItem", void 0);
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', smartySpot_1.smartySpot)
	    ], shedulerItem.prototype, "spot", void 0);
	    __decorate([
	        core_1.Output(), 
	        __metadata('design:type', Object)
	    ], shedulerItem.prototype, "shedulerDeleteEvent", void 0);
	    shedulerItem = __decorate([
	        core_1.Component({
	            selector: 'shedulerItem',
	            templateUrl: './app/views/sheduler/shedulerItem.html',
	            directives: []
	        }), 
	        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService])
	    ], shedulerItem);
	    return shedulerItem;
	}());
	exports.shedulerItem = shedulerItem;
	//# sourceMappingURL=shedulerItem.js.map

/***/ },

/***/ 385:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var core_1 = __webpack_require__(5);
	var spotService_1 = __webpack_require__(330);
	var languageService_1 = __webpack_require__(376);
	var notifyService_1 = __webpack_require__(380);
	var spotShedulerItem_1 = __webpack_require__(334);
	var smartySpot_1 = __webpack_require__(331);
	var datetimepeaker_1 = __webpack_require__(386);
	var shedulerAdd = (function () {
	    function shedulerAdd(_spotService, _lan, _notify, _datetimepeaker) {
	        this._spotService = _spotService;
	        this._lan = _lan;
	        this._notify = _notify;
	        this._datetimepeaker = _datetimepeaker;
	        this.shedulerAddEvent = new core_1.EventEmitter();
	        this.onLoading = false;
	        this.hw = null;
	        this.taskhw = null;
	        this.spotTools = [];
	        this.taskPType = null;
	        this.DOW = null;
	        this.DOM = null;
	        this.DATE = null;
	        this.HOURS = null;
	        this.MINUTES = null;
	        this.command = null;
	        this.DIM = null;
	        this.title = null;
	        this.renderDateNeed = false;
	        this.shedulerTaskPType = [];
	        this.DOWS = [];
	        this.avaibleHW = [];
	        this.avaibleDOMS = [];
	        this.avaibleHOURS = [];
	        this.avaibleMINUTES = [];
	    }
	    shedulerAdd.prototype.ngOnInit = function () {
	        this.shedulerTaskPType = this._lan.capts.shedulerTaskPType();
	        this.DOWS = this._lan.capts.DOWS();
	        for (var _i = 0, _a = this.spot.hw; _i < _a.length; _i++) {
	            var item = _a[_i];
	            this.avaibleHW.push(this._lan.capts.hw(item.type, item.ID));
	        }
	        for (var i = 1; i <= 31; i++) {
	            this.avaibleDOMS.push(i);
	        }
	        for (var i = 0; i <= 23; i++) {
	            this.avaibleHOURS.push(i);
	        }
	        for (var i = 0; i <= 55; i += 5) {
	            this.avaibleMINUTES.push(i);
	        }
	    };
	    shedulerAdd.prototype.ngDoCheck = function () { };
	    shedulerAdd.prototype.ngAfterViewChecked = function () {
	        if (this.renderDateNeed) {
	            console.log("DATE render");
	            this.setTaskDATE(dateYMD());
	            this._datetimepeaker.datepeaker(".shedulerAdd .dateInline");
	            this.renderDateNeed = false;
	        }
	    };
	    shedulerAdd.prototype.setTaskDATE = function (date) {
	        var shedulerAddDATE = document.getElementById("shedulerAddDATE");
	        if (shedulerAddDATE)
	            shedulerAddDATE.value = date;
	    };
	    shedulerAdd.prototype.getTaskDATE = function () {
	        var shedulerAddDATE = document.getElementById("shedulerAddDATE");
	        return shedulerAddDATE ? shedulerAddDATE.value : "";
	    };
	    shedulerAdd.prototype.setHW = function (event) {
	        if (!event.target.value)
	            return;
	        this.spotTools = [];
	        this.hw = this.spot.hw.find(function (el, indx, arr) {
	            return el.ID == event.target.value;
	        });
	        if (this.hw)
	            for (var _i = 0, _a = this.hw.tools; _i < _a.length; _i++) {
	                var tool = _a[_i];
	                if (tool == "ONOFF") {
	                    this.spotTools.push({ "key": "ON", "val": this._lan.capts.spotToolsCaptions("ON") });
	                    this.spotTools.push({ "key": "OFF", "val": this._lan.capts.spotToolsCaptions("OFF") });
	                }
	                else {
	                    this.spotTools.push({ "key": tool, "val": this._lan.capts.spotToolsCaptions(tool) });
	                }
	            }
	    };
	    shedulerAdd.prototype.setTaskPType = function (event) {
	        switch (event.target.value) {
	            case "DATE":
	                this.renderDateNeed = true;
	                break;
	        }
	    };
	    shedulerAdd.prototype.save = function () {
	        var _this = this;
	        this.onLoading = true;
	        this.DATE = this.getTaskDATE();
	        if (!this.hw
	            || !this.taskPType
	            || ["EDAY", "DOW", "DOM", "DATE"].indexOf(this.taskPType) == -1
	            || !this.command
	            || ["ON", "OFF", "DIM"].indexOf(this.command) == -1
	            || !this.title
	            || this.title.length == 0
	            || (this.taskPType == "DOW" && !parseInt(this.DOW))
	            || (this.taskPType == "DOM" && !this.DOM)
	            || (this.taskPType == "DATE" && !this.DATE)
	            || (this.taskPType == "DIM" && !this.DIM)) {
	            this._notify.error(this._lan.__('notFullFilled'));
	            this.onLoading = false;
	            return;
	        }
	        var commandKey = null;
	        var commandVal = null;
	        var dateKey = this.taskPType;
	        var dateVal = "1";
	        var timeH = parseInt(this.HOURS) || 0;
	        var timeM = parseInt(this.MINUTES) || 0;
	        var title = this.title;
	        switch (this.command) {
	            case "ON":
	                commandKey = "ONOFF";
	                commandVal = "1";
	                break;
	            case "OFF":
	                commandKey = "ONOFF";
	                commandVal = "0";
	                break;
	            case "DIM":
	                commandKey = "DIM";
	                commandVal = this.DIM;
	                break;
	        }
	        switch (this.taskPType) {
	            case "DOW":
	                dateVal = this.DOW.toString();
	                break;
	            case "DOM":
	                dateVal = this.DOM.toString();
	                break;
	            case "DATE":
	                dateVal = this.DATE;
	                break;
	        }
	        var spot_sheduler_item = new spotShedulerItem_1.spotShedulerItem(null, this.spot.ID, title, commandKey, commandVal, dateKey, dateVal, this.hw.ID, timeH, timeM, "", //dateYMD()//not cur time
	        new Date().getHours());
	        this._spotService.shedulerAdd(this.spot, spot_sheduler_item).then(function (success) {
	            _this._notify.alert(_this._lan.__('taskAdded'));
	            _this.onLoading = false;
	            _this.close();
	        }, function (fail) {
	            _this._notify.error(_this._lan.__('sendToSpotError'));
	            _this.onLoading = false;
	        });
	    };
	    shedulerAdd.prototype.close = function () {
	        this.shedulerAddEvent.emit('hide');
	    };
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', smartySpot_1.smartySpot)
	    ], shedulerAdd.prototype, "spot", void 0);
	    __decorate([
	        core_1.Output(), 
	        __metadata('design:type', Object)
	    ], shedulerAdd.prototype, "shedulerAddEvent", void 0);
	    shedulerAdd = __decorate([
	        core_1.Component({
	            selector: 'shedulerAdd',
	            templateUrl: './app/views/sheduler/shedulerAdd.html',
	            directives: []
	        }), 
	        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService, datetimepeaker_1.datetimepeaker])
	    ], shedulerAdd);
	    return shedulerAdd;
	}());
	exports.shedulerAdd = shedulerAdd;
	function dateYMD() {
	    var year = "", month = "", day = "", date = new Date();
	    year = String(date.getFullYear());
	    month = String(date.getMonth() + 1);
	    if (month.length == 1) {
	        month = "0" + month;
	    }
	    day = String(date.getDate());
	    if (day.length == 1) {
	        day = "0" + day;
	    }
	    return day + "." + month + "." + year;
	}
	//# sourceMappingURL=shedulerAdd.js.map

/***/ },

/***/ 386:
/***/ function(module, exports) {

	"use strict";
	var datetimepeaker = (function () {
	    function datetimepeaker() {
	    }
	    datetimepeaker.prototype.timepeaker = function (selector) {
	        $(selector).datetimepicker({
	            datepicker: false,
	            step: 15,
	            inline: true,
	            format: 'H:i'
	        });
	    };
	    datetimepeaker.prototype.datepeaker = function (selector) {
	        $(selector).datetimepicker({
	            timepicker: false,
	            format: 'd.m.Y',
	            inline: true,
	            lang: 'ru',
	            minDate: '0'
	        });
	    };
	    return datetimepeaker;
	}());
	exports.datetimepeaker = datetimepeaker;
	//# sourceMappingURL=datetimepeaker.js.map

/***/ },

/***/ 387:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
	    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
	    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
	    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
	    return c > 3 && r && Object.defineProperty(target, key, r), r;
	};
	var __metadata = (this && this.__metadata) || function (k, v) {
	    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
	};
	var __param = (this && this.__param) || function (paramIndex, decorator) {
	    return function (target, key) { decorator(target, key, paramIndex); }
	};
	var core_1 = __webpack_require__(5);
	var notifyService_1 = __webpack_require__(380);
	var languageService_1 = __webpack_require__(376);
	var spotService_1 = __webpack_require__(330);
	var http_1 = __webpack_require__(337);
	__webpack_require__(358);
	__webpack_require__(360);
	__webpack_require__(367);
	__webpack_require__(375);
	var spotAdd = (function () {
	    function spotAdd(_spotService, _lan, _notify, http) {
	        this._spotService = _spotService;
	        this._lan = _lan;
	        this._notify = _notify;
	        this.http = http;
	        this.noSpots = new core_1.EventEmitter();
	        this.spotAddEvent = new core_1.EventEmitter();
	        this.setConfig = 'http://192.168.4.1/setConfig';
	        this.exeCommand = 'http://192.168.4.1/exeCommand';
	        this.getInfo = 'http://192.168.4.1/getInfo';
	    }
	    spotAdd.prototype.ngOnInit = function () { this.begin(); };
	    spotAdd.prototype.cloading = function () { this.onLoading = true; };
	    spotAdd.prototype.cloaded = function () { this.onLoading = false; };
	    spotAdd.prototype.cerror = function () { this.onError = true; };
	    spotAdd.prototype.cnoerror = function () { this.onError = false; };
	    spotAdd.prototype.begin = function () {
	        this.title = this._lan.__('spotSearch');
	        this.onLoading = false;
	        this.onError = false;
	        this.localAP = null;
	        this.ssidList = [];
	        this.noSSSIDList = false;
	        if (confirm(this._lan.__('needWIFI'))) {
	            this.step0();
	        }
	        else {
	            this.stopAdd();
	        }
	    };
	    spotAdd.prototype.stopAdd = function () {
	        this.cloaded();
	        this.spotAddEvent.emit('close');
	    };
	    spotAdd.prototype.fail = function (message) {
	        if (message === void 0) { message = null; }
	        console.log("fail");
	        this.cloaded();
	        this.cerror();
	        this.title = message || this._lan.__('errorDefault');
	        this._notify.error(message);
	        this.noSSSIDList = false;
	    };
	    spotAdd.prototype.step0 = function () {
	        var _this = this;
	        this.cloading();
	        this._spotService.corExe('currentWiFiSSID', []).then(function (result) {
	            _this.localAP = result;
	            _this.step1();
	        }, function (message) {
	            _this.fail(_this._lan.__('noLocalWIFI'));
	        });
	    };
	    spotAdd.prototype.step1 = function () {
	        var _this = this;
	        this._spotService.corExe('findNewSpot', []).then(function (data) {
	            _this.step2();
	        }, function (data) {
	            if (data == 'NotFound') {
	                _this._notify.alert(_this._lan.__('notConfiguredNF'));
	                _this._spotService.corExe('connectConfigured', [_this.localAP]).then(function () {
	                    _this._spotService.spotsIdentify();
	                    _this.stopAdd();
	                }, function () {
	                    _this.fail(data);
	                });
	            }
	            else {
	                _this.fail(data);
	            }
	        });
	    };
	    spotAdd.prototype.step2 = function () {
	        var _this = this;
	        this._spotService.corExe('connectNewSpot', []).then(function () {
	            _this.step3();
	        }, function (data) {
	            if (data == 'NotConnected') {
	                _this.fail(_this._lan.__('spotCantConnect'));
	            }
	            else {
	                _this.fail(data);
	            }
	        });
	    };
	    spotAdd.prototype.step3 = function () {
	        var _this = this;
	        this._spotService.corExe('isConnectedToNewSpot', []).then(function (result) {
	            _this.step4();
	        }, function (data) {
	            if (data == 'NotConnected') {
	                _this.fail(_this._lan.__('spotCantConnect'));
	            }
	            else {
	                _this.fail(data);
	            }
	        });
	    };
	    spotAdd.prototype.step4 = function () {
	        var _this = this;
	        console.log("step4");
	        this.cloading();
	        this.ssidList = [];
	        this.noSSSIDList = false;
	        this.title = this._lan.__('spotSearch');
	        this.http.get(this.getInfo + '?ssidlist=1')
	            .map(function (res) { return res.json(); })
	            .subscribe(function (data) {
	            _this.ssidList = [];
	            _this.cloaded();
	            if (data.result) {
	                _this.title = _this._lan.__('spotConnectWiFi');
	                for (var item in data.content) {
	                    _this.ssidList.push({ 'bssid': item, 'ssid': data.content[item] });
	                }
	                if (_this.ssidList.length == 0) {
	                    _this.title = _this._lan.__('spotConnectWiFiNotFound');
	                    _this.noSSSIDList = true;
	                }
	            }
	            else {
	                _this.fail();
	            }
	        }, function (err) { return _this.fail(err); });
	    };
	    spotAdd.prototype.step5 = function (apBSSID) {
	        var _this = this;
	        this.cloading();
	        this.ssidList = [];
	        this.title = this._lan.__('spotConnectWiFiPass');
	        this._spotService.corExe('isConnectedToNewSpot', []).then(function (result) {
	            var apPass = prompt(_this._lan.__('spotConnectWiFiPass'), '');
	            if (apPass.length == 0) {
	                _this._notify.alert(_this._lan.__('spotConnectWiFiCantConnect'));
	                _this.step4();
	                return;
	            }
	            console.log(_this.setConfig + '?apBSSID=' + apBSSID + "&apPass=" + apPass);
	            _this.http.get(_this.setConfig + '?apBSSID=' + apBSSID + "&apPass=" + apPass)
	                .map(function (res) { return res.text(); })
	                .subscribe(function (data) {
	                if (parseInt(data) == 1) {
	                    _this.step6();
	                }
	                else {
	                    _this.fail();
	                }
	            }, function (err) { return _this.fail(err); });
	        }, function (data) {
	            if (data == 'NotConnected') {
	                _this.fail(_this._lan.__('spotCantConnect'));
	            }
	            else {
	                _this.fail(data);
	            }
	        });
	    };
	    spotAdd.prototype.step6 = function () {
	        var _this = this;
	        this.title = this._lan.__('spotConnectWiFiConnecting');
	        this.http.get(this.exeCommand + '?connectAP=1')
	            .map(function (res) { return res.text(); })
	            .timeout(60000, function () { console.log("timeout"); _this.fail(_this._lan.__('spotCantConnect')); })
	            .subscribe(function (data) {
	            console.log("step6", parseInt(data));
	            switch (parseInt(data)) {
	                case 1:
	                    _this.step7();
	                    break;
	                case 2:
	                case -1:
	                    _this._notify.error(_this._lan.__('spotConnectWiFiPassWrong'));
	                    _this.step4();
	                    break;
	                case -2:
	                case -3:
	                    _this._notify.error(_this._lan.__('spotConnectWiFiCantConnect'));
	                    _this.step4();
	                    break;
	                case -4:
	                    _this._notify.error(_this._lan.__('spotConnectWiFiNotSecured'));
	                    _this.step4();
	                    break;
	                default:
	                    _this._notify.error(_this._lan.__('spotConnectWiFiCantConnect'));
	                    _this.step4();
	            }
	        }, function (err) { return _this.fail(err); });
	    };
	    spotAdd.prototype.step7 = function () {
	        var _this = this;
	        this.title = this._lan.__('spotConfigured');
	        this._spotService.corExe('defaultTimeZone', []).then(function (timeZone) {
	            console.log(_this.setConfig + '?timeZone=' + timeZone);
	            _this.http.get(_this.setConfig + '?timeZone=' + timeZone)
	                .map(function (res) { return res.text(); })
	                .subscribe(function (data) {
	                if (parseInt(data) == 1) {
	                    _this.step8();
	                }
	                else {
	                    _this.fail();
	                }
	            }, function (err) { return _this.fail(err); });
	        }, function () {
	            _this.fail();
	        });
	    };
	    spotAdd.prototype.step8 = function () {
	        var _this = this;
	        this.title = this._lan.__('spotEnterName');
	        var spotTitle = prompt(this._lan.__('spotEnterName'), '');
	        this.http.get(this.setConfig + '?spotTitle=' + spotTitle)
	            .map(function (res) { return res.text(); })
	            .subscribe(function (data) {
	            if (parseInt(data) == 1) {
	                _this.step9();
	            }
	            else {
	                _this.fail();
	            }
	        }, function (err) { return _this.fail(err); });
	    };
	    spotAdd.prototype.step9 = function () {
	        var _this = this;
	        this.http.get(this.exeCommand + '?rebootSpot=1')
	            .map(function (res) { return res.text(); })
	            .subscribe(function (data) { }, function (err) { }, function () {
	            _this._spotService.corExe('connectConfigured', [_this.localAP]).then(function () { }, function () { });
	            _this.title = _this._lan.__('spotConnected');
	            _this.stopAdd();
	        });
	    };
	    __decorate([
	        core_1.Input(), 
	        __metadata('design:type', Object)
	    ], spotAdd.prototype, "noSpots", void 0);
	    __decorate([
	        core_1.Output(), 
	        __metadata('design:type', Object)
	    ], spotAdd.prototype, "spotAddEvent", void 0);
	    spotAdd = __decorate([
	        core_1.Component({
	            selector: 'spotAdd',
	            templateUrl: './app/views/spot/spotAdd.html',
	        }),
	        __param(3, core_1.Inject(http_1.Http)), 
	        __metadata('design:paramtypes', [spotService_1.spotService, languageService_1.languageService, notifyService_1.notifyService, http_1.Http])
	    ], spotAdd);
	    return spotAdd;
	}());
	exports.spotAdd = spotAdd;
	//# sourceMappingURL=spotAdd.js.map

/***/ }

});
//# sourceMappingURL=main.bundle.js.map